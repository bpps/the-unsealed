import { SELECT_ADVISOR } from '../actions/selectedAdvisor'
import { SET_QAS } from '../actions/qas'
import { fetchQas } from '../utils'

const getAdvisors = store => next => action => {
  next(action)
  switch (action.type) {
    case SELECT_ADVISOR:
      fetchQas( action.selectedAdvisor, 1 ).then( qas_obj => {
        const qas = qas_obj.letters
        next({
          type: SET_QAS,
          qas,
          merge: false
        })
      })
    break
    default:
    break
  }
}

export default getAdvisors
