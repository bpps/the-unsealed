export const SELECT_ADVISOR = 'SELECT_ADVISOR'

export const selectAdvisor = selectedAdvisor => {
  return {
    type: SELECT_ADVISOR,
    selectedAdvisor
  }
}
