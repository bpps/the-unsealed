export const SET_QAS = 'SET_QAS'

export const setQas = (qas, merge) => {
  return {
    type: SET_QAS,
    qas,
    merge
  }
}
