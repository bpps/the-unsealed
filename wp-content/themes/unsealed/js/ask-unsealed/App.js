import React from 'react'
import Advisors from './components/Advisors'
import Qas from './components/Qas'
import { Helmet } from "react-helmet";

function App(props) {
  return (
    <div className="ask-unsealed-app pb-5 container-wide">
      <Helmet>
        <meta charSet="utf-8" />
        <title>The Unsealed - Ask The Unsealed</title>
        <link rel="canonical" href="unsealed/ask-the-unsealed" />
      </Helmet>
      <div className="row">
        <Advisors/>
        <Qas location={props.location} history={props.history}/>
      </div>
    </div>
  );
}

export default App
