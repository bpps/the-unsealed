import { combineReducers } from 'redux'
import { qas, loadingQas } from './qas'
import { selectedAdvisor } from './selectedAdvisor'

export default combineReducers({
  qas,
  loadingQas,
  selectedAdvisor
})
