import { SET_QAS } from '../actions/qas'
import { SELECT_ADVISOR } from '../actions/selectedAdvisor'

export const qas = (state = [], action) => {
  switch(action.type) {
    case SET_QAS:
      if ( action.merge ) {
        return [...state, ...action.qas]
      } else {
        return action.qas
      }
    default:
      return state
  }
}

export const loadingQas = (state = false, action) => {
  switch(action.type) {
    case SELECT_ADVISOR:
      return true
    case SET_QAS:
      return false
    default:
      return state
  }
}
