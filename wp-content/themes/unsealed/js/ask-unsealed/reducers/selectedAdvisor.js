import { SELECT_ADVISOR } from '../actions/selectedAdvisor'

export const selectedAdvisor = (state = null, action) => {
  switch(action.type) {
    case SELECT_ADVISOR:
      return action.selectedAdvisor
    default:
      return state
  }
}
