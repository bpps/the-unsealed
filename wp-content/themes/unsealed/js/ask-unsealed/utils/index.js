import axios from 'axios'

export var decodeEntities = (function() {
  // this prevents any overhead from creating the object each time
  var element = document.createElement('div');

  function decodeHTMLEntities (str) {
    if(str && typeof str === 'string') {
      // strip script/html tags
      str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
      str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
      element.innerHTML = str;
      str = element.textContent;
      element.textContent = '';
    }

    return str;
  }

  return decodeHTMLEntities;
})();

export var objectToFormData = (obj, form, namespace) => {
  var fd = form || new FormData();
  var formKey;
  for(var property in obj) {
    if(obj.hasOwnProperty(property)) {
      if(namespace) {
        formKey = namespace + '[' + property + ']';
      } else {
        formKey = property;
      }
      // if the property is an object, but not a File,
      // use recursivity.
      if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
        objectToFormData(obj[property], fd, property);
      } else {
        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }
    }
  }
  return fd;
};


export function fetchQas (advisor, paged = 1, exclude = '') {
  let data = {
    action: 'do_ajax',
    fn : 'get_letters',
    dataType: 'json',
    advisor: advisor,
    paged: paged,
    postType: 'ask-the-unsealed'
  }
  console.log(data)
  if ( exclude != '' ) {
    data.exclude = exclude
  }
  return axios.post( ajaxurl, objectToFormData(data) )
  .then(results => {
    return results.data
  })
  .catch( error => error )
}

export function fetchQaBySlug (slug) {
  let data = {
    action: 'do_ajax',
    fn : 'get_letter_by_slug',
    dataType: 'json',
    postType: 'ask-the-unsealed',
    slug: slug
  }
  return axios.post( ajaxurl, objectToFormData(data) )
  .then(results => {
    return results.data
  })
  .catch( error => error )
}
