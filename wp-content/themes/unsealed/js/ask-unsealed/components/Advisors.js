import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Advisor from './Advisor'
import { selectAdvisor } from '../actions/selectedAdvisor'

class Advisors extends Component {
  constructor (props) {
    super(props)
    this.handleSelectAdvisor = this.handleSelectAdvisor.bind(this)
  }

  handleSelectAdvisor (advisor) {
    if ( advisor === this.props.selectedAdvisor ) return
    this.props.dispatch( selectAdvisor(advisor) )
  }

  render () {
    const { selectedAdvisor } = this.props
    return (
      <div className="col-md-3 terms-column">
        <div className="filter-header py-3 d-flex justify-content-center align-items-center d-md-none">
          <select className="filter-terms underlined py-1" value={selectedAdvisor || ''} onChange={ advisor => this.handleSelectAdvisor(advisor.target.value) }>
            <option value="disabled topic" disabled>See advice from...</option>
            <option value="">All advice</option>
            {
              advisors.map( advisor => {
                return (
                  <option key={`select-advisor-${advisor.term_id}`} value={advisor.slug}>
                    {advisor.name}
                  </option>
                )
              })
            }
          </select>
        </div>
        <div className="d-none d-md-block py-3">
          <Advisor advisor={{ name: 'All advice', term_id: null }} />
          {
            advisors.map( advisor => {
              return (
                <div className="btn-row" key={`advisor-${advisor.term_id}`}>
                  <button
                    className={`btn-brush-hover btn${ (advisor.term_id === selectedAdvisor || advisor.slug === selectedAdvisor) ? ' selected' : '' }`}
                    key={ `advisor-${advisor.term_id}` }
                    onClick={ () => this.handleSelectAdvisor(advisor.term_id) }>
                    <span>{ advisor.name }</span>
                  </button>
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }
}

Advisors.propTypes = {

}

export default connect((state) => ({
  selectedAdvisor: state.selectedAdvisor
}))(Advisors)
