import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchQas, fetchQaBySlug } from '../utils'
import { loadQas, setQas } from '../actions/qas'
import Qa from './Qa'

class Qas extends Component {
  constructor (props) {
    super(props)
    this.handleScroll = this.handleScroll.bind(this)
    this.getMoreQas = this.getMoreQas.bind(this)
    this.state = {
      paged: 1,
      fetching: true,
      endOfFeed: false,
      initialPost: ''
    }
  }

  componentDidMount () {
    document.addEventListener('scroll', this.handleScroll);
    const { dispatch, history, location, selectedAdvisor } = this.props
    const currentPath = this.props.location.pathname.replace('/', '')
    if ( currentPath != '' ) {
      // Get url slug as first post
      fetchQaBySlug(currentPath).then( firstQa => {
        fetchQas(false, 1, currentPath).then( qas => {
          const allQas = [...firstQa, ...qas.letters]
          dispatch( setQas(allQas, false) )
          this.setState({ fetching: false, paged: 2, initialPost: currentPath, endOfFeed: !qas.has_more_letters })
        })
      })
    } else {
      const advisor = selectedAdvisor ? selectedAdvisor : false
      fetchQas(advisor, 1, currentPath).then( qas => {
        dispatch( setQas(qas.letters, false) )
        this.setState({ fetching: false, paged: 2, endOfFeed: !qas.has_more_letters })
      })
    }

  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleScroll);
  }

  shouldComponentUpdate (nextProps, nextState) {
    if ( nextProps.selectedAdvisor != this.props.selectedAdvisor ) {
      const { dispatch } = this.props
      this.setState({ fetching: true })
      fetchQas( nextProps.selectedAdvisor, 1 ).then( qas => {
        dispatch( setQas(qas.letters, false) )
        this.setState({ paged: 2, fetching: false, endOfFeed: !qas.has_more_letters, initialPost: '' })
      })
    }
    return true
  }

  getMoreQas () {
    const { dispatch, selectedAdvisor } = this.props
    fetchQas(selectedAdvisor, this.state.paged, this.state.initialPost).then( qas => {
      // Stop updating if no qas are returned.
      // NOTE: this can be changed to have the function return a bool if there are more qas
      dispatch( setQas(qas.letters, true) )
      this.setState({ fetching: false, paged: this.state.paged + 1, endOfFeed: !qas.has_more_letters })
    })
  }

  handleScroll () {
    if ( this.state.fetching || this.state.endOfFeed ) return
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset + (docHeight / 3);
    if (windowBottom >= docHeight) {
      this.setState({
        fetching: true
      }, this.getMoreQas() )
    }
  }

  render () {
    const { qas, loadingQas, selectedAdvisor, history } = this.props
    const { fetching, endOfFeed } = this.state
    return (
      <div className="col-md-9 col-lg-7">
        <div className="letters">
          {
            !loadingQas &&
            qas.map( qa => {
              return (
                <Qa key={`qa-${qa.id}`} qa={qa} history={history}/>
              )
            })
          }
          {
            ( loadingQas || (fetching && !endOfFeed) ) && <div className="py-3 loading d-flex justify-content-center"><span>Loading</span></div>
          }
          {
            (qas.length === 0 && !loadingQas) &&
            <p>No qas</p>
          }
        </div>
      </div>
    )
  }
}

Qas.propTypes = {

}

export default connect((state) => ({
  qas: state.qas,
  loadingQas: state.loadingQas,
  selectedAdvisor: state.selectedAdvisor
}))(Qas)
