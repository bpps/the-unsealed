import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { selectAdvisor } from '../actions/selectedAdvisor'

class Advisor extends Component {
  constructor (props) {
    super(props)
    this.handleSelectAdvisor = this.handleSelectAdvisor.bind(this)
  }

  handleSelectAdvisor (advisor) {
    if ( advisor === this.props.selectedAdvisor ) return
    this.props.dispatch( selectAdvisor(advisor) )
  }

  render () {
    const { advisor, selectedAdvisor } = this.props
    return (
      <div className="btn-row">
        <button
          className={`btn-brush-hover btn${ (advisor.term_id === selectedAdvisor || advisor.slug === selectedAdvisor) ? ' selected' : '' }`}
          key={ `advisor-${advisor.term_id}` }
          onClick={ () => this.handleSelectAdvisor(advisor.term_id) }>
          <span>{ advisor.name }</span>
        </button>
      </div>
    )
  }
}

export default connect((state) => ({
  selectedAdvisor: state.selectedAdvisor
}))(Advisor)
