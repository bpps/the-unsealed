import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Advisor from './Advisor'
import VizSensor from 'react-visibility-sensor';
import { Helmet } from "react-helmet";
import { decodeEntities } from '../utils';

import {
  EmailShareButton,
  EmailIcon,
  FacebookShareButton,
  FacebookShareCount,
  FacebookIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TwitterShareButton,
  TwitterIcon
} from "react-share";

class Qa extends Component {
  constructor (props) {
    super(props)
    this.handleQaActive = this.handleQaActive.bind(this)
    this.state = { isVisible: false }
  }

  handleQaActive (isVisible) {
    const { qa, history } = this.props
    if ( isVisible ) history.push(`/${qa.slug}`)
    this.setState({ isVisible: isVisible })
  }

  render () {
    const { qa } = this.props
    const basename = this.props.history.createHref({pathname: '/'})
    const qaUrl = `${siteurl}${basename}${qa.slug}`
    return (
      <div className="letter border-bottom pt-4 pb-5">
        {
          this.state.isVisible &&
          <Helmet>
            <meta charSet="utf-8" />
            <title>{qa.title}</title>
            <link rel="canonical" href={qaUrl} />
            <meta property="og:url" content={qaUrl} />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={qa.title} />
            <meta property="og:description" content={qa.excerpt ? qa.excerpt : ''} />
            <meta property="og:image" content={qa.featured_image ? qa.featured_image : ''} />
          </Helmet>
        }
        <VizSensor
          key={qa.id}
          onChange={isVisible => this.handleQaActive(isVisible)}>
          <div className="viz-sensor"></div>
        </VizSensor>
        <div>
          {
            qa.terms &&
            <div className="row align-items-center advice-from no-gutters">
              <span className="col-auto mr-1">To </span><Advisor key={`qa-${qa.terms[0].id}-advisor-${qa.terms[0].term_id}`} advisor={qa.terms[0]} /><span className="col-auto">:</span>
            </div>
          }
          <div className="qa">
            <h3 className="qa-question mb-1">{qa.title}</h3>
            <div className="question-info mb-5">
              <p>Question from <span>{ qa.author }</span></p>
            </div>
            <div className="qa-answer font-weight-bold pb-2" dangerouslySetInnerHTML={{ __html: decodeEntities(qa.content) }}></div>
          </div>
          <div className="social-share-links row align-items-center pull-right">
            <div className="col-auto">
              <span className="script-font h4 share-label mr-2">Share this advice</span>
            </div>
            <div className="social-link-wrapper col-auto">
              <FacebookShareButton url={qaUrl} quote={qa.excerpt ? qa.excerpt : ''}>
                <FacebookIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
              </FacebookShareButton>
              <FacebookShareCount url={qaUrl}>
                {shareCount => <span className="myShareCountWrapper">{shareCount}</span>}
              </FacebookShareCount>
            </div>
            <div className="social-link-wrapper col-auto">
              <LinkedinShareButton url={qaUrl} title={qa.title} summary={qa.excerpt ? qa.excerpt : false} source="https://theunsealed.com">
                <LinkedinIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
              </LinkedinShareButton>
            </div>
            <div className="social-link-wrapper col-auto">
              <TwitterShareButton url={qaUrl} title={qa.title}>
                <TwitterIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
              </TwitterShareButton>
            </div>
            <div className="social-link-wrapper col-auto">
              <EmailShareButton url={qaUrl} subject={"I thought you might like this post from The Unsealed"} separator="&nbsp;" body={`${qa.title} - ${qa.excerpt ? qa.excerpt : ''}`}>
                <EmailIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
              </EmailShareButton>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Qa.propTypes = {

}

export default connect((state) => ({

}))(Qa)
