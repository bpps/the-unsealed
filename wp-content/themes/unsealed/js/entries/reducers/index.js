import { combineReducers } from 'redux'
import { entries, loadingEntries } from './entries'
import { selectedContest } from './selectedContest'
import { userEmail, showModal, verifyStep } from './userEmail'

export default combineReducers({
  entries,
  loadingEntries,
  selectedContest,
  userEmail,
  showModal,
  verifyStep
})
