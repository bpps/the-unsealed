import { SET_EMAIL, TOGGLE_MODAL, CHECK_EMAIL, SET_VERIFY_STEP } from '../actions/userEmail'
import { signupEmail } from '../utils'

export const verifyStep = (state = null, action) => {
  switch(action.type) {
    case SET_VERIFY_STEP:
      return action.data
    default:
      return state
  }
}

export const userEmail = (state = null, action) => {
  switch(action.type) {
    case SET_EMAIL:
      return action.email
    default:
      return state
  }
}

export const showModal = (state = null, action) => {
  switch(action.type) {
    case TOGGLE_MODAL:
      return action.toggle
    case SET_EMAIL:
      return false
    default:
      return state
  }
}
