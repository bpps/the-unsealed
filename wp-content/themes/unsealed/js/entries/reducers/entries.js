import { SET_ENTRIES } from '../actions/entries'
import { SELECT_CONTEST } from '../actions/selectedContest'

export const entries = (state = [], action) => {
  switch(action.type) {
    case SET_ENTRIES:
      if ( action.merge ) {
        return [...state, ...action.entries]
      } else {
        return action.entries
      }
    default:
      return state
  }
}

export const loadingEntries = (state = false, action) => {
  switch(action.type) {
    case SELECT_CONTEST:
      return true
    case SET_ENTRIES:
      return false
    default:
      return state
  }
}
