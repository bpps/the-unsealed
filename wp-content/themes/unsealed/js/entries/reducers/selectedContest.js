import { SELECT_CONTEST } from '../actions/selectedContest'

export const selectedContest = (state = null, action) => {
  switch(action.type) {
    case SELECT_CONTEST:
      return action.selectedContest
    default:
      return state
  }
}
