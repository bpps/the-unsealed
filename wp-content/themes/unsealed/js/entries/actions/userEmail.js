export const SET_EMAIL = 'SET_EMAIL'
export const TOGGLE_MODAL = 'TOGGLE_MODAL'
export const CHECK_EMAIL = 'CHECK_EMAIL'
export const VERIFY_EMAIL = 'VERIFY_EMAIL'
export const VERIFY_CODE = 'VERIFY_CODE'
export const SET_VERIFY_STEP = 'SET_VERIFY_STEP'

export const setEmail = email => {
  return {
    type: SET_EMAIL,
    email
  }
}

export const checkEmail = email => {
  return {
    type: CHECK_EMAIL,
    email
  }
}

export const verficationEmail = email => {
  return {
    type: VERIFY_EMAIL,
    email
  }
}

export const checkVerificationCode = (email, code) => {
  return {
    type: VERIFY_CODE,
    email,
    code
  }
}

export const toggleModal = toggle => {
  return {
    type: TOGGLE_MODAL,
    toggle
  }
}