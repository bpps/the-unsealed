export const SELECT_CONTEST = 'SELECT_CONTEST'

export const selectContest = selectedContest => {
  return {
    type: SELECT_CONTEST,
    selectedContest
  }
}
