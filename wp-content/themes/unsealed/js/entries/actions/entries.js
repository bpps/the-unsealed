export const SET_ENTRIES = 'SET_ENTRIES'

export const setEntries = (entries, merge) => {
  return {
    type: SET_ENTRIES,
    entries,
    merge
  }
}
