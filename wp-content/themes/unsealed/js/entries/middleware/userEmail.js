import { CHECK_EMAIL, SET_EMAIL, VERIFY_EMAIL, VERIFY_CODE, SET_VERIFY_STEP } from '../actions/userEmail'
import { signupEmail, verificationEmail, checkVerificationCode } from '../utils'
import Cookies from 'js-cookie';

const checkEmail = store => next => action => {
  next(action)
  switch (action.type) {
    case CHECK_EMAIL:
      signupEmail( action.email ).then( data => {
        if ( data.status == 'already signed up' || data.status == 'successful signup' ) {
          next({
            type: SET_EMAIL,
            email: action.email
          })
        }
      })
    break;
    case VERIFY_EMAIL:
      verificationEmail(action.email).then (resp => {
        console.log(resp)
        if (resp.status == 200) {
          if (resp.data.state == 'succeed_in_sending_verification_code') {
            next({
              type: SET_VERIFY_STEP,
              data: {
                step: 'verify_code',
                error: null
              }
            })
          } else if (resp.data.state == 'already_registered') {
            if (confirm('You\'re already signed up!')) {
              Cookies.set('vote_email', action.email, { expires: 30 });
              next({
                type: SET_EMAIL,
                email: action.email
              })
            } else {
              console.log('not saved to db.');
            }

          } else if (resp.data.state == 'error') {
            next({
              type: SET_VERIFY_STEP,
              data: {
                step: 'email',
                error: resp.data.error_message
              }
            })
          }
        }
      })
      break;
    case VERIFY_CODE:
      checkVerificationCode(action.email, action.code).then(resp => {
        if (resp.status == 200) {
          if (resp.data.state == 'succeed_in_verification') {
            next({
              type: SET_EMAIL,
              email: action.email
            })
          } else if (resp.data.state == 'already_signed_up') {
            next({
              type: SET_EMAIL,
              email: action.email
            })
          }else if (resp.data.state == 'failed_in_verification') {
            next({
              type: SET_VERIFY_STEP,
              data: {
                step: 'verify_code',
                error: 'Failed to verify to: Please check the verification code that was sent to your email.'
              }
            })
          } else if (resp.data.state == 'invalid_email') {
            next({
              type: SET_VERIFY_STEP,
              data: {
                step: 'verify_code',
                error: 'Failed to Add email to MailChimp email list, Please contact us.'
              }
            });
          } else if (resp.data.state == 'error') {
            next({
              type: SET_VERIFY_STEP,
              data: {
                step: 'verify_code',
                error: 'Something went wrong, please contact us.'
              }
            });
          }
        }
      })
      break;
    default:
    break
  }
}

export default checkEmail
