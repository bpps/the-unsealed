import { SELECT_CONTEST } from '../actions/selectedTopic'
import { SET_ENTRIES } from '../actions/entries'
import { fetchLetters } from '../utils'

const getTopics = store => next => action => {
  next(action)
  switch (action.type) {
    case SELECT_CONTEST:
      fetchLetters( action.selectedTopic, 1 ).then( entries_obj => {
        const entries = entries_obj.entries
        next({
          type: SET_ENTRIES,
          entries,
          merge: false
        })
      })
    break
    default:
    break
  }
}

export default getTopics
