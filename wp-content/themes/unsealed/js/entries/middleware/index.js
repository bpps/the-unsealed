import logger from './logger'
import checkEmail from './userEmail'
import { applyMiddleware } from 'redux'

export default applyMiddleware(checkEmail)
