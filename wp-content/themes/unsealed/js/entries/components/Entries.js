import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchEntries, fetchEntryById } from '../utils'
import { loadEntries, setEntries } from '../actions/entries'
import Entry from './Entry'

class Entries extends Component {
  constructor (props) {
    super(props)
    this.handleScroll = this.handleScroll.bind(this)
    this.getMoreEntries = this.getMoreEntries.bind(this)
    this.state = {
      paged: 1,
      fetching: true,
      endOfFeed: false,
      initialPost: ''
    }
  }

  componentDidMount () {
    document.addEventListener('scroll', this.handleScroll);
    const { dispatch, history, location } = this.props
    let currentPath = this.props.location.pathname.replace('/', '')
    currentPath = currentPath.replace('/', '')
    if ( currentPath != '' ) {
      // Get url slug as first post
      fetchEntryById(currentPath).then( firstEntry => {
        console.log(firstEntry);
        fetchEntries(false, 1, firstEntry[0].slug).then( entries_obj => {
          const allEntries = [...firstEntry, ...entries_obj.entries]
          dispatch( setEntries(allEntries, false) )
          this.setState({ fetching: false, paged: 2, initialPost: currentPath, endOfFeed: !entries_obj.has_more_entries })
        })
      })
    } else {
      fetchEntries(false, 1, currentPath).then( entries_obj => {
        dispatch( setEntries(entries_obj.entries, false) )
        this.setState({ fetching: false, paged: 2, endOfFeed: !entries_obj.has_more_entries })
      })
    }

  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleScroll);
  }

  shouldComponentUpdate (nextProps, nextState) {
    if ( nextProps.selectedContest != this.props.selectedContest ) {
      const { dispatch } = this.props
      this.setState({ fetching: true })
      fetchEntries( nextProps.selectedContest, 1 ).then( entries_obj => {
        dispatch( setEntries(entries_obj.entries, false) )
        this.setState({ paged: 2, fetching: false, endOfFeed: !entries_obj.has_more_entries, initialPost: '' })
      })
    }
    return true
  }

  getMoreEntries () {
    const { dispatch, selectedContest } = this.props
    fetchEntries(selectedContest, this.state.paged, this.state.initialPost).then( entries_obj => {
      // Stop updating if no entries are returned.
      // NOTE: this can be changed to have the function return a bool if there are more entries
      dispatch( setEntries(entries_obj.entries, true) )
      this.setState({ fetching: false, paged: this.state.paged + 1, endOfFeed: !entries_obj.has_more_entries })
    })
  }

  handleScroll () {
    if ( this.state.fetching || this.state.endOfFeed ) return
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset + (docHeight / 3);
    if (windowBottom >= docHeight) {
      this.setState({
        fetching: true
      }, this.getMoreEntries() )
    }
  }

  render () {
    const { entries, loadingEntries, selectedContest, history } = this.props
    const { fetching, endOfFeed } = this.state
    return (
      <div className="col-md-9 col-lg-7">
        <div className="letters">
          {
            !loadingEntries &&
            entries.map( (entry, ind) => {
              return (
                <Entry key={`entry-${entry.id}`} isFirst={ind === 0} entry={entry} history={history}/>
              )
            })
          }
          {
            ( loadingEntries || (fetching && !endOfFeed) ) && <div className="py-3 loading d-flex justify-content-center"><span>Loading</span></div>
          }
          {
            (entries.length === 0 && !loadingEntries) &&
            <p>No entries</p>
          }
        </div>
      </div>
    )
  }
}

Entries.propTypes = {

}

export default connect((state) => ({
  entries: state.entries,
  loadingEntries: state.loadingEntries,
  selectedContest: state.selectedContest,
  showModal: state.showModal
}))(Entries)
