import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Contest from './Contest'
import VizSensor from 'react-visibility-sensor';
import { Helmet } from "react-helmet";
import ReactDOMServer from 'react-dom/server';
import { upvoteEntry, decodeEntities } from '../utils'
import { toggleModal } from '../actions/userEmail'
import Heart from '../assets/heart.svg';
import HeartVoted from '../assets/heart-voted.svg';
import Cookies from 'js-cookie';

import {
  EmailShareButton,
  EmailIcon,
  FacebookShareButton,
  FacebookShareCount,
  FacebookIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TwitterShareButton,
  TwitterIcon
} from "react-share";

class Entry extends Component {
  constructor (props) {
    super(props)
    this.handleEntryActive = this.handleEntryActive.bind(this)
    this.handleVote = this.handleVote.bind(this)
    this.state = {
      isVisible: props.isFirst,
      votes: props.entry.votes,
      shouldUpvote: false
    }
  }

  handleVote () {
    const { entry, userEmail, dispatch } = this.props
    this.setState({ shouldUpvote: false })
    if ( userEmail ) {
      const voteCount = user_info.member ? entry.member_vote : 1
      upvoteEntry(entry.id, userEmail, voteCount).then( votes => {
        console.log('votes', votes);
        this.setState({ votes, shouldUpvote: false, voted: true })
        setTimeout( () => { this.setState({ voted: false }) }, 2500 );
      })
    } else {
      let cookie_email = Cookies.get('vote_email');
      if (cookie_email && cookie_email.length > 0) {
        let email = cookie_email;
        const voteCount = user_info.member ? entry.member_vote : 1
        upvoteEntry(entry.id, email, voteCount).then( votes => {
          this.setState({ votes, shouldUpvote: false, voted: true })
          setTimeout( () => { this.setState({ voted: false }) }, 2500 );
        })
      } else {
        this.setState({ shouldUpvote: true }, () => {
          dispatch( toggleModal(true) )
        })
      }
    }
  }

  componentDidUpdate(prevProps) {
    if ( prevProps.showModal !== this.props.showModal && this.props.showModal == false ) {
      if ( !this.props.userEmail ) {
        this.setState({ shouldUpvote: false })
      } else {
        if (
          this.state.shouldUpvote &&
          this.state.votes[this.props.userEmail] == undefined ) {
          this.handleVote()
        }
      }
    }
  }

  handleEntryActive (isVisible) {
    const { entry, history } = this.props
    if ( isVisible ) history.push(`/${entry.slug}`)
    this.setState({
      isVisible: isVisible
    })
  }

  render () {
    const { entry, selectedContest, userEmail } = this.props
    const { votes, voted } = this.state
    console.log(votes, userEmail)
    const canVote = votes[userEmail] == undefined
    const basename = this.props.history.createHref({pathname: '/'})
    const entryUrl = entry.link
    const myVote = user_info.member ? entry.member_vote : 1
    console.log(entryUrl, entry)
    const voteCount = Object.keys(votes).reduce(function (previous, key) {
        return previous + parseInt(votes[key].votes);
    }, 0);
    return (
      <div className="letter border-bottom pt-4 pb-5">
        {
          this.state.isVisible &&
          <Helmet>
            <meta charSet="utf-8" />
            <title>{entry.title}</title>
            <link rel="canonical" href={entryUrl} />
            <meta property="og:url" content={entryUrl} />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={entry.title} />
            <meta property="og:description" content={entry.excerpt ? entry.excerpt : ''} />
            <meta property="og:image" content={entry.featured_image ? entry.featured_image : ''} />
          </Helmet>
        }
        <VizSensor
          key={entry.id}
          onChange={isVisible => this.handleEntryActive(isVisible)}>
          <div className="viz-sensor"></div>
        </VizSensor>
        <div className="row">
          {
            (entry.contest && selectedContest === null) &&
            <div className="container topics-row row">
              <Contest key={`entry-${entry.id}-term-${entry.contest.id}`} contest={entry.contest} />
            </div>
          }
          <div className="col-12">
            <h3>{entry.title}</h3>
            <h4 className="h5 mb-4">By {entry.name}</h4>
            <div dangerouslySetInnerHTML={{ __html: decodeEntities(entry.content) }}></div>
            <div className="unsealed-signature">
              <span className="script-font h1">{entry.signature || entry.name}</span>
            </div>
            {
              entry.blog_link && (
                <p className="mt-5">Check out {`${entry.name}${entry.name.slice(-1) === 's' ? '' : '\'s'}`} site <a className="font-weight-bold" href={entry.blog_link}>here</a></p>
              )
            }
            <div className={`upvote-wrapper position-relative mt-4${!canVote ? ' disabled' : ''}`}>
              {
                entry.voting_open  
                ? (
                    <>
                      <p>Voting ends on { entry.end_vote_date }</p>
                      {
                        entry.tease && (
                          userEmail
                          ? <p className="font-weight-bold teal mb-3">{ entry.tease }</p>
                          : <a className="font-weight-bold teal mb-3 d-block" href="#" onClick={this.handleVote}>{ entry.tease_logged_out }</a>
                        )
                      }
                    </>
                  )
                : <p className="font-weight-bold teal mb-3">Voting has ended for this letter.</p>
              }
              <div className={`upvote-button upvote-voted d-flex align-items-center px-0 font-weight-bold${ voted ? ' voted' : '' }`}><img src={HeartVoted} alt="upvote"/><span>+{myVote}</span></div>
              <button style={{ opacity: entry.voting_open ? '1' : '.3', pointerEvents: entry.voting_open ? 'auto' : 'none' }}className="bg-transparent text-dark upvote-button d-flex align-items-center p-0 font-weight-bold" onClick={this.handleVote} disabled={!entry.voting_open}>
                <svg width="109" height="109" viewBox="0 0 109 109" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <circle cx="54.5" cy="54.5" r="54.5" fill="black"/>
                  <g clipPath="url(#clip0)">
                    <path d="M53.0982 85.787C53.4683 86.165 53.9723 86.375 54.5 86.375C55.0276 86.375 55.5316 86.165 55.9017 85.787L81.086 60.2694C91.5886 49.6302 84.1257 31.25 69.1895 31.25C60.2198 31.25 56.1038 37.8414 54.5 39.0725C52.8882 37.8361 48.7985 31.25 39.8105 31.25C24.9215 31.25 17.3667 49.583 27.9166 60.2694L53.0982 85.787Z" fill="#6AC8C2"/>
                  </g>
                  <defs>
                    <clipPath id="clip0">
                      <rect width="63" height="63" fill="white" transform="translate(23 26)"/>
                    </clipPath>
                  </defs>
                </svg>
                <span className="ml-2">{voteCount === 0 ? 'No votes yet' : `${voteCount} vote${voteCount === 1 ? '' : 's'}`}</span>
              </button>
            </div>
          </div>
          <div className="col-12">
            <div className="social-share-links mt-4 row align-items-center pull-right">
              <div className="col-auto">
                <span className="script-font h3 share-label mr-2">Share this submission</span>
              </div>
              <div className="social-link-wrapper col-auto">
                <FacebookShareButton url={entryUrl} quote={entry.excerpt ? entry.excerpt : ""}>
                  <FacebookIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </FacebookShareButton>
                <FacebookShareCount url={entryUrl}>
                  {shareCount => <span className="myShareCountWrapper">{shareCount}</span>}
                </FacebookShareCount>
              </div>
              <div className="social-link-wrapper col-auto">
                <LinkedinShareButton url={entryUrl} title={entry.title} summary={entry.excerpt ? entry.excerpt : ""} source="https://theunsealed.com">
                  <LinkedinIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </LinkedinShareButton>
              </div>
              <div className="social-link-wrapper col-auto">
                <TwitterShareButton url={entryUrl} title={entry.title}>
                  <TwitterIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </TwitterShareButton>
              </div>
              <div className="social-link-wrapper col-auto">
                <EmailShareButton url={entryUrl} subject={"I thought you might like this post from The Unsealed"} separator="&nbsp;" body={`${entry.title} - ${entry.excerpt ? entry.excerpt : ''}`}>
                  <EmailIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </EmailShareButton>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Entry.propTypes = {

}

export default connect((state) => ({
  entries: state.entries,
  loadingEntries: state.loadingEntries,
  selectedContest: state.selectedContest,
  userEmail: state.userEmail,
  showModal: state.showModal
}))(Entry)
