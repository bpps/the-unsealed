import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { selectContest } from '../actions/selectedContest'
import { decodeEntities } from '../utils'

class Contest extends Component {
  constructor (props) {
    super(props)
    this.handleSelectContest = this.handleSelectContest.bind(this)
  }

  handleSelectContest (contest) {
    console.log(contest)
    if ( contest === this.props.selectedContest ) return
    this.props.dispatch( selectContest(contest) )
  }

  render () {
    const { contest, selectedContest } = this.props
    return (
      <div className="btn-row">
        <button
          className={`btn-brush-hover teal font-weight-bold btn${ contest.id === selectedContest ? ' selected' : '' }`}
          key={ `contest-${contest.id}` }
          onClick={ () => this.handleSelectContest(contest.id) }>
          <span dangerouslySetInnerHTML={{ __html: decodeEntities(contest.title) }}></span>
        </button>
      </div>
    )
  }
}

export default connect((state) => ({
  selectedContest: state.selectedContest
}))(Contest)
