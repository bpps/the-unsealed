import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Contest from './Contest'
import { selectContest } from '../actions/selectedContest'

class Contests extends Component {
  constructor (props) {
    super(props)
    this.handleSelectContest = this.handleSelectContest.bind(this)
  }

  handleSelectContest (entry) {
    if ( entry === this.props.selectedContest ) return
    this.props.dispatch( selectContest(entry) )
  }

  render () {
    const { selectedContest } = this.props
    console.log(contests)
    return (
      <div className="col-md-3 terms-column">
        <div className="filter-header py-3 d-flex justify-content-center align-items-center d-md-none">
          <span className="h1 script-font teal mr-3 mb-0">Contests</span>
          <select className="filter-terms underlined py-1" value={selectedContest || ''} onChange={ entry => this.handleSelectContest(entry.target.value) }>
            <option value="disabled entry" disabled>Choose a entry</option>
            <option value="">All stories</option>
            {
              contests.map( contest => {
                return (
                  <option key={`select-contest-${contest.ID}`} value={contest.ID}>
                    {contest.post_title}
                  </option>
                )
              })
            }
          </select>
        </div>
        <div className="d-none d-md-block py-3">
          <div className="btn-row">
            <button
              className={`btn-brush-hover btn${ null === selectedContest ? ' selected' : '' }`}
              key="all-letters"
              onClick={ () => this.handleSelectContest('') }>
              <span>All stories</span>
            </button>
          </div>
          {
            contests.map( contest => {
              return (
                <div className="btn-row" key={`entry-${contest.ID}`}>
                  <button
                    className={`btn-brush-hover btn${ contest.ID === selectedContest ? ' selected' : '' }`}
                    key={ `contest-${contest.ID}` }
                    onClick={ () => this.handleSelectContest(contest.ID) }>
                    <span>{ contest.post_title }</span>
                  </button>
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }
}

Contests.propTypes = {

}

export default connect((state) => ({
  selectedContest: state.selectedContest
}))(Contests)
