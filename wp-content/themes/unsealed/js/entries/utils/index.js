import axios from 'axios'

export var decodeEntities = (function() {
  var cache = {},
      character,
      e = document.createElement('div');

  return function(html) {
    return html.replace(/([&][^&; ]+[;])/g, function(entity) {
      character = cache[entity];
			if (!character) {
        e.innerHTML = entity;
        if (e.childNodes[0])
          character = cache[entity] = e.childNodes[0].nodeValue;
        else
          character = '';
      }
      return character;
    });
  };
})();

export var objectToFormData = (obj, form, namespace) => {
  var fd = form || new FormData();
  var formKey;
  for(var property in obj) {
    if(obj.hasOwnProperty(property)) {
      if(namespace) {
        formKey = namespace + '[' + property + ']';
      } else {
        formKey = property;
      }
      // if the property is an object, but not a File,
      // use recursivity.
      if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
        objectToFormData(obj[property], fd, property);
      } else {
        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }
    }
  }
  return fd;
};


export function fetchEntries (contest, paged = 1, exclude = '') {
  let data = {
    action: 'do_ajax',
    fn : 'get_entries',
    dataType: 'json',
    contest: contest,
    paged: paged
  }
  if ( exclude != '' ) {
    data.exclude = exclude
  }
  return axios.post( ajaxurl, objectToFormData(data) )
  .then(results => {
    return results.data
  })
  .catch( error => error )
}

export function fetchEntryById (id) {
  let data = {
    action: 'do_ajax',
    fn : 'get_entry_by_id',
    dataType: 'json',
    id
  }
  return axios.post( ajaxurl, objectToFormData(data) )
  .then(results => {
    return results.data
  })
  .catch( error => error )
}

export function upvoteEntry (id, email, voteCount) {
  let data = {
    action: 'do_ajax',
    fn : 'upvote_entry',
    dataType: 'json',
    id: id,
    email: email,
    vote_count: voteCount
  }
  return axios.post( ajaxurl, objectToFormData(data) )
  .then(results => {
    return results.data
  })
  .catch( error => error )
}

export async function signupEmail (email) {
  let data = {
    action: 'do_ajax',
    fn : 'sign_up_user',
    dataType: 'json',
    email: email
  }
  return axios.post( ajaxurl, objectToFormData(data) )
  .then(results => {
    return results.data
  })
  .catch( error => error )
}

export async function verificationEmail(email) {
  let data = {
    action: 'do_ajax',
    fn: 'verification_email',
    dataType: 'json',
    email: email
  }
  return axios.post(ajaxurl, objectToFormData(data)).then (results=>{
    return results
  })
  .catch(error => error)
}

export async function checkVerificationCode(email, code) {
  let data = {
    action: 'do_ajax',
    fn: 'verfication_code',
    dataType: 'json',
    email: email,
    code: code
  }
  return axios.post(ajaxurl, objectToFormData(data)).then (results=>{
    return results
  })
  .catch(error => error)
}
