import React, { useState, useEffect } from 'react'
import Contests from './components/Contests'
import Entries from './components/Entries'
import { Helmet } from "react-helmet";
import { connect } from 'react-redux'
import Modal from 'react-modal';
import { toggleModal, checkEmail, verficationEmail, checkVerificationCode } from './actions/userEmail'


function App(props) {
  const { entries, showModal, verifyStep } = props
  const basename = props.history.createHref({pathname: '/'})
  const [email, setEmail] = useState(0);
  const [code, setCode] = useState('');
  console.log('verifyStep = ', verifyStep);

  function closeModal() {
    props.dispatch(toggleModal(false))
  }

  async function submitEmail(e) {
    e.preventDefault()
    await props.dispatch(verficationEmail(email));
  }

  function verifyCode(e) {
    e.preventDefault()
    props.dispatch(checkVerificationCode(email, code));
    
  }

  var subscribeSpans = []
  var subscribeText = []
  for (let y = 0; y < 30; y++) {
    subscribeText.push('Subscribe ')
  }
  for (var x = 0; x < 3; x++) {
    subscribeSpans.push(<span key={`subscribe-${x}`}>{subscribeText}</span>)
  }

  var loginSpans = []
  var loginText = []
  for (let y = 0; y < 30; y++) {
    loginText.push('Login ')
  }
  for (var x = 0; x < 3; x++) {
    loginSpans.push(<span key={`login-${x}`}>{loginText}</span>)
  }

  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      maxWidth: '600px',
      width: '100%',
      padding: 0
    },
    overlay: {
      backgroundColor: 'rgba(0,0,0,.4)'
    }
  };

  return (
    <div className="entries-app pb-5 container-wide">
      <Helmet>
        {
          entries.length && <title>The Unsealed - { entries[0].title }</title>
        }
        {
          entries.length && <meta property="og:url" content={basename + entries[0].slug} />
        }
        <link rel="canonical" href="" />
      </Helmet>
      <div className="row">
        <Contests/>
        <Entries location={props.location} history={props.history}/>
      </div>
      <Modal
        isOpen={showModal}
        // onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Email signup"
      >
        <div className="modal-content-wrapper position-relative p-3">
          <button className="close" onClick={closeModal}><span aria-hidden="true">&times;</span></button>
          <div className="py-5 text-center">
            <div className="py-4">
              <h2 className="h3 mb-4">Become a member or log in to vote</h2>
              <div>
                <div className="row justify-content-center">
                  <div className="col-auto">
                    <a className="btn-unsealed small" href={`${siteurl}/login?redirect_to=${siteurl}/contest-submissions${props.location.pathname}`}>
                      <span className="btn-unsealed-text">Login</span>
                      <div className="btn-background-text" aria-hidden="true">{ loginSpans }</div>
                    </a>
                  </div>
                  <div className="col-auto">
                    <a className="btn-unsealed small" href={`${siteurl}/account/sign-up?redirect_to=${siteurl}/contest-submissions${props.location.pathname}`}>
                      <span className="btn-unsealed-text">Subscribe</span>
                      <div className="btn-background-text" aria-hidden="true">{ subscribeSpans }</div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            {verifyStep.step === 'email' && (
              <div className="pt-4">
                <h3 className="mb-4 h4">Or sign up for our newsletter</h3>
                <form onSubmit={ e => submitEmail(e) }>
                  <input className="p-1" onChange={ e => setEmail(e.target.value)} type="email"/>
                  <button className="ml-2" type="submit">submit</button>
                </form>
                {verifyStep.error && (
                  <small>{verifyStep.error}</small>
                )}
              </div>
            )}

            {verifyStep.step === 'verify_code' && (
              <div className="pt-4">
                <h3 className="mb-4 h5">Enter the verification code that was sent to your email</h3>
                <form onSubmit={ e => verifyCode(e) }>
                  <input className="p-1" onChange={ e => setCode(e.target.value)} type="text" placeholder="Verification code" />
                  <input type="hidden" value={code} />
                  <button className="ml-2" type="submit">Verify</button>
                  <button className="ml-2" type="button" onClick={e => submitEmail(e)}>Resend Code</button>
                </form>
                {verifyStep.error && (
                  <small>{verifyStep.error}</small>
                )}
              </div>
            )}
          </div>
        </div>
      </Modal>
    </div>
  );
}

export default connect((state) => ({
  entries: state.entries,
  showModal: state.showModal,
  verifyStep: state.verifyStep
}))(App)
