import $ from 'jquery';

$(function() {
    $('.upvote-btn').on('click', function () {
        const entryId = $(this).data('entry-id')
        const email = getCookie('vote_email')
        $.ajax({
            url: ajaxurl,
            data: {
                action: 'do_ajax',
                fn: 'upvote_entry',
                id: entryId,
                email: email ? email : null 
            },
            success: function (data) {
                console.log(data)
                $(`#contest-entry-${entryId} .upvote-button`).addClass('voted')
                $(`#contest-entry-${entryId} .vote-count`).html()
                $(`#contest-entry-${entryId} .upvote-button`).attr('disabled', true).css('pointer-events', 'none')
                $(`#contest-entry-${entryId} .upvote-wrapper`).addClass('disabled')
                setTimeout( function () {
                    $(`#contest-entry-${entryId} .upvote-button`).removeClass('voted')
                }, 3000)
            },
            complete: function (data) {
                
            },
            error: function (errorThrown) {
                console.log(errorThrown);
            },
        });
    })

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }
})