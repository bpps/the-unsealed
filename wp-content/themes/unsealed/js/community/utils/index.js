import axios from 'axios'

export var decodeEntities = (function() {
  var cache = {},
      character,
      e = document.createElement('div');

  return function(html) {
    return html.replace(/([&][^&; ]+[;])/g, function(entity) {
      character = cache[entity];
			if (!character) {
        e.innerHTML = entity;
        if (e.childNodes[0])
          character = cache[entity] = e.childNodes[0].nodeValue;
        else
          character = '';
      }
      return character;
    });
  };
})();

export var objectToFormData = (obj, form, namespace) => {
  var fd = form || new FormData();
  var formKey;
  for(var property in obj) {
    if(obj.hasOwnProperty(property)) {
      if(namespace) {
        formKey = namespace + '[' + property + ']';
      } else {
        formKey = property;
      }
      // if the property is an object, but not a File,
      // use recursivity.
      if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
        objectToFormData(obj[property], fd, property);
      } else {
        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }
    }
  }
  return fd;
};


export function fetchLetters (topic, paged = 1, exclude = '') {
  let data = {
    action: 'do_ajax',
    fn : 'get_letters',
    dataType: 'json',
    topic: topic,
    paged: paged,
    postType: 'community-voices'
  }
  if ( exclude != '' ) {
    data.exclude = exclude
  }
  return axios.post( ajaxurl, objectToFormData(data) )
  .then(results => {
    return results.data
  })
  .catch( error => error )
}

export function fetchLetterBySlug (slug) {
  let data = {
    action: 'do_ajax',
    fn : 'get_letter_by_slug',
    dataType: 'json',
    postType: 'community-voices',
    slug: slug
  }
  return axios.post( ajaxurl, objectToFormData(data) )
  .then(results => {
    return results.data
  })
  .catch( error => error )
}
