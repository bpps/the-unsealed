export const SELECT_TOPIC = 'SELECT_TOPIC'

export const selectTopic = selectedTopic => {
  return {
    type: SELECT_TOPIC,
    selectedTopic
  }
}
