export const SET_LETTERS = 'SET_LETTERS'

export const setLetters = (letters, merge) => {
  return {
    type: SET_LETTERS,
    letters,
    merge
  }
}
