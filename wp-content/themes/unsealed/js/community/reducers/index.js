import { combineReducers } from 'redux'
import { letters, loadingLetters } from './letters'
import { selectedTopic } from './selectedTopic'

export default combineReducers({
  letters,
  loadingLetters,
  selectedTopic
})
