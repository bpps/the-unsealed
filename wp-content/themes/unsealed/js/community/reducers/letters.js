import { SET_LETTERS } from '../actions/letters'
import { SELECT_TOPIC } from '../actions/selectedTopic'

export const letters = (state = [], action) => {
  switch(action.type) {
    case SET_LETTERS:
      if ( action.merge ) {
        return [...state, ...action.letters]
      } else {
        return action.letters
      }
    default:
      return state
  }
}

export const loadingLetters = (state = false, action) => {
  switch(action.type) {
    case SELECT_TOPIC:
      return true
    case SET_LETTERS:
      return false
    default:
      return state
  }
}
