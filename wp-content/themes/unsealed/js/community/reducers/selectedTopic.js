import { SELECT_TOPIC } from '../actions/selectedTopic'

export const selectedTopic = (state = null, action) => {
  switch(action.type) {
    case SELECT_TOPIC:
      return action.selectedTopic
    default:
      return state
  }
}
