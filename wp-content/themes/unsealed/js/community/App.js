import React from 'react'
import Topics from './components/Topics'
import Letters from './components/Letters'
import { Helmet } from "react-helmet";
import { connect } from 'react-redux'

function App(props) {
  const { letters } = props
  const basename = props.history.createHref({pathname: '/'})
  return (
    <div className="letters-app pb-5 container-wide">
      <Helmet>
        {
          letters.length && <title>The Unsealed - { letters[0].title }</title>
        }
        {
          letters.length && <meta property="og:url" content={basename + letters[0].slug} />
        }
        <link rel="canonical" href="" />
      </Helmet>
      <div className="row">
        <Topics/>
        <Letters location={props.location} history={props.history}/>
      </div>
    </div>
  );
}

export default connect((state) => ({
  letters: state.letters
}))(App)
