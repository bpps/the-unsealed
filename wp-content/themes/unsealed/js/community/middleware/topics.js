import { SELECT_TOPIC } from '../actions/selectedTopic'
import { SET_LETTERS } from '../actions/letters'
import { fetchLetters } from '../utils'

const getTopics = store => next => action => {
  next(action)
  switch (action.type) {
    case SELECT_TOPIC:
      fetchLetters( action.selectedTopic, 1 ).then( letters_obj => {
        const letters = letters_obj.letters
        next({
          type: SET_LETTERS,
          letters,
          merge: false
        })
      })
    break
    default:
    break
  }
}

export default getTopics
