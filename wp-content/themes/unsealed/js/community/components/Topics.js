import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Topic from './Topic'
import { selectTopic } from '../actions/selectedTopic'

class Topics extends Component {
  constructor (props) {
    super(props)
    this.handleSelectTopic = this.handleSelectTopic.bind(this)
  }

  handleSelectTopic (topic) {
    if ( topic === this.props.selectedTopic ) return
    this.props.dispatch( selectTopic(topic) )
  }

  render () {
    const { selectedTopic } = this.props
    return (
      <div className="col-md-3 terms-column">
        <div className="filter-header py-3 d-flex justify-content-center align-items-center d-md-none">
          <span className="h1 script-font teal mr-3 mb-0">Topics</span>
          <select className="filter-terms underlined py-1" value={selectedTopic || ''} onChange={ topic => this.handleSelectTopic(topic.target.value) }>
            <option value="disabled topic" disabled>Choose a topic</option>
            <option value="">All stories</option>
            {
              topics.map( topic => {
                return (
                  <option key={`select-topic-${topic.term_id}`} value={topic.term_id}>
                    {topic.name}
                  </option>
                )
              })
            }
          </select>
        </div>
        <div className="d-none d-md-block py-3">
          <div className="btn-row">
            <button
              className={`btn-brush-hover btn${ null === selectedTopic ? ' selected' : '' }`}
              key="all-letters"
              onClick={ () => this.handleSelectTopic('') }>
              <span>All stories</span>
            </button>
          </div>
          {
            topics.map( topic => {
              return (
                <div className="btn-row" key={`topic-${topic.term_id}`}>
                  <button
                    className={`btn-brush-hover btn${ topic.slug === selectedTopic ? ' selected' : '' }`}
                    key={ `topic-${topic.term_id}` }
                    onClick={ () => this.handleSelectTopic(topic.slug) }>
                    <span>{ topic.name }</span>
                  </button>
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }
}

Topics.propTypes = {

}

export default connect((state) => ({
  selectedTopic: state.selectedTopic
}))(Topics)
