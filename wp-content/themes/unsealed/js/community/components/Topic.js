import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { selectTopic } from '../actions/selectedTopic'

class Topic extends Component {
  constructor (props) {
    super(props)
    this.handleSelectTopic = this.handleSelectTopic.bind(this)
  }

  handleSelectTopic (topic) {
    if ( topic === this.props.selectedTopic ) return
    this.props.dispatch( selectTopic(topic) )
  }

  render () {
    const { topic, selectedTopic } = this.props
    return (
      <div className="btn-row">
        <button
          className={`btn-brush-hover btn${ topic.term_id === selectedTopic ? ' selected' : '' }`}
          key={ `topic-${topic.term_id}` }
          onClick={ () => this.handleSelectTopic(topic.term_id) }>
          <span>{ topic.name }</span>
        </button>
      </div>
    )
  }
}

export default connect((state) => ({
  selectedTopic: state.selectedTopic
}))(Topic)
