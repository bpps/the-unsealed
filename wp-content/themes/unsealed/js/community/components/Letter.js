import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Topic from './Topic'
import VizSensor from 'react-visibility-sensor';
import { Helmet } from "react-helmet";
import ReactDOMServer from 'react-dom/server';
import {
  EmailShareButton,
  EmailIcon,
  FacebookShareButton,
  FacebookShareCount,
  FacebookIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TwitterShareButton,
  TwitterIcon
} from "react-share";
import { decodeEntities } from '../utils'

class Letter extends Component {
  constructor (props) {
    super(props)
    this.handleLetterActive = this.handleLetterActive.bind(this)
    this.state = { isVisible: props.isFirst }
  }

  handleLetterActive (isVisible) {
    const { letter, history } = this.props
    if ( isVisible ) history.push(`/${letter.slug}`)
    this.setState({
      isVisible: isVisible
    })
  }

  render () {
    const { letter, selectedTopic } = this.props
    const basename = this.props.history.createHref({pathname: '/'})
    const letterUrl = `${siteurl}${basename}${letter.slug}`
    return (
      <div className="letter border-bottom pt-4 pb-5">
        {
          this.state.isVisible &&
          <Helmet>
            <meta charSet="utf-8" />
            <title>{letter.title}</title>
            <link rel="canonical" href={letterUrl} />
            <meta property="og:url" content={letterUrl} />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={letter.title} />
            <meta property="og:description" content={letter.excerpt ? letter.excerpt : ''} />
            <meta property="og:image" content={letter.featured_image ? letter.featured_image : ''} />
          </Helmet>
        }
        <VizSensor
          key={letter.id}
          onChange={isVisible => this.handleLetterActive(isVisible)}>
          <div className="viz-sensor"></div>
        </VizSensor>
        <div className="row">
          {
            (letter.terms && selectedTopic === null) &&
            <div className="container topics-row row">
              {
                letter.terms.map( term => {
                  return <Topic key={`letter-${letter.id}-term-${term.term_id}`} topic={term} />
                })
              }
            </div>
          }
          <div className="col-12">
            <h3 className="mb-5">{letter.title }</h3>
            <div className="font-weight-bold" dangerouslySetInnerHTML={{ __html: decodeEntities(letter.content) }}></div>
          </div>
          <div className="col-12">
            <div className="social-share-links mt-4 row align-items-center pull-right">
              <div className="col-auto">
                <span className="script-font h3 share-label mr-2">Share this story</span>
              </div>
              <div className="social-link-wrapper col-auto">
                <FacebookShareButton url={letterUrl} quote={letter.excerpt ? letter.excerpt : ""}>
                  <FacebookIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </FacebookShareButton>
                <FacebookShareCount url={letterUrl}>
                  {shareCount => <span className="myShareCountWrapper">{shareCount}</span>}
                </FacebookShareCount>
              </div>
              <div className="social-link-wrapper col-auto">
                <LinkedinShareButton url={letterUrl} title={letter.title} summary={letter.excerpt ? letter.excerpt : ""} source="https://theunsealed.com">
                  <LinkedinIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </LinkedinShareButton>
              </div>
              <div className="social-link-wrapper col-auto">
                <TwitterShareButton url={letterUrl} title={letter.title}>
                  <TwitterIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </TwitterShareButton>
              </div>
              <div className="social-link-wrapper col-auto">
                <EmailShareButton url={letterUrl} subject={"I thought you might like this post from The Unsealed"} separator="&nbsp;" body={`${letter.title} - ${letter.excerpt ? letter.excerpt : ''}`}>
                  <EmailIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </EmailShareButton>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Letter.propTypes = {

}

export default connect((state) => ({
  letters: state.letters,
  loadingLetters: state.loadingLetters,
  selectedTopic: state.selectedTopic
}))(Letter)
