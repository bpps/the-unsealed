import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchLetters, fetchLetterBySlug } from '../utils'
import { loadLetters, setLetters } from '../actions/letters'
import Letter from './Letter'

class Letters extends Component {
  constructor (props) {
    super(props)
    this.handleScroll = this.handleScroll.bind(this)
    this.getMoreLetters = this.getMoreLetters.bind(this)
    this.state = {
      paged: 1,
      fetching: true,
      endOfFeed: false,
      initialPost: ''
    }
  }

  componentDidMount () {
    document.addEventListener('scroll', this.handleScroll);
    const { dispatch, history, location } = this.props
    const currentPath = this.props.location.pathname.replace('/', '')
    if ( currentPath != '' ) {
      // Get url slug as first post
      fetchLetterBySlug(currentPath).then( firstLetter => {
        fetchLetters(false, 1, currentPath).then( letters_obj => {
          const allLetters = [...firstLetter, ...letters_obj.letters]
          dispatch( setLetters(allLetters, false) )
          this.setState({ fetching: false, paged: 2, initialPost: currentPath, endOfFeed: !letters_obj.has_more_letters })
        })
      })
    } else {
      fetchLetters(false, 1, currentPath).then( letters_obj => {
        dispatch( setLetters(letters_obj.letters, false) )
        this.setState({ fetching: false, paged: 2, endOfFeed: !letters_obj.has_more_letters })
      })
    }

  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleScroll);
  }

  shouldComponentUpdate (nextProps, nextState) {
    if ( nextProps.selectedTopic != this.props.selectedTopic ) {
      const { dispatch } = this.props
      this.setState({ fetching: true })
      fetchLetters( nextProps.selectedTopic, 1 ).then( letters_obj => {
        dispatch( setLetters(letters_obj.letters, false) )
        this.setState({ paged: 2, fetching: false, endOfFeed: !letters_obj.has_more_letters, initialPost: '' })
      })
    }
    return true
  }

  getMoreLetters () {
    const { dispatch, selectedTopic } = this.props
    fetchLetters(selectedTopic, this.state.paged, this.state.initialPost).then( letters_obj => {
      // Stop updating if no letters are returned.
      // NOTE: this can be changed to have the function return a bool if there are more letters
      dispatch( setLetters(letters_obj.letters, true) )
      this.setState({ fetching: false, paged: this.state.paged + 1, endOfFeed: !letters_obj.has_more_letters })
    })
  }

  handleScroll () {
    if ( this.state.fetching || this.state.endOfFeed ) return
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset + (docHeight / 3);
    console.log('getting more letters')
    if (windowBottom >= docHeight) {
      this.setState({
        fetching: true
      }, this.getMoreLetters() )
    }
  }

  render () {
    const { letters, loadingLetters, selectedTopic, history } = this.props
    const { fetching, endOfFeed } = this.state
    return (
      <div className="col-md-9 col-lg-7">
        <div className="letters">
          {
            !loadingLetters &&
            letters.map( (letter, ind) => {
              return (
                <Letter key={`letter-${letter.id}`} isFirst={ind === 0} letter={letter} history={history}/>
              )
            })
          }
          {
            ( loadingLetters || (fetching && !endOfFeed) ) && <div className="py-3 loading d-flex justify-content-center"><span>Loading</span></div>
          }
          {
            (letters.length === 0 && !loadingLetters) &&
            <p>No letters</p>
          }
        </div>
      </div>
    )
  }
}

Letters.propTypes = {

}

export default connect((state) => ({
  letters: state.letters,
  loadingLetters: state.loadingLetters,
  selectedTopic: state.selectedTopic
}))(Letters)
