import $ from 'jquery';
import 'bootstrap'
import 'slick-carousel';
import sal from 'sal.js'
import Jcrop from 'jcrop';
import Cookies from 'js-cookie';

sal();

$(function() {
	const meetLaurenImages = document.querySelector('.meet-lauren-images');
	if ( meetLaurenImages ) {
		meetLaurenImages.addEventListener('sal:in', ({ detail }) => {
			detail.target.classList.add('active')
		});
		meetLaurenImages.addEventListener('sal:out', ({ detail }) => {
			detail.target.classList.remove('active')
		});
	}

	if ( $('.inline-video') ) {
		$('.inline-video .play-icon').on('click', (e) => {
			$(e.currentTarget).css({ opacity: 0, 'pointer-events' : 'none' })
			const $video = $(e.currentTarget).parent().find('video').get(0)
			$($video).attr('controls', true)
			$video.play()
		})
	}

	$('.menu-has-children>a').on('click', function (e) {
		if ( $(window).width() < 1145 && !$(e.currentTarget).hasClass('sub-menu-item') ) {
			e.preventDefault()
			$(e.currentTarget).parent().find('.dropdown-menu').slideToggle()
		}
	})

	$(document).ready( function () {
		const urlParams = new URLSearchParams(window.location.search);
		const videoId = urlParams.get('video_id')
		if ( videoId ) {
			$(`[href="#${videoId}"]`).click()
		}
	})

	if ( $('.video-modal') ) {
		let videoSrc = '';
		let videoType = '';
		$(document).on('show.bs.modal','.video-modal', async function (e) {
			const videoId = $(e.relatedTarget).data('video-id');
			$('.video-share-links').html($(`#video-post-${videoId} .video-share`).html())
			a2a.init()
			const hasAccess = $(e.relatedTarget).data('has-access');
			if ( hasAccess !== undefined && hasAccess !== 1 ) {
				const thumbnail = $(e.relatedTarget).data('thumbnail')

				$('.popup-login', e.target).css({ 'background-image': `url(${thumbnail})`, 'display': 'flex' })
				return
			}
			videoSrc = $(e.relatedTarget).data('video-src') ? $(e.relatedTarget).data('video-src') : $(e.target).data('video-src');
			videoType = $(e.relatedTarget).data('video-type') ? $(e.relatedTarget).data('video-type') : $(e.target).data('video-type')
			const autoPlay = $(e.relatedTarget).data('autoplay') ? $(e.relatedTarget).data('autoplay') : $(e.target).data('autoplay')
			const $video = $('.video', e.currentTarget)
			if ( videoType === 'upload' ) {
				$('.video source', e.currentTarget).attr('src', videoSrc);
			} else {
				videoSrc = videoSrc.replace("watch?v=", "embed/");
				$('.video-iframe', e.currentTarget).attr('src', videoSrc + '?autoplay=1');
			}
			fbq('trackCustom', 'ViewVideo', { video: videoSrc });
			$video[0].load()

			$('.play-button', e.target).hide()
			if ( !autoPlay && autoPlay != false ) {
				try {
			    await $video[0].play();
			  } catch(err) {
					if ( videoType === 'upload' ) {
							$('.play-button', e.target).show()
					}
			  }
			}
			if ( $('.play-button', e.target) ) {
				$('.play-button', e.target).on('click', () => {
					$('.play-button', e.target).hide()
					$video[0].play()
				})
			}
		});

		// stop playing the youtube video when I close the modal
		$('.video-modal').on('hide.bs.modal', function (e) {
			// clear video share links
			$('.video-share-links').html('')
			// a poor man's stop video
			$('.popup-login', e.target).css({ 'display': 'none' })
			if ( videoType === 'upload' ) {
				const $video = $('.video', e.currentTarget)
				$video[0].pause()
				setTimeout ( () => {
					$('.video source', e.currentTarget).attr('src', '');
					$('.video', e.currentTarget).removeAttr('autoplay')
					videoSrc = ''
				}, 1000)
			} else {
				$('.video-iframe', e.currentTarget).attr('src', '');
			}
		});
	}

	if ( $('.redirect-video').length ) {
		setTimeout( function() {

			window.location.href = $('.redirect-video').data('redirect');
		}, 300)
	}

	if ( $('#non-member-video-popup') ) {
		$('#non-member-video-popup').modal({ show: false })
		$(document).mouseleave(function() {
			if ( !getCookie('non-member-video') && !$('#subscribe-popup').hasClass('show') ) {
				$('#non-member-video-popup').modal('show')
				setCookie('non-member-video', 1, 30)
			}
		})
	}

	if ( $('#subscribe-popup') ) {
		const timedelay = $('#subscribe-popup').data('timeout')
		setTimeout( () => {
			if ( !$('#non-member-video-popup').hasClass('show') ) {
				$('#subscribe-popup').modal('show')
			}
		}, timedelay)

		$(document).on('show.bs.modal','.subscribe-modal', function (e) {
			setCookie('subscribe-fortune', '1', 30)
		})

		$('#subscribe-popup-form').submit(function (e) {
			e.preventDefault();
			let email = $('#subscribe-popup-form input[type="email"]').val();
			$('.subscribe-popup-body').addClass('inactive')
			if ( validateEmail(email) ) {
				$.ajax({
					url: ajaxurl,
					data: {
						action: 'do_ajax',
						fn: 'sign_up_user',
						signup_type: 'fortune',
						email: email,
					},
					success: function (data) {
						if (data !== null) {
							$('#subscribe-popup .email-response').append(JSON.parse(data).response);
							$('.subscribe-popup-body').slideUp();
							setTimeout( () => {
								$('#subscribe-popup .email-response').slideDown();
							}, 300)
						}
					},
					complete: function (data) {
						$('.subscribe-popup-body').removeClass('inactive')
					},
					error: function (errorThrown) {
						console.log(errorThrown);
					},
				});
			}
		});
	}

	function setCookie(name,value,days) {
		var expires = "";
		if (days) {
				var date = new Date();
				date.setTime(date.getTime() + (days*24*60*60*1000));
				expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + (value || "") + expires + "; Secure; SameSite; path=/";
	}

	function getCookie(name) {
			var nameEQ = name + "=";
			var ca = document.cookie.split(';');
			for(var i=0;i < ca.length;i++) {
					var c = ca[i];
					while (c.charAt(0)==' ') c = c.substring(1,c.length);
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
			}
			return null;
	}

	function validateEmail(email) {
	    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(String(email).toLowerCase());
	}

	function validateURL(str) {
	  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
	    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
	    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
	    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
	    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
	    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
	  return !!pattern.test(str);
	}

	if ( $('.menu-search button') ) {
		$('.menu-search button').on('click', function () {
			$('#search-bar').slideToggle("500")
			if ( $('#search-bar').is(':visible') ) {
				$('#search').focus()
			} else {
				$('#search').blur()
			}
		})
	}

	function toggleSearch() {

	}

	$('.navbar-toggler').click(function () {
		$('.menu-ham').toggleClass('open');
		if ( !$('.menu-ham').hasClass('open') ) {
			$('#search-bar').slideUp("500")
			$('#search').blur()
		}
	});

	$('#footer-email-form').submit(function (e) {
		e.preventDefault();
		let email = $('#footer-email-form input[type="email"]').val();
		if ( validateEmail(email) ) {
			$.ajax({
				url: ajaxurl,
				data: {
					action: 'do_ajax',
					fn: 'sign_up_user',
					email: email,
				},
				success: function (data) {
					if (data !== null) {
						setCookie('subscribe-fortune', '1', 30)
						$('footer-email-form-wrapper .email-response').append(JSON.parse(data).response);
						$('#footer-email-form').fadeOut(null, function () {
							$('footer-email-form-wrapper .email-response').fadeIn();
						});
					}
				},
				complete: function (data) {},
				error: function (errorThrown) {
					console.log(errorThrown);
				},
			});
		}
	});

	$('.show-comments').click(function () {
		$('.all-comments').slideDown();
	});

	if ( $('.image-grid') ) {
		$('.parallax').mousemove(function (e) {
			$('.image-grid img').each(function (i, b) {
				let scale = 1;
				switch (i) {
					case 0:
						scale = 2;
						break;
					case 1:
						scale = 7;
						break;
					case 2:
						scale = 4;
						break;
					case 3:
						scale = 9;
						break;
					case 4:
						scale = 1.5;
						break;
				}
				let $this = $(this);
				const x = ($(window).width() - e.pageX * scale) / 90;
				const y = ($(window).height() - e.pageY * scale) / 90;
				$this.css('transform', `translate(${x}px, ${y}px)`);
			});
		});
	}

	if ( $('.floating-prompts') ) {
		var prompts = document.getElementsByClassName('floating-prompt');
		var promptLength = prompts.length;
		setInterval( () => {
			fadePrompt ()
		}, 5000)
		fadePrompt()
		function fadePrompt () {
			// alert("There are "+promptLength+" div tags in the html code");
			var randomPrompt = Math.floor(Math.random()*promptLength);
			$('.floating-prompt').fadeOut(1000)
			setTimeout( () => {
				$('.floating-prompt').eq(randomPrompt).css({
					top: getRandomPos(10, 90) + '%',
					left: getRandomPos(10, 90) + '%',
					transform: 'scale(' + getRandomPos(0.8, 2.5) + ')'
				})
				$('.floating-prompt').eq(randomPrompt).fadeIn()
			}, 1000)
		}
	}

	if ( $('.as-seen-logos') ) {
		$('.as-seen-logos').slick({
			arrows: true,
			slidesToShow: 6,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 0,
			speed: 8000,
			pauseOnHover: false,
			cssEase: 'linear',
			// lazyLoad: 'ondemand',
			fade: false,
			infinite: true,
			// centerMode: true,
			focusOnSelect: false,
			variableWidth: false,
			// prevArrow : $('.gallery-arrow-prev', $slideshow),
			// nextArrow : $('.gallery-arrow-next', $slideshow),
			responsive: [
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 4,
						// 'arrows': false
					}
				},
				{
					breakpoint: 688,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 496,
					settings: {
						slidesToShow: 2
					}
				}
			]
		});
	}

	function getRandomPos(min, max) {
	  return Math.random() * (max - min) + min;
	}

	// Testimonials Carousel
	$('.testimonials').slick({
		autoplay: true,
		speed: 1500,
		fade: true,
		arrows: true,
		adaptiveHeight: true
	})

	$('.upload-entry-image').on( 'click', function(e) {
		e.preventDefault();

		var button = $(this),
		custom_uploader = wp.media({
			title: 'Insert image',
			library : {
				uploadedTo : wp.media.view.settings.post.id, // attach to the current post?
				type : 'image'
			},
			crop: true,
			button: {
				text: 'Use this image' // button label text
			},
			multiple: false
		}).on('select', function() { // it also has "open" and "close" events
			var attachment = custom_uploader.state().get('selection').first().toJSON();
			button.html('<img src="' + attachment.url + '">').next().val(attachment.id).next().show();
			button.addClass('visible')
			$('[name="entry-img"]').val(attachment.id)
			$('.remove-entry-image').show();
		}).open();
	});

	// on remove button click
	$('.remove-entry-image').on('click', function(e){

		e.preventDefault();

		var button = $(this);
		button.next().val(''); // emptying the hidden field
		button.hide().prev().html('Upload image')
		button.prev().removeClass('visible');
	});

	if ( $('#submit-contest-entry').length ) {
		$('#submit-contest-entry form').on('submit', e => {
			e.preventDefault();
			$('[type="submit"]').val('submitting')
			const title = $('[name="entry-title"]').val()
			let image = $('[name="entry-img"]').val()
			let content = $('[name="entry-content"]').val()
					content = content.replace(/\r?\n/g, '<br />')
			let link = validateURL($('[name="entry-link"]').val()) ? $('[name="entry-link"]').val() : ''
			const contestId = $('#submit-contest-entry form').data('contest-id')
			const subscriberId = $('#submit-contest-entry form').data('member-id')
			$.ajax({
				url: ajaxurl,
				method: 'POST', 
				data: {
					action: 'do_ajax',
					fn: 'submit_contest_entry',
					contest: contestId,
					subscriber: subscriberId,
					title,
					content,
					link,
					image
				},
				success: function (data) {
					$('#submit-contest-entry form').remove();
					$('#submit-contest-entry h2').remove();
					if (data === 'submitted') {
						$('#submit-contest-entry .modal-body').append(`
							<div class="text-center">
								<p>Thank you for your entry!</p>
								<p>We'll reach out when the judges have determined a winner</p>
							</div>
						`)
					} else {
						$('#submit-contest-entry .modal-body').append(`
							<div class="text-center">
								<p>Something went wrong. Please try again later</p>
							</div>
						`)
					}
				},
				complete: function (data) {
					// console.log(data)
				},
				error: function (errorThrown) {
					console.log(errorThrown);
				},
			});
		})
	}


	// Custom JS from BuddyPress

	$(document).ready( function () {

		if ($("#other-user-account-button-wrapper").length) {
			setInterval(function() {
				$('#other-user-account-button-wrapper a.friendship-button[rel="add"]').attr('data-toggle', 'tooltip');
				$('#other-user-account-button-wrapper a.friendship-button[rel="add"]').attr('data-placement', 'top');
				$('#other-user-account-button-wrapper a.friendship-button[rel="add"]').attr('title', 'Send a friend request');

				$('#other-user-account-button-wrapper a.friendship-button[rel="remove"]').attr('data-toggle', 'tooltip');
				$('#other-user-account-button-wrapper a.friendship-button[rel="remove"]').attr('data-placement', 'top');
				$('#other-user-account-button-wrapper a.friendship-button[rel="remove"]').attr('title', 'Cancel a friend request');
				$('[data-toggle="tooltip"]').tooltip()
			}, 100)


			$('#other-user-account-button-wrapper a.activity-button.mention').attr('data-toggle', 'tooltip');
			$('#other-user-account-button-wrapper a.activity-button.mention').attr('data-placement', 'top');
			$('#other-user-account-button-wrapper a.activity-button.mention').attr('title', 'Public Post');

			$('#other-user-account-button-wrapper .send-message').attr('data-toggle', 'tooltip');
			$('#other-user-account-button-wrapper .send-message').attr('data-placement', 'top');
			$('#other-user-account-button-wrapper a.send-message').attr('title', 'Private Message');
		}
		$('[data-toggle="tooltip"]').tooltip()
		if ($('#manage-group').length) {
			$('#manage-group a.button').attr('href', $('#members-groups-li #members').attr('href'));
		}

		if (document.getElementById('whats-new')) {
			const formAction = document.getElementById('whats-new-form').getAttribute('action')
			////https://localhost:8888/unsealed/activity/post/
			const $submitButtonDecoyWrapper = document.getElementById('whats-new-submit-decoy')
			const $submitButtonDecoy = document.getElementById('aw-whats-new-submit-decoy')
			const $submitButtonWrapper = document.getElementById('whats-new-submit')
			
			if ( $('#available-contests .contest-entry').length ) {
				$('#available-contests .contest-entry a').on('click', function (e) {
					e.preventDefault()
					$('.letter-form-content').show('d-none')
					$('#whats-new-form-submit-wrapper, .writing-links').removeClass('d-none').addClass('d-flex');
					$('#available-contests').addClass('d-none')
					$('#contest-prompt').removeClass('d-none').addClass('d-flex')
					$('#contest-prompt h3').html($('.slick-current .contest-prompt').html())
					$('.contest-id').val($(this).data('contest-id'))
					$('.letter-form-image-content').show()
				})
			}

			$('input[type=radio][name=toggle-letter-contest]').on('click', function(e) {
				if ( this.value === 'contest' ) {
					$('.contests-form-content').removeClass('d-none')
					$('.letter-form-content').hide()
					$('.letter-form-image-content').hide()
					$('#available-contests').removeClass('d-none')
					$('.writing-links, #after-form-options-selectors, #whats-new-form-submit-wrapper').removeClass('d-flex').addClass('d-none')
					if ( $('#available-contests').length && !$('#available-contests').hasClass('slick-initialized') ) {
						$('#available-contests').slick({
							arrows: true,
							slidesToShow: 1,
							slidesToScroll: 1,
							autoplay: false,
							pauseOnHover: false,
							cssEase: 'linear',
							// lazyLoad: 'ondemand',
							fade: false,
							infinite: true,
							// centerMode: true,
							variableWidth: false
						})
					}
				} else {
					if ( $('#activity_title').val() || $('#whats-new').val() ) {
						if ( confirm('Your progress will be lost. Continue?') ) {
							resetContestForm()
						} else {
							$('#toggle-contest').prop('checked', true)
						}
					} else {
						resetContestForm()
					}
				}
				fieldValidation = getFieldValidation()
			})

			function resetContestForm () {
				$('#activity_title, #whats-new').val('')
				$('#available-contests').removeClass('d-none')
				$('#contest-prompt').removeClass('d-flex').addClass('d-none')
				$('#contest-prompt h3').html('')
				$('.contests-form-content').addClass('d-none')
				$('.letter-form-image-content').show()
				$('.writing-links, #after-form-options-selectors, #whats-new-form-submit-wrapper').removeClass('d-none').addClass('d-flex')
				$('.letter-form-content').show()
				$('#after-form-options-selectors').show();
				$('#toggle-letter').prop('checked', true)
				$('.contest-id').val(null)
			}

			const queryString = window.location.search;
			const urlParams = new URLSearchParams(queryString);
			const contestId = urlParams.get('contest')
			if ( contestId ) {
				$('#toggle-contest').trigger('click')
				$('#available-contests .slick-slide').each( function () {
					const slideContestId = $('.contest-entry', this).data('contest-id')
					if (slideContestId.toString() === contestId ) {
						$('#available-contests').slick('slickGoTo', $(this).data('slick-index'))
						return;
					}
				})
			}

			let fieldValidation = getFieldValidation()

			function getFieldValidation () {
				return {
					title: {
						error: 'Give your letter a title',
						validated: $('#activity_title').length ? false : true
					},
					image: {
						error: 'Upload an image for your letter',
						validated: $('input[name="entry-img"]').length ? false : true
					},
					group: {
						error: 'Select a group to post your letter in',
						validated: ($('#whats-new-post-in').length && $('#whats-new-post-in').is(':visible') && !$('#whats-new-post-in').val()) ? false : true
					},
					count: {
						error: `Your letter should be at least ${validationWordCount.min} words`,
						validated: false
					}
				}
			}

			document.getElementById('whats-new').addEventListener('input', function (e) {
				let value = e.target.value;
				if (value) {
					var wordCount = value.includes('\n') ? value.replace('\n').match(/(\w+)/g) : value.match(/(\w+)/g);
					wordCount = wordCount ? wordCount.length : 0
					if (wordCount < validationWordCount.min) {
						$('#whats-new-content #validation').removeClass('valid');
						$('#whats-new-content #validation').addClass('invalid');
						$('#whats-new-content #validation .current-words-cnt').html(`(${wordCount} words)`);
						fieldValidation.count.validated = false
					} else {
						$('#whats-new-content #validation').addClass('valid');
						$('#whats-new-content #validation').removeClass('invalid');
						fieldValidation.count.validated = true
					}
					if ( validationWordCount.max ) {
						if ( wordCount > validationWordCount.max ) {
							const wordsArray = getWordsMax(e.target.value)

							e.target.value = wordsArray.join('').trim()
							$('.word-max').css('color', 'red')
						}
						if ( wordCount == validationWordCount.max ) {
							$('.word-max').css('color', 'red')
						}
						if ( wordCount < validationWordCount.max ) {
							$('.word-max').css('color', 'inherit')
						}
					}
				} else {
					$('#whats-new-content #validation .current-words-cnt').html('');
					$('#whats-new-content #validation').removeClass('valid');
					$('.word-max').css('color', 'inherit')
					fieldValidation.count.validated = false
				}
				validateNewLetter()
			}, false);

			const titleField = document.getElementById('activity_title')
			if ( titleField ) {
				titleField.addEventListener('input', function (e) {
					let value = e.target.value;
					fieldValidation.title.validated = value ? true : false
					validateNewLetter()
				}, false);
			}

			const cropImagePlaceholder = document.getElementById('activity-add-image-wrapper')
			const cropImageContainer = document.getElementById('letter-crop-container');
			const cropContainer = document.getElementById('letter-crop-container')
			const imageComplete = document.getElementById('letter-crop-complete')
			const previewImage = document.getElementById('unsealed-letter-image-preview')
			let uploadedImage = null
			const canvas = document.getElementById('unsealed-letter-image-canvas')
			const letterImageFile = document.getElementById('letter-image-file')
			let context = canvas.getContext('2d');
			const letterImageUpload = document.getElementById('unsealed-letter-upload')
			const letterImageInput = document.getElementById('letter-image-file')

			let jcrop = {}
			let jcropInstance = null

			$('.activity-remove-image').on('click', function(e){
				e.preventDefault();
				removeActivityImage()
			});

			function removeActivityImage() {
				cropImagePlaceholder.style.display = 'block'
				imageComplete.style.display = 'none'
				fieldValidation.image.validated = false
				jcrop.destroy()
				context.clearRect(0, 0, canvas.width, canvas.height);
				letterImageFile.value = null
				uploadedImage.remove()
				uploadedImage = null
				validateNewLetter()
			}

			letterImageUpload.addEventListener('change', startCropLetterImage);
			$(".activity-upload-image").on("click", function(e) {
			    e.preventDefault();
				if ( $('.jcrop-stage').length ) {
					$('.activity-remove-image').click();
				}
				letterImageUpload.click()
			})
			let unsplashPage = 1;
			const unsplashPerPage = $(window).width() > 900 ? 9 : 6;
			let unsplashKeywords = null

			$('#unsplash-search, #unsplash-search-inner').on('click', function (e) {
				e.preventDefault();
				unsplashKeywords = $(this).parent().find('.unsplash-keywords').val();
				if ( unsplashKeywords.length ) {
					if ( $('.jcrop-stage').length ) {
						$('.activity-remove-image').click();
					}
					fetchUnsplashImages()
				} else {
					// handle error
				}
			})

			$(document).mouseup( function(e) {
				var container = $('#unsplash-popup');
				if ( $(container).is(':visible') ) {
					// if the target of the click isn't the container nor a descendant of the container
					if (!container.is(e.target) && container.has(e.target).length === 0) {
						unsplashClose()
					}
				}
			});

			$('.unsplash-next').on('click', function () {
				unsplashPage++
				fetchUnsplashImages()
			})

			$('.unsplash-prev').on('click', function () {
				unsplashPage--
				fetchUnsplashImages()
			})

			$('#unsplash-popup__close span').on('click', function () {
				unsplashClose();
			})

			$('#unsplash-submit').on('click', function () {
				const imageUrl = $('.unsplash-image.active').data('image-url')
				unsplashClose()

				cropImageContainer.style.display = 'block'
				cropImagePlaceholder.style.display = 'none'
				uploadedImage = new Image();
				uploadedImage.crossOrigin = "anonymous";
				uploadedImage.id = 'unsealed-letter-image'
				uploadedImage.src = imageUrl
				$('[name="external_image"]').val(true)
				$(cropImageContainer).prepend(uploadedImage)
				cropLetterImage()
			})

			function unsplashClose () {
				$('#unsplash-popup__content-wrapper-content').html('')
				$('.unsplash-count').html('')
				$('#unsplash-popup').hide();
			}

			function fetchUnsplashImages () {
				$('#unsplash-popup').show();
				$('.unsplash-keywords').val(unsplashKeywords)
				$.get(`https://api.unsplash.com/search/photos?page=${unsplashPage}&per_page=${unsplashPerPage}&query=${encodeURIComponent(unsplashKeywords)}&client_id=${unsplash_key.unsplash_key}&orientation=landscape`, function(data, status){
					if ( data ) {
						$('#unsplash-popup__content-wrapper-content').html('')
						if ( data.results.length === 0 ) {
							$('#unsplash-popup__content-wrapper-content').html('<div class="unsplash-no-images"><div class="text-center"><h3>No images found</h3><p>Try another search</p></div></div>')
							return
						}
						data.results.forEach( uImage => {
							$('#unsplash-popup__content-wrapper-content').append(`
								<div class="unsplash-image-wrapper">
									<div class="unsplash-image" data-image-url="${ uImage.urls.regular }">
										<img src="${ uImage.urls.thumb }"/>
									</div>
								</div>`)
						})
						$('.unsplash-image').on('click', function () {
							$('.unsplash-image').not(this).removeClass('active')
							$(this).toggleClass('active');
							$('#unsplash-submit').attr('disabled', !$('.unsplash-image.active').length > 0);
						})
						$('.unsplash-count').html(`${ (unsplashPage - 1) * unsplashPerPage + 1 } - ${ ((unsplashPage - 1) * unsplashPerPage) + data.results.length } of ${data.total}`)
					}
				});
			}

			function selectUnsplashImage () {
				const imageUrl = data.results[0].urls.regular
				cropImageContainer.style.display = 'block'
				cropImagePlaceholder.style.display = 'none'
				uploadedImage = new Image();
				uploadedImage.crossOrigin = "anonymous";
				uploadedImage.id = 'unsealed-letter-image'
				uploadedImage.src = imageUrl
				$('[name="external_image"]').val(true)
				$(cropImageContainer).prepend(uploadedImage)
				cropLetterImage()
			}

			function startCropLetterImage () {
				cropImageContainer.style.display = 'block'
				cropImagePlaceholder.style.display = 'none'
				var reader
				reader = new FileReader();

				reader.onload = function(e) {
					// uploadLetterImage();
					uploadedImage = new Image();
					uploadedImage.id = 'unsealed-letter-image'
					uploadedImage.src = e.target.result;
					$(cropImageContainer).prepend(uploadedImage)
					cropLetterImage()
					uploadedImage.onload = function() {

						// cropLetterImage()
					};


				}

		    	reader.readAsDataURL(letterImageUpload.files[0]);
			}

			function cropLetterImage () {
				jcropInstance = null
				jcropInstance = Jcrop.load('unsealed-letter-image').then(img => {
					uploadedImage.width = $('#letter-crop-container').width()
					const cropHeight = uploadedImage.offsetWidth * .5
					const cropTop = (uploadedImage.offsetHeight - cropHeight) / 2
					const origin = [0,cropTop,cropContainer.offsetWidth,cropContainer.offsetWidth * .5];
					previewImage.style.width = cropContainer.offsetWidth
					jcrop = Jcrop.attach(img, {
						rect: origin.slice(0),
						// aspectRatio: 3/4,
						handles: ['sw','nw','ne','se'],
						aspectRatio: origin[2]/origin[3],
						boxHeight: cropHeight,
						boxWidth: uploadedImage.offsetWidth
					});
					const rect = Jcrop.Rect.create(origin[0],origin[1],origin[2],origin[3]);
					const options = {};
					jcrop.newWidget(rect,options)
					$('.crop-finished').off('click').on('click', (e) => {
						e.preventDefault()
						updatePreview()
					})
				});

			}

			// function updatePreview(c) {
	    //   console.log("called");
	    //   if(parseInt(c.w) > 0) {
	    //       // Show image preview
	    //       var imageObj = document.getElementById('unsealed-letter-image')
	    //       var canvas = document.getElementById('unsealed-letter-image-preview')
	    //       var context = canvas.getContext("2d");
	    //       context.drawImage(imageObj, c.x, c.y, c.w, c.h, 0, 0, imageObj.width, imageObj.height);
	    //   }
	    // };

			function updatePreview() {
				const imageObj = document.getElementById('unsealed-letter-image')
				const imgPos = jcrop.active.pos
				const imgWidthRatio = imageObj.naturalWidth / imageObj.width
				const imgHeightRatio = imageObj.naturalHeight / imageObj.height

				const imgX = imgPos.x * imgWidthRatio
				const imgY = imgPos.y * imgHeightRatio
				const imgW = imgPos.w * imgWidthRatio
				const imgH = imgPos.h * imgHeightRatio
				const letterImageX = document.getElementById('letter-image-x')
							letterImageX.value = imgX
				const letterImageY = document.getElementById('letter-image-y')
							letterImageY.value = imgY
				const letterImageW = document.getElementById('letter-image-w')
							letterImageW.value = imgW
				const letterImageH = document.getElementById('letter-image-h')
							letterImageH.value = imgH

				canvas.width = imgW
				canvas.height = imgH
			    context = canvas.getContext('2d');
				context.drawImage(imageObj, imgX, imgY, imgW, imgH, 0, 0, imgW, imgH);
				const imageSrc = canvas.toDataURL('image/png');
				previewImage.src = imageSrc;
				$('[name="canvas-image"]').val(imageSrc)
				cropContainer.style.display = 'none'
				imageComplete.style.display = 'block'
				letterImageFile.value = letterImageUpload.files[0]
				fieldValidation.image.validated = true
				validateNewLetter()
	    }

		function uploadLetterImage () {
			var formData = new FormData()
			formData.append('letter-image-file-upload', letterImageUpload.files[0])
			formData.append('action', 'bp_upload_source_image')
			$('#aw-whats-new-submit-decoy').addClass('disabled');
			$.ajax({
				url: ajaxurl,
				processData: false,
				type: 'POST',
				contentType: false,
				data: formData,
				success: function (data) {
					let response = JSON.parse(data);
					let fullPath = response.full_path;
					let dst_path = response.dest_path;
					$('#whats-new-form [name="letter-image-upload-file"]').val(fullPath);
					$('#whats-new-form input[name="dst-file"]').val(dst_path);
					$('#aw-whats-new-submit-decoy').removeClass('disabled');
				},
				complete: function (data) {

				},
				error: function (errorThrown) {
					alert("Error in uploading source image");
					console.log(errorThrown);
				}
			});
		}

			document.getElementById('whats-new-post-in').addEventListener('change', function (e) {
				fieldValidation.group.validated = true
				validateNewLetter()
			}, false);

			document.getElementById('aw-whats-new-submit-decoy').addEventListener('click', function (e) {
				Object.keys(fieldValidation).forEach( field => {
					if ( !fieldValidation[field].validated ) {
						if ( field == 'image' ) {
							const jcropImage = document.getElementById('unsealed-letter-image')
							if ( jcropImage ) {
								fieldValidation['image'].error = 'Click "Finished Cropping"'
							} else {
								fieldValidation['image'].error = 'Upload an image for your letter'
							}
						}
						$( `<p role="alert" class="activity-error error activity-error-${field}">${fieldValidation[field].error}</p>` ).insertAfter( `.validate-${field}` );
					}
				})
			}, false)

			function validateNewLetter() {
				let validated = true
				Object.keys(fieldValidation).forEach( field => {
					if ( fieldValidation[field].validated ) {
						$(`.activity-error-${field}`).remove()
					} else {
						validated = false
					}
				})
				if ( validated ) {
					if ( $($submitButtonDecoyWrapper).is(':visible') ) {
						$($submitButtonDecoyWrapper).hide()
						$($submitButtonWrapper).show()
					}
				} else {
					if ( !$($submitButtonDecoyWrapper).is(':visible') ) {
						$($submitButtonDecoyWrapper).show()
						$($submitButtonWrapper).hide()
					}
				}
				// formData.append('letter-image-file-upload', letterImageUpload.files[0])
			}
		}

		if ( $('.reply-validation').length ) {
			$('[name="ac_form_submit"]').addClass('disabled');
			$('.ac-form').each( function () {
				const form = $(this)
				$('.ac-input', form).on('input', function (e) {
					let value = e.target.value;
					if (value) {
						var wordCount = value.replace('\n').match(/(\w+)/g).length;

						if ( validationWordCount.max ) {
							if ( wordCount > validationWordCount.max ) {
								const wordsArray = getWordsMax(value)
								e.target.value = wordsArray.join('').trim()
								$('.word-max', form).css('color', 'red')
							}
							if ( wordCount == validationWordCount.max ) {
								$('.word-max', form).css('color', 'red')
							}
							if ( wordCount < validationWordCount.max ) {
								$('.word-max', form).css('color', 'inherit')
							}
						}
						if (wordCount < validationWordCount.min) {
							$('[name="ac_form_submit"]').addClass('disabled');
							$('.reply-validation', form).removeClass('valid');
							$('.reply-validation', form).addClass('invalid');
							$('.reply-validation .current-words-cnt', form).html(`(${wordCount} words)`);
						} else {
							$('.reply-validation', form).addClass('valid');
							$('.reply-validation', form).removeClass('invalid');
							$('[name="ac_form_submit"]').removeClass('disabled');
						}
					} else {
						$('.reply-validation .current-words-cnt', form).html('');
						$('.reply-validation', form).removeClass('valid');
						$('[name="ac_form_submit"]').addClass('disabled');
						$('.word-max', form).css('color', 'inherit')
					}
				})
			})
		}

		if ( $('.activity-edit').length ) {
			$('.activity-edit').on('click', function (e) {
				e.preventDefault();
				if ( $(this).hasClass('cancel-edit') ) {
					$(`#activity-${$(this).data('activity-id')}`).removeClass('editing-activity')
					$(this).html('Edit Letter').removeClass('cancel-edit');
				} else {
					$(`#activity-${$(this).data('activity-id')}`).addClass('editing-activity')
					$(this).html('Cancel Edit').addClass('cancel-edit');
				}
			})
		}
	});

	function getWordsMax( words ) {
		let wordsArray = words ? words.split(' ') : []
		const newWordsArray = []
		wordsArray.forEach( (word, wordIndex) => {
			const newWord = word.split('\n')
			if ( word.includes('\n') ) {
				const breakWords = word.split('\n')
				breakWords[0] = breakWords[0] + '\n'
				breakWords[1] = wordIndex == wordsArray.length - 1 ? breakWords[1].trimStart() + '' : breakWords[1].trimStart() + ' '
				breakWords.forEach( brokenWord => newWordsArray.push(brokenWord) )
			} else {
				const newWord = wordIndex == wordsArray.length - 1 ? word : word + ' '
				newWordsArray.push(newWord)
			}
		})

		newWordsArray.length = validationWordCount.max
		return newWordsArray
	}

	if ( $('.copy-activity-link').length ) {
		var $temp = $("<input>");
		$('.copy-activity-link').on('click', function(e) {
			e.preventDefault()
			var clickedCopy = $(this);
			var $url = $(this).attr('href');
			$("body").append($temp);
		  $temp.val($url).select();
		  document.execCommand("copy");
		  $temp.remove();
			$('.copied-activity-url', this).addClass('active');
			setTimeout( function () {
				$('.copied-activity-url', clickedCopy).removeClass('active')
			}, 1000)
		})
	}

	if ( $('.report-letter').length ) {
		$('.report-letter').on('click', function (e) {
			e.preventDefault()
			const letter = $(this)
			const letterId = $(letter).data('letter-id')
			const reporterId = $(letter).data('user-id')
			const reportedId = $(letter).data('activity-user-id')
			const isReported = $(letter).hasClass('reported')
			const reportMessage = $(letter).data('report-message') ? $(letter).data('report-message') : null
			$(letter).css({'pointer-events': 'none', 'opacity': .2 });
			$.ajax({
				url: ajaxurl,
				data: {
					action: 'bp_report_activity',
					letter_id: letterId,
					reporter_id: reporterId,
					reported_id: reportedId,
					reported: isReported,
					message: reportMessage
				},
				success: function (data) {
					$(letter).css({'pointer-events': 'auto', 'opacity': 1 });
					if ( data === '1' ) {
						$(letter).addClass('reported')
					} else {
						$(letter).removeClass('reported')
					}
				},
				complete: function (data) {
					// console.log(data)
				},
				error: function (errorThrown) {
					console.log(errorThrown);
				},
			});
		})
	}

	if ( $('.feedback-link').length ) {
		$('.feedback-link').on('click', function (e) {
			e.preventDefault()
			const emailTo = $(this).data('mail-to')
			const emailBody = $(this).parent().parent().parent().parent().find('textarea').val()
			window.location.href = `mailto:${emailTo}?subject=I'd like to get feedback on my letter&body=Requested Feedback:%0D%0A%0D%0AMy letter so far: %0D%0A${emailBody}`;
		})
	}

	if ( $('#unsealed-lock-signup').length ) {
		if ( $('.inline-lock-wrapper').length ) {
			$('body').addClass('content-lock-content')
		}
		let isSubscribed = false;
		$('#unsealed-lock-signup').on('submit', function (e) {
			e.preventDefault();
			const email = $('#unsealed-lock-email').val()
			let data = {
		      action: 'do_ajax',
		      fn: 'verification_email',
		      dataType: 'json',
		      email: email
		    }
			$.ajax({
				url: ajaxurl,
				data: data,
				success: function (data) {
					if (data !== null) {
						const dataObj = JSON.parse(data)
						$('#signup-required-signup').hide();
						switch (dataObj.state) {
							case 'succeed_in_sending_verification_code':
								$('#signup-required-verification').show();
							break;
							case 'already_registered':
								// $('#signup-required-verified').show()
								isSubscribed = true
								// switch (dataObj.user_type) {
								// 	case 'subscriber':
								// 		$('#signup-required-subscriber').show()
								// 	break;
								// 	case 'member':
								// 		$('#signup-required-member').show()
								// 	break;
								// 	default:
								// 		$('#signup-required-subscriber').show()
								// 	break;
								// }
								$('#signup-required').fadeOut();
								$('body').removeClass('content-lock')
							break;
							case 'error':

							break;
							default:
								$('#signup-required-error').show()

						}
					}
				},
				complete: function (data) {

				},
				error: function (errorThrown) {
					console.log(errorThrown);
				},
			})
		})

		$('#signup-lock-verify').on('submit', function (e) {
			e.preventDefault();
			const email = $('#unsealed-lock-email').val()
			const code = $('#unsealed-lock-code').val()
			let data = {
				action: 'do_ajax',
  		    	fn: 'verfication_code',
  		    	dataType: 'json',
  		    	email: email,
  		    	code: code
		    }
			$.ajax({
				url: ajaxurl,
				data: data,
				success: function (data) {
					if (data !== null) {
						const dataObj = JSON.parse(data).state
						switch (dataObj) {
							case 'succeed_in_verification':
								// $('#signup-required-subscriber').show()
								$('#signup-required').fadeOut();
								$('body').removeClass('content-lock')
								isSubscribed = true
							break;
							case 'failed_in_verification':
								// try again??
							break;
							default:
								$('#signup-required-error').show()
							break;

						}
					}
				},
				complete: function (data) {

				},
				error: function (errorThrown) {
					console.log(errorThrown);
				},
			})
		})
	}

	if ( $('#upvote-entry-form').length ) {

		$('#upvote-entry-form').on('submit', function (e) {
			e.preventDefault()
			sendVerificationCode()
		})

		function sendVerificationCode() {
			const email = $('#upvote-entry-form input[type="email"]').val()
			console.log(email)
			$.ajax({
				url: ajaxurl,
				data: {
					action: 'do_ajax',
					fn: 'verification_email',
					email
				},
				success: function (data) {
					if ( $('#contest-verify-code').is(':visible') ) {
						$('.code-resent').removeClass('d-none')
					}
					$('#contest-newsletter-signup').addClass('d-none')
					$('#contest-verify-code').removeClass('d-none')
				},
				complete: function (data) {
					// Complete
				},
				error: function (errorThrown) {
					console.log(errorThrown);
				}
			});
		}

		$('#resend-code').on('click', function (e) {
			e.preventDefault()
			sendVerificationCode()
		})
		$('#contest-verify-form').on('submit', function (e) {
			e.preventDefault()
			const email = $('#upvote-entry-form input[type="email"]').val()
			const code = $('#contest-verify-form input[type="text"]').val()
			$('#contest-verify-code .alert').addClass('d-none')
			$('.code-resent').addClass('d-none')
			$.ajax({
				url: ajaxurl,
				data: {
					action: 'do_ajax',
					fn: 'verification_code',
					email: email,
					code: code
				},
				success: function (data) {
					if ( data == 'failed_in_verification' ) {
						$('#contest-verify-code .alert').removeClass('d-none')
					} else if ( data === 'already_signed_up' ) {
						alert('You\'re already signed up!')
					} else if ( data === 'invalid_email' ) {
						alert('This email seems to be invalid')
					} else {
						$('#upvote-entry').modal({ show: false })
						// setCookie('vote_email', email, 30)
						// UPVOTE CONTEST
					}
					
				},
				complete: function (data) {
					// Complete
				},
				error: function (errorThrown) {
					console.log(errorThrown);
				},
			});
		})
	}
});
