import { combineReducers } from 'redux'
import { letters } from './letters'

export default combineReducers({
  letters
})
