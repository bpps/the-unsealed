import { SET_LETTERS } from '../actions/letters'

export const letters = (state = [], action) => {
  switch(action.type) {
    case SET_LETTERS:
      if ( action.merge ) {
        return [...state, ...action.letters]
      } else {
        return action.letters
      }
    default:
      return state
  }
}
