import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, useLocation } from 'react-router-dom'
import App from './App';
import reducer from './reducers'
import middleware from './middleware'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

const initialState = {
  letters: []
}

const store = createStore(reducer, initialState, middleware)
const basename = process.env.NODE_ENV === 'production' ? '/letters' : '/unsealed/letters'

ReactDOM.render(
  <Provider store={store}>
    <Router basename={basename}>
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById('root'))
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
