import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link, useLocation } from 'react-router-dom'

const Topic = (props) => {
  const { topic } = props
  const location = useLocation();
  let currentPath = location.pathname.replace('/', '')
      currentPath = currentPath.replace('/', '')
  const topicPath = topic.slug ? '/' + topic.slug : '/'
  return (
    <option value={topic.slug}>
      <Link
        to={topicPath}
        className={`btn-brush-hover btn${ topic.slug === currentPath ? ' selected' : '' }`}>
        <span>{ topic.name }</span>
      </Link>
    </option>
  )
}

export default Topic
