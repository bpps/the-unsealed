import React, { Component, useEffect, useRef } from 'react'
import { useLocation, withRouter } from "react-router-dom"
import { decodeEntities } from '../utils'

function Header ({ history, props}) {
  const headerRef = useRef(null);
  const location = useLocation()
  let currentPath = location.pathname.replace('/', '')
      currentPath = currentPath.replace('/', '')
  const selectedTopic = topics.filter( topic => {
    return topic.slug === currentPath
  })
  const selectedTopicName = selectedTopic.length ? selectedTopic[0].name : 'Featured letters'
  const topicImage = selectedTopic.length ? selectedTopic[0].image : allLettersHeader.image
  const topicDesc = selectedTopic.length ? selectedTopic[0].description : allLettersHeader.description
  useEffect(() => {
    const unlisten = history.listen(() => {
      headerRef.current
      .scrollIntoView({behavior: "smooth"})
    });
    return () => {
      unlisten();
    }
  }, []);

  return (
    <header ref={headerRef} className="entry-header pb-4">
      <div id="community-bg-images">
      </div>
      <div className="d-flex flex-column align-items-center py-4 py-md-5">
        {
          topicImage && <div className="header-bg-img"><img src={topicImage}/></div>
        }
        <div className="container">
          {
            topicImage &&
            <div className="header-img">
              <img src={topicImage} alt={selectedTopicName}/>
            </div>
          }
          <div>
            <p className="h1 d-block position-relative entry-title script-font">
              {selectedTopicName}
            </p>
            {
              topicDesc && <div className="d-flex justify-content-center topic-desc"><div className="col-md-6" dangerouslySetInnerHTML={{ __html: decodeEntities(topicDesc) }}></div></div>
            }
          </div>
        </div>
      </div>
    </header>
  )
}

export default withRouter(Header);
