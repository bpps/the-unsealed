import React, { Component } from 'react'
import { useHistory, useLocation } from "react-router-dom";

function Topics () {
  let history = useHistory();
  const location = useLocation();
  let currentPath = location.pathname.replace('/', '')
      currentPath = currentPath.replace('/', '')
  function handleSelectTopic (value) {
    history.push(`/${value}`);
  }

  return (
    <div className="d-flex justify-content-end pt-4 pb-2">
      <div className="terms-selector col-auto">
        <div className="filter-header d-flex align-items-center">
          <span className="h1 script-font teal mr-3 mb-0">Topic</span>
          <select className="filter-terms underlined py-1" defaultValue={currentPath} onChange={ topic => handleSelectTopic(topic.target.value) }>
            <option disabled>Choose a topic</option>
            <option value="">All letters</option>
            {
              topics.map( topic => {
                return (
                  <option key={`topic-${topic.term_id}`} value={topic.slug}>
                    {topic.name}
                  </option>
                )
              })
            }
          </select>
        </div>
      </div>
    </div>
  )
}

export default Topics
