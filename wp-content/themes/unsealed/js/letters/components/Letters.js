import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchLetters, fetchLetterBySlug } from '../utils'
import { loadLetters, setLetters } from '../actions/letters'
import Letter from './Letter'

class Letters extends Component {
  constructor (props) {
    super(props)
    this.handleScroll = this.handleScroll.bind(this)
    this.getMoreLetters = this.getMoreLetters.bind(this)
    this.handleTopicChange = this.handleTopicChange.bind(this)
    this.state = {
      paged: 1,
      fetching: true,
      endOfFeed: false,
      loading: true
    }
  }

  componentDidMount () {
    document.addEventListener('scroll', this.handleScroll);
    const { dispatch, location, history } = this.props
    const currentPath = location.pathname.replace('/', '')
    const topic = currentPath === '' ? false : currentPath.replace('/', '')
    fetchLetters(topic, 1).then( letters_obj => {
      dispatch( setLetters(letters_obj.letters, false) )
      this.setState({ loading: false, fetching: false, paged: 2, endOfFeed: !letters_obj.has_more_letters })
    })
    this.unlisten = history.listen((location, action) => {
      this.handleTopicChange(location)
    })
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleScroll);
    this.unlisten();
  }

  handleTopicChange (location) {
    const { dispatch } = this.props
    let currentPath = location.pathname.replace('/', '')
      currentPath = currentPath.replace('/', '')
    const topic = currentPath === '' ? false : currentPath
    this.setState({ loading: true, fetching: true, endOfFeed: false })
    fetchLetters( topic, 1 ).then( letters_obj => {
      dispatch( setLetters(letters_obj.letters, false) )
      this.setState({ loading: false, paged: 2, fetching: false, endOfFeed: !letters_obj.has_more_letters })
    })
  }

  getMoreLetters () {
    const { dispatch, location } = this.props
    const currentPath = location.pathname.replace('/', '')
    const topic = currentPath === '' ? false : currentPath
    fetchLetters(topic, this.state.paged).then( letters_obj => {
      // Stop updating if no letters are returned.
      // NOTE: this can be changed to have the function return a bool if there are more letters
      dispatch( setLetters(letters_obj.letters, true) )
      this.setState({ fetching: false, paged: this.state.paged + 1, endOfFeed: !letters_obj.has_more_letters })
    })
  }

  handleScroll () {
    if ( this.state.fetching || this.state.endOfFeed ) return
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset + (docHeight / 3);
    if (windowBottom >= docHeight) {
      this.setState({
        fetching: true
      }, this.getMoreLetters() )
    }
  }

  render () {
    const { letters, history } = this.props
    const { fetching, endOfFeed, loading } = this.state
    return (
      <div>
        <div className="letters d-flex flex-wrap">
          {
            (!loading && letters) &&
            letters.map( letter => {
              return (
                <Letter key={`letter-${letter.id}`} letter={letter} history={history}/>
              )
            })
          }
          {
            (letters.length === 0 && !loading) &&
            <p>No letters</p>
          }
        </div>
        {
          ( loading || (fetching && !endOfFeed) ) && <div className="py-3 loading d-flex justify-content-center"><span>Loading</span></div>
        }
      </div>
    )
  }
}

Letters.propTypes = {

}

export default connect((state) => ({
  letters: state.letters
}))(Letters)
