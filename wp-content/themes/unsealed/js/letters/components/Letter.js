import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Topic from './Topic'
import {
  EmailShareButton,
  FacebookShareButton,
  FacebookShareCount,
  FacebookIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TwitterShareButton,
  TwitterIcon
} from "react-share";
import { decodeEntities } from '../utils'

class Letter extends Component {
  constructor (props) {
    super(props)
    this.handleLetterActive = this.handleLetterActive.bind(this)
  }

  handleLetterActive () {
    const { letter, history } = this.props
    history.push(`/${letter.slug}`)
  }

  render () {
    const { letter } = this.props
    return (
      <div className="letter pt-4 pt-4 pb-4 col-sm-6 col-md-4 col-xl-3">
        <div className="row">
          <div className="col-12">
            <div className="letter-image-container image-ratio bg-3-2">
              <div className="letter-image position-relative">
                <a href={ letter.link } style={{ backgroundImage: `url(${ letter.featured_image })` }} className="bg-cover card-img-overlay">
                </a>
              </div>
            </div>
          </div>
          <div className="col-12">
            {
              letter.topics &&
              <div className="row">
                {
                  letter.topics.map( topic => {
                    return <Topic key={`letter-${letter.id}-topic-${topic.term_id}`} topic={topic} />
                  })
                }
              </div>
            }
            <h3 className="h4 mt-3"><a href={ letter.link } dangerouslySetInnerHTML={{ __html: decodeEntities(letter.title) }}></a></h3>
            {
              letter.excerpt &&
              <div dangerouslySetInnerHTML={{ __html: decodeEntities(letter.excerpt) }}></div>
            }
            <a className="teal mt-2 d-block" href={ letter.link }>Read this letter</a>
          </div>
          <div className="col-12">
            <div className="social-share-links mt-4 row no-gutters align-items-center pull-right">
              <div className="col-auto">
                <span className="script-font h3 share-label mr-2">Share this story</span>
              </div>
              <div className="social-link-wrapper col-auto">
                <FacebookShareButton url={letter.link} quote={letter.excerpt ? letter.excerpt : false}>
                  <FacebookIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </FacebookShareButton>
                <FacebookShareCount url={letter.link}>
                  {shareCount => <span className="myShareCountWrapper">{shareCount}</span>}
                </FacebookShareCount>
              </div>
              <div className="social-link-wrapper col-auto">
                <LinkedinShareButton url={letter.link} title={letter.title} summary={letter.excerpt ? letter.excerpt : false} source="https://theunsealed.com">
                  <LinkedinIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </LinkedinShareButton>
              </div>
              <div className="social-link-wrapper col-auto">
                <TwitterShareButton url={letter.link} title={letter.title}>
                  <TwitterIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </TwitterShareButton>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Letter.propTypes = {

}

export default connect((state) => ({
  letters: state.letters
}))(Letter)
