import React from 'react'
import Topics from './components/Topics'
import Letters from './components/Letters'
import Header from './components/Header'
function App(props) {
  return (
    <div className="letters-app">
      <Header/>
      <div className="pb-5 container-wide">
        <Topics/>
        <Letters location={props.location} history={props.history}/>
      </div>
    </div>
  );
}

export default App
