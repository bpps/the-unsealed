import $ from 'jquery';
import 'bootstrap'

$(function() {
	let usernameTimer, usernameTimeoutVal, emailTimer, emailTimeoutVal = 750
	const $username = document.getElementById('username')
	const $email = document.getElementById('bemail')

	$($username).on('keydown', function(e) {
		var value =  e.target.value;
	  //only allow a-z, A-Z, digits 0-9 and comma, with only 1 consecutive comma ...
	  if ( e.key !== undefined && (!e.key.match(/[a-zA-Z0-9_-]/) || (e.key == ',' && value[value.length-1] == ','))) {
	    e.preventDefault();
	  }
  });

	if ( $username ) {
		if ( $username.value !== '' ) {
			finishedTypingUsername($username.value)
		}
		$username.addEventListener('input', function (e) {
			$('#username-approved').removeClass('active')
			$('#username-taken').removeClass('active')
			window.clearTimeout(usernameTimer); // prevent errant multiple timeouts from being generated
			usernameTimer = window.setTimeout(() => {
		  	finishedTypingUsername(e.target.value)
		  }, usernameTimeoutVal);
		})
	}

	if ( $email ) {
		if ( $email.value !== '' ) {
			finishedTypingEmail($email.value)
		}
		$email.addEventListener('input', function (e) {
			$('#email-taken').removeClass('active')
			window.clearTimeout(emailTimer); // prevent errant multiple timeouts from being generated
			emailTimer = window.setTimeout(() => {
		  	finishedTypingEmail(e.target.value)
		  }, emailTimeoutVal);
		})
	}

	function finishedTypingUsername(username) {
		$.ajax({
			url: ajaxurl,
			data: {
				action: 'do_ajax',
				fn: 'unsealed_check_username',
				username: username
			},
			success: function (data) {
				if ( data > 0 ) {
					$('#username-taken').addClass('active')
					$($username).addClass('error')
				} else {
					$('#username-approved').addClass('active')
					$($username).removeClass('error')
				}
			},
			error: function (errorThrown) {
				console.log(errorThrown);
			},
		});
	}

	function finishedTypingEmail(email) {
		$.ajax({
			url: ajaxurl,
			data: {
				action: 'do_ajax',
				fn: 'unsealed_check_email',
				email: email
			},
			success: function (data) {
				console.log(data)
				if ( data > 0 ) {
					$('#email-taken').addClass('active')
					$($email).addClass('error')
				} else {
					$($email).removeClass('error')
				}
			},
			error: function (errorThrown) {
				console.log(errorThrown);
			},
		});
	}


});
