import React from 'react'
import Contests from './components/Contests'
import Header from './components/Header'
function App(props) {
  return (
    <div className="contests-app">
      <Header/>
      <div className="pb-5 container-wide">
        <Contests location={props.location} history={props.history}/>
      </div>
    </div>
  );
}

export default App
