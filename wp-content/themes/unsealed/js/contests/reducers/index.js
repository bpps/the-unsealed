import { combineReducers } from 'redux'
import { contests } from './contests'

export default combineReducers({
  contests
})
