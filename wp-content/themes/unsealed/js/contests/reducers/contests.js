import { SET_CONTESTS } from '../actions/contests'

export const contests = (state = [], action) => {
  switch(action.type) {
    case SET_CONTESTS:
      if ( action.merge ) {
        return [...state, ...action.contests]
      } else {
        return action.contests
      }
    default:
      return state
  }
}
