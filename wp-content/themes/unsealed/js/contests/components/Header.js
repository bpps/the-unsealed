import React, { Component, useEffect, useRef } from 'react'
import { useLocation, withRouter } from "react-router-dom"
import { decodeEntities } from '../utils'

function Header ({ history, props}) {
  const headerRef = useRef(null);
  const location = useLocation()
  let currentPath = location.pathname.replace('/', '')
      currentPath = currentPath.replace('/', '')
  const headerTitle = contestsHeader.title
  const headerImage = contestsHeader.image
  const headerDesc = contestsHeader.description
  let dollars = []
  for (var i = 0; i < 9; i++) {
    var fontSize = Math.floor(Math.random() * 3) + 2
    dollars.push(<div key={`dollar-${i}`} className={`script-font teal display-${ fontSize }`}><span>$</span></div>)
    dollars.push(<div></div>)
  }
  useEffect(() => {
    const unlisten = history.listen(() => {
      headerRef.current
      .scrollIntoView({behavior: "smooth"})
    });
    return () => {
      unlisten();
    }
  }, []);

  return (
    <header ref={headerRef} className="entry-header pb-4">
      <div className="header-images-container">
        <div className="image-grid" style={{ opacity: .4 }}>
          {dollars}
        </div>
      </div>
      <div className="d-flex flex-column align-items-center py-4 py-md-5">
        {
          headerImage && <div className="header-bg-img"><img src={headerImage}/></div>
        }
        <div className="container">
          {
            headerImage &&
            <div className="header-img">
              <img src={headerImage} alt={headerTitle}/>
            </div>
          }
          <div>
            {
              contestsHeader.link
              ? <a className="text-white h1 d-block position-relative entry-title script-font" href={contestsHeader.link}>
                  {headerTitle}
                </a>
              : <p className="h1 d-block position-relative entry-title script-font">
                  {headerTitle}
                </p>
            }
            {
              headerDesc && <div className="d-flex justify-content-center topic-desc"><div className="col-md-6" dangerouslySetInnerHTML={{ __html: decodeEntities(headerDesc) }}></div></div>
            }
            {
              contestsHeader.prize && <p className="h4 mb-4"><strong>{contestsHeader.prize}</strong></p>
            }
            {
              contestsHeader.link && <a className="teal" href={contestsHeader.link}>Contest Details</a>
            }
          </div>
        </div>
      </div>
    </header>
  )
}

export default withRouter(Header);
