import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchContests, fetchContestBySlug } from '../utils'
import { loadContests, setContests } from '../actions/contests'
import Contest from './Contest'

class Contests extends Component {
  constructor (props) {
    super(props)
    this.handleScroll = this.handleScroll.bind(this)
    this.getMoreContests = this.getMoreContests.bind(this)
    this.state = {
      paged: 1,
      fetching: true,
      endOfFeed: false,
      loading: true
    }
  }

  componentDidMount () {
    document.addEventListener('scroll', this.handleScroll);
    const { dispatch, location, history } = this.props
    const currentPath = location.pathname.replace('/', '')
    const topic = currentPath === '' ? false : currentPath.replace('/', '')
    fetchContests(1).then( contests_obj => {
      dispatch( setContests(contests_obj.contests, false) )
      this.setState({ loading: false, fetching: false, paged: 2, endOfFeed: !contests_obj.has_more_contests })
    })
    this.unlisten = history.listen((location, action) => {
      this.handleTopicChange(location)
    })
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleScroll);
    this.unlisten();
  }

  getMoreContests () {
    const { dispatch, location } = this.props
    fetchContests(this.state.paged).then( contests_obj => {
      // Stop updating if no contests are returned.
      // NOTE: this can be changed to have the function return a bool if there are more contests
      dispatch( setContests(contests_obj.contests, true) )
      this.setState({ fetching: false, paged: this.state.paged + 1, endOfFeed: !contests_obj.has_more_contests })
    })
  }

  handleScroll () {
    if ( this.state.fetching || this.state.endOfFeed ) return
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset + (docHeight / 3);
    if (windowBottom >= docHeight) {
      this.setState({
        fetching: true
      }, this.getMoreContests() )
    }
  }

  render () {
    const { contests, history } = this.props
    const { fetching, endOfFeed, loading } = this.state
    const upcomingContests = contests.filter( contest => {
      return contest.active
    })
    const pastContests = contests.filter( contest => {
      return !contest.active
    })

    return (
      <div>
        <div className="letters d-flex flex-wrap">
          {
            (!loading && upcomingContests.length !== 0 ) &&
            <h3 className="w-100 mt-5 mb-3 text-center as-seen-header">Current Contests</h3>
          }
          {
            (!loading && upcomingContests.length !== 0) &&
            upcomingContests.map( contest => {
              return (
                <Contest key={`contest-${contest.id}`} contest={contest} history={history}/>
              )
            })
          }
          {
            (!loading && pastContests.length !== 0) &&
            <h3 className="w-100 mt-5 mb-3 text-center as-seen-header">Past Contests</h3>
          }
          {
            (!loading && pastContests.length !== 0) &&
            pastContests.map( contest => {
              return (
                <Contest key={`contest-${contest.id}`} contest={contest} history={history}/>
              )
            })
          }
          {
            (contests.length === 0 && !loading) &&
            <p>No contests</p>
          }
        </div>
        {
          ( loading || (fetching && !endOfFeed) ) && <div className="py-3 loading d-flex justify-content-center"><span>Loading</span></div>
        }
      </div>
    )
  }
}


export default connect((state) => ({
  contests: state.contests
}))(Contests)
