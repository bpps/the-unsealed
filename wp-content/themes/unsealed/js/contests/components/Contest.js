import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  EmailShareButton,
  FacebookShareButton,
  FacebookShareCount,
  FacebookIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TwitterShareButton,
  TwitterIcon
} from "react-share";
import { decodeEntities } from '../utils'

class Contest extends Component {
  constructor (props) {
    super(props)
    this.handleContestActive = this.handleContestActive.bind(this)
  }

  handleContestActive () {
    const { contest, history } = this.props
    history.push(`/${contest.slug}`)
  }

  render () {
    const { contest } = this.props
    return (
      <div className="letter pt-4 pt-4 pb-4 col-sm-6 col-md-4 col-xl-3">
        <div className="row">
          <div className="col-12">
            <div className="letter-image-container image-ratio bg-3-2">
              <div className="letter-image position-relative">
                <a href={ contest.link } style={{ backgroundImage: `url(${ contest.featured_image })` }} className="bg-cover card-img-overlay">
                </a>
              </div>
            </div>
          </div>
          <div className="col-12">
            <h3 className="h4 mt-3"><a href={ contest.link } dangerouslySetInnerHTML={{ __html: decodeEntities(contest.title) }}></a></h3>
            {
              contest.excerpt &&
              <div dangerouslySetInnerHTML={{ __html: decodeEntities(contest.excerpt) }}></div>
            }
            {
              contest.prize &&
              <p className="font-weight-bold h6 mt-2">{contest.prize}</p>
            }
            <a className="teal mt-2 d-block" href={ contest.link }>
              {
                contest.active
                  ? 'Contest Details'
                  : 'See the winners'
              }
            </a>
          </div>
          <div className="col-12">
            <div className="social-share-links mt-4 row no-gutters align-items-center pull-right">
              <div className="col-auto">
                <span className="script-font h3 share-label mr-2">Share this contest</span>
              </div>
              <div className="social-link-wrapper col-auto">
                <FacebookShareButton url={contest.link} quote={contest.excerpt ? contest.excerpt : false}>
                  <FacebookIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </FacebookShareButton>
                <FacebookShareCount url={contest.link}>
                  {shareCount => <span className="myShareCountWrapper">{shareCount}</span>}
                </FacebookShareCount>
              </div>
              <div className="social-link-wrapper col-auto">
                <LinkedinShareButton url={contest.link} title={contest.title} summary={contest.excerpt ? contest.excerpt : false} source="https://theunsealed.com">
                  <LinkedinIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </LinkedinShareButton>
              </div>
              <div className="social-link-wrapper col-auto">
                <TwitterShareButton url={contest.link} title={contest.title}>
                  <TwitterIcon bgStyle={{ fill: 'transparent', padding: 0 }} iconFillColor='black' size={32} round={false} />
                </TwitterShareButton>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}


export default connect((state) => ({
  contests: state.contests
}))(Contest)
