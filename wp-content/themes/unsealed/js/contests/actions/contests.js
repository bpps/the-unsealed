export const SET_CONTESTS = 'SET_CONTESTS'

export const setContests = (contests, merge) => {
  return {
    type: SET_CONTESTS,
    contests,
    merge
  }
}
