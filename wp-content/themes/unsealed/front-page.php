<?php
/**
 * The main template file
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			echo get_home_hero(); // modules/home-hero.php
			echo get_featured_letters(); // modules/featured-letters.php
			echo get_from_our_community(); // modules/community.php
			echo get_spotlights(); // modules/community.php
			echo get_videos_module(); // modules/featured-videos.php
			// echo get_about_lauren(); // modules/about-lauren.php
			echo get_social_platform_module(); // modules/social-platform-callout.php
			echo get_as_seen_on(); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
