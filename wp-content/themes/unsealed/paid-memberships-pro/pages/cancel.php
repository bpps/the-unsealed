<?php
	global $pmpro_msg, $pmpro_msgt, $pmpro_confirm, $current_user, $wpdb;

	if(isset($_REQUEST['levelstocancel']) && $_REQUEST['levelstocancel'] !== 'all') {
		//convert spaces back to +
		$_REQUEST['levelstocancel'] = str_replace(array(' ', '%20'), '+', $_REQUEST['levelstocancel']);

		//get the ids
		$old_level_ids = array_map('intval', explode("+", preg_replace("/[^0-9al\+]/", "", $_REQUEST['levelstocancel'])));

	} elseif(isset($_REQUEST['levelstocancel']) && $_REQUEST['levelstocancel'] == 'all') {
		$old_level_ids = 'all';
	} else {
		$old_level_ids = false;
	}
?>
<div id="pmpro_cancel" class="mt-5 text-center <?php echo pmpro_get_element_class( 'pmpro_cancel_wrap', 'pmpro_cancel' ); ?>">
	<?php
		if($pmpro_msg)
		{
			?>
			<div class="<?php echo pmpro_get_element_class( 'pmpro_message ' . $pmpro_msgt, $pmpro_msgt ); ?>"><?php echo $pmpro_msg?></div>
			<?php
		}
	?>
	<?php
		if(!$pmpro_confirm)
		{
			if($old_level_ids)
			{
				if(!is_array($old_level_ids) && $old_level_ids == "all")
				{
					?>
					<p><?php _e('Are you sure you want to cancel your subscription?', 'paid-memberships-pro' ); ?></p>
					<div class="w-75 mx-auto alert-wrapper mt-4 bg-white d-block position-relative">
						<div class="position-relative bg-white alert" style="border: 2px solid #43d3cb;">
							<p><strong><?php _e('Letter\'s and contest entries you\'ve posted may be removed from the site or set to private when you cancel.?', 'paid-memberships-pro' ); ?></strong></p>
							<p class="mb-0"><strong><?php _e('You will also no longer qualify for our contest entries', 'paid-memberships-pro' ); ?></strong></p>
						</div>
					</div>
					<?php
				}
				else
				{
					if ( strtotime($current_user->user_registered) < strtotime('2021-1-10') ) { ?>
						<h3>We're sad to see you go!</h3>
						<div class="w-75 mx-auto alert-wrapper mt-4 bg-white d-block position-relative">
							<div class="position-relative bg-white alert" style="border: 2px solid #43d3cb;">
								<p><strong><?php _e('Letter\'s and contest entries you\'ve posted may be removed from the site or set to private when you cancel.', 'paid-memberships-pro' ); ?></strong></p>
								<p class="mb-0"><strong><?php _e('You will also no longer qualify for our contest entries', 'paid-memberships-pro' ); ?></strong></p>
							</div>
						</div>
						<p style="font-size: 1.3rem;">Please email us at <a class="teal fw-bold" href="mailto:lauren@theunsealed.com">lauren@theunsealed.com</a> to cancel</p>
						<p>Include your account email address and your reason for canceling</p>
					<?php
					} else {
						$level_names = $wpdb->get_col("SELECT name FROM $wpdb->pmpro_membership_levels WHERE id IN('" . implode("','", $old_level_ids) . "')");
						?>
						<p><?php printf(_n('Are you sure you want to cancel your %s subscription?', 'Are you sure you want to cancel your %s subscription?', count($level_names), 'paid-memberships-pro'), strtolower(pmpro_implodeToEnglish($level_names))); ?></p>
						<div class="d-flex flex-column align-items-center <?php echo pmpro_get_element_class( 'pmpro_actionlinks' ); ?>">
							<a class="col-auto <?php echo pmpro_get_element_class( 'pmpro_btn pmpro_btn-cancel pmpro_nolink nolink', 'pmpro_btn-cancel' ); ?>" href="<?php echo pmpro_url("account")?>"><?php _e('No, I changed my mind', 'paid-memberships-pro' );?></a>
							<a class="col-auto <?php echo pmpro_get_element_class( 'pmpro_btn pmpro_btn-submit pmpro_yeslink yeslink', 'pmpro_btn-submit' ); ?>" href="<?php echo pmpro_url("cancel", "?levelstocancel=" . esc_attr($_REQUEST['levelstocancel']) . "&confirm=true")?>"><?php _e('Yes, cancel my subscription', 'paid-memberships-pro' );?></a>
						</div>
					<?php
					}
				}
			}
			else
			{
				if($current_user->membership_level->ID)
				{
					?>
					<h2><?php _e("My Subscription", 'paid-memberships-pro' );?></h2>
					<?php
					if ( count($current_user->membership_levels) > 1 ) { ?>
						<table class="<?php echo pmpro_get_element_class( 'pmpro_table' ); ?>" width="100%" cellpadding="0" cellspacing="0" border="0">
							<thead>
								<tr>
									<th><?php _e("Level", 'paid-memberships-pro' );?></th>
									<th><?php _e("Expiration", 'paid-memberships-pro' ); ?></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$current_user->membership_levels = pmpro_getMembershipLevelsForUser($current_user->ID);
								foreach($current_user->membership_levels as $level) {
								?>
								<tr>
									<td class="<?php echo pmpro_get_element_class( 'pmpro_cancel-membership-levelname' ); ?>">
										<?php echo $level->name ?>
									</td>
									<td class="<?php echo pmpro_get_element_class( 'pmpro_cancel-membership-expiration' ); ?>">
									<?php
										if($level->enddate) {
											$expiration_text = date_i18n( get_option( 'date_format' ), $level->enddate );
	 										} else {
	 											$expiration_text = "Ongoing";
										}

										echo apply_filters( 'pmpro_account_membership_expiration_text', $expiration_text, $level );
									?>
									</td>
									<td class="<?php echo pmpro_get_element_class( 'pmpro_cancel-membership-cancel' ); ?>">
										<a href="<?php echo pmpro_url("cancel", "?levelstocancel=" . $level->id)?>"><?php _e("Cancel", 'paid-memberships-pro' );?></a>
									</td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>
						<div class="<?php echo pmpro_get_element_class( 'pmpro_actions_nav' ); ?>">
							<a href="<?php echo pmpro_url("cancel", "?levelstocancel=all"); ?>"><?php _e("Cancel All Memberships", 'paid-memberships-pro' );?></a>
						</div>
					<?php
					} else {
						$membership = $current_user->membership_levels[0]; ?>
						<div class="d-flex flex-column align-items-center">
							<div class="<?php echo pmpro_get_element_class( 'pmpro_cancel-membership-levelname' ); ?>">
								<?php echo $membership->name ?>
							</div>
							<?php
							if($membership->enddate) {
								$expiration_text = date_i18n( get_option( 'date_format' ), $membership->enddate );
								} else {
									$expiration_text = "Ongoing";
							} ?>
							<div>
								<p>Expiration: <span><?php echo apply_filters( 'pmpro_account_membership_expiration_text', $expiration_text, $membership ); ?></span></p>
							</div>
							<div><a href="<?php echo pmpro_url("cancel", "?levelstocancel=" . $membership->id)?>"><?php _e("Cancel", 'paid-memberships-pro' );?></a>
						</div>
					<?php
					} ?>
					<?php
				}
			}
		}
		else
		{
			?>
			<p class="<?php echo pmpro_get_element_class( 'pmpro_cancel_return_home' ); ?>"><a href="<?php echo get_home_url()?>"><?php _e('Click here to go to the home page.', 'paid-memberships-pro' );?></a></p>
			<?php
		}
	?>
</div> <!-- end pmpro_cancel, pmpro_cancel_wrap -->
