<?php
	global $gateway, $pmpro_review, $skip_account_fields, $pmpro_paypal_token, $wpdb, $current_user, $pmpro_level, $pmpro_levels, $pmpro_show_discount_code, $pmpro_error_fields;
	global $discount_code, $username, $first_name, $last_name, $password, $password2, $bfirstname, $blastname, $baddress1, $baddress2, $bcity, $bstate, $bzipcode, $bcountry, $bphone, $bemail, $bconfirmemail, $CardType, $AccountNumber, $ExpirationMonth,$ExpirationYear;

	/**
	 * Filter to set if PMPro uses email or text as the type for email field inputs.
	 *
	 * @since 1.8.4.5
	 *
	 * @param bool $use_email_type, true to use email type, false to use text type
	 */

	$acknowledge_options = [
    'name' => 'acknowledge',
		'html' => '<p>By signing up, you agree to our <a class="teal" href="https://theunsealed.com/the-unsealed-terms-of-service">terms of service</a>, <a class="teal" href="https://theunsealed.com/the-unsealed-terms-of-sale">terms of sale</a> and <a class="teal" href="https://theunsealed.com/the-unsealed-privacy-policy">privacy policy</a>.</p>',
		'label' => false
  ];
	$acknowledge = new PMProRH_Field('acknowledge', 'html', $acknowledge_options);
	if ( !$pmpro_review ) {
	  pmprorh_add_registration_field('before_submit_button', $acknowledge);
	}
	 // Add first/last name fields to checkout
	//  $first_name_options = $last_name_options = [
	// 		'addMember' => true,
	// 		'label' => 'First name',
	// 		'profile' => true
	// 	];
	// 	$first_name = new PMProRH_Field('first_name', 'text', $first_name_options);
	//  pmprorh_add_registration_field("first_name", $first_name);
	// 	$last_name_options['label'] = 'Last name';
	// 	$last_name = new PMProRH_Field('last_name', 'text', $last_name_options);
	//  pmprorh_add_registration_field("last_name", $last_name);
	$pmpro_email_field_type = apply_filters('pmpro_email_field_type', true);

	// Set the wrapping class for the checkout div based on the default gateway;
	$default_gateway = pmpro_getOption( 'gateway' );
	if ( empty( $default_gateway ) ) {
		$pmpro_checkout_gateway_class = 'pmpro_checkout_gateway-none';
	} else {
		$pmpro_checkout_gateway_class = 'pmpro_checkout_gateway-' . $default_gateway;
	} ?>
	<div id="pmpro_level-<?php echo $pmpro_level->id; ?>" class="<?php echo pmpro_get_element_class( $pmpro_checkout_gateway_class, 'pmpro_level-' . $pmpro_level->id ); ?>">

		<form id="pmpro_form" class="<?php echo pmpro_get_element_class( 'pmpro_form' ); ?>" action="<?php if(!empty($_REQUEST['review'])) echo pmpro_url("checkout", "?level=" . $pmpro_level->id); ?>" method="post">

			<input type="hidden" id="level" name="level" value="<?php echo esc_attr($pmpro_level->id) ?>" />
			<input type="hidden" id="checkjavascript" name="checkjavascript" value="1" />
			<?php if ($discount_code && $pmpro_review) { ?>
				<input class="<?php echo pmpro_get_element_class( 'input pmpro_alter_price', 'discount_code' ); ?>" id="discount_code" name="discount_code" type="hidden" size="20" value="<?php echo esc_attr($discount_code) ?>" />
			<?php } ?>

			<?php if($pmpro_review) { ?>
				<p><?php _e('Almost done. Review the membership information and pricing below then <strong>click the "Complete Payment" button</strong> to finish your order.', 'paid-memberships-pro' );?></p>
			<?php } ?>

			<?php
			$include_pricing_fields = apply_filters( 'pmpro_include_pricing_fields', true );

			if ( !$pmpro_review ) {
				unsealed_display_subscription_levels();

				unsealed_display_level_options();

				unsealed_display_account_fields();
			}

			unsealed_display_payment_form(); ?>

		</form>
		<?php do_action('pmpro_checkout_after_form'); ?>

	</div> <!-- end pmpro_level-ID -->
	
	<script>
		var currentStep = jQuery('.checkout-step:visible').data('step-id')
		var topOfCheckout = document.getElementById('unsealed-checkout-header')
		var rect = topOfCheckout.getBoundingClientRect(),
		scrollTop = window.pageYOffset || document.documentElement.scrollTop;
		var topPos = rect.top + scrollTop - 50

		function customizeContribution (level) {
			jQuery('#custom-contribution-' + level).slideToggle()
		}
		function checkoutNextStep (step) {
			if ( step === currentStep ) {
				return
			}
			if ( step === 1 ) {
				jQuery('.level-radio').each( (i, radio) => {
					jQuery(radio).prop('checked', false)
				})
			}
			currentStep = step

			jQuery('.checkout-step').removeClass('visible')
			jQuery('#checkout-step-' + step).addClass('visible')
			var stepTitle = jQuery('#checkout-step-' + step).data('step')
			// toggle timeline
			jQuery('.timeline-option').removeClass('current')
			jQuery('.timeline-option[data-step="' + step + '"]').addClass('active current')
			for ( var i = 1; i < 5; i++ ) {
				if ( i > step ) {
					jQuery('.timeline-option[data-step="' + i + '"]').removeClass('active current')
				}
			}

			window.scrollTo({ top: topPos, behavior: 'smooth' });
			// window.history.pushState("", stepTitle, "?" + string_to_slug(stepTitle));
		}

		function pricing_continue (step, level, min) {
			window.scrollTo({ top: topPos, behavior: 'smooth' });
			if ( step === currentStep ) {
				return
			}
			var price = jQuery('input[name="level-' + level + '-price"]:checked').val()
			if ( price === '0' ) {
				price = jQuery('input[name="level-' + level + '-custom_price"]').val()
			}
			if ( price < min ) {
				jQuery('.pricing-alert').show()
				return
			}
			currentStep = step
			jQuery('.checkout-step').removeClass('visible')
			jQuery('#checkout-step-' + step).addClass('visible')
			var stepTitle = jQuery('#checkout-step-' + step).data('step')
			// toggle timeline
			jQuery('.timeline-option').removeClass('current')
			jQuery('.timeline-option[data-step="' + step + '"]').addClass('active current')
			for ( var i = 1; i < 5; i++ ) {
				if ( i > step ) {
					jQuery('.timeline-option[data-step="' + i + '"]').removeClass('active current')
				}
			}

			// window.history.pushState("", stepTitle, "?" + string_to_slug(stepTitle));
		}

		jQuery('#pmpro_mailing_lists input').attr('checked', true)

		function validateForm () {
			let validated = true
			const $username = document.getElementById('username')
			const $password = document.getElementById('password')
			const $password2 = document.getElementById('password2')
			const $email = document.getElementById('bemail')
			const $emailconfirm = document.getElementById('bconfirmemail')
			const $first_name = document.getElementById('first_name')
			const $last_name = document.getElementById('last_name')
			const $tos_checkbox = document.getElementById('tos')
			const $user_errors = document.getElementById('pmpro_user_fields_errors')
			$user_errors.innerHTML = ''

			if ( $username !== null && $username.value == '' ) {
				validated = false
				$user_errors.innerHTML = $user_errors.innerHTML + '<p>Please provide a username</p>'
			}
			if ( $username !== null && $username.classList.contains('error') ) {
				validated = false
				$user_errors.innerHTML = $user_errors.innerHTML + '<p>Please try a different username</p>'
			}
			if ( $password !== null && $password.value == '' ) {
				validated = false
				$user_errors.innerHTML = $user_errors.innerHTML + '<p>Please provide a password</p>'
			}
			if ( $password !== null && $password.value !== $password2.value ) {
				validated = false
				$user_errors.innerHTML = $user_errors.innerHTML + '<p>Passwords do not match</p>'
			}
			if ( $email !== null && $email.value == '' ) {
				validated = false
				$user_errors.innerHTML = $user_errors.innerHTML + '<p>Please provide an email address</p>'
			} else {
				if ( $email !== null && !validateEmail($email.value) ) {
					validated = false
					$user_errors.innerHTML = $user_errors.innerHTML + '<p>Please provide a valid email address</p>'
				}
			}
			if ( $email !== null && $email.value !== $emailconfirm.value ) {
				validated = false
				$user_errors.innerHTML = $user_errors.innerHTML + '<p>Email fields do not match</p>'
			}
			if ( $first_name !== null && $first_name.value === '' ) {
				validated = false
				$user_errors.innerHTML = $user_errors.innerHTML + '<p>Please provide a first name (you can change the settings in your profile to hide this after sign up)</p>'
			}
			if ( $last_name !== null && $last_name.value === '' ) {
				validated = false
				$user_errors.innerHTML = $user_errors.innerHTML + '<p>Please provide a last name (you can change the settings in your profile to hide this after sign up)</p>'
			}
			if ( $tos_checkbox !== null && !$tos_checkbox.checked ) {
				validated = false
				$user_errors.innerHTML = $user_errors.innerHTML + '<p>Please check the box to accept our terms & conditions</p>'
			}
			if ( $email !== null && $email.classList.contains('error') ) {
				$user_errors.innerHTML = `<p class="error-email">It looks like you\'re already subscribed! <a href="<?php echo home_url('login'); ?>">Login now</a></p>`
			}
			return validated
		}

		function account_continue (step) {
			if ( step === currentStep ) {
				return
			}
			const validated = validateForm()
			if ( !validated ) {
				window.scrollTo({ top: topPos, behavior: 'smooth' });
				return
			}
			currentStep = step
			jQuery('.checkout-step').removeClass('visible')
			jQuery('#checkout-step-' + step).addClass('visible')
			var stepTitle = jQuery('#checkout-step-' + step).data('step')
			// toggle timeline
			jQuery('.timeline-option').removeClass('current')
			jQuery('.timeline-option[data-step="' + step + '"]').addClass('active current')
			for ( var i = 1; i < 5; i++ ) {
				if ( i > step ) {
					jQuery('.timeline-option[data-step="' + i + '"]').removeClass('active current')
				}
			}
			window.scrollTo({ top: topPos, behavior: 'smooth' });
			// window.history.pushState("", stepTitle, "?" + string_to_slug(stepTitle));
		}

		function validateEmail(mail) {
		 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
		  {
		    return (true)
		  }
		    return (false)
		}

		var form = document.getElementById('pmpro_form');
		form.addEventListener('submit', function (e) {
			console.log(currentStep)
			if ( currentStep === 1 ) {
				e.preventDefault();
			}
			if ( currentStep === 2 ) {
				e.preventDefault();
				jQuery('#checkout-step-2 .submit-btn').click();
			}
			if ( currentStep === 3 ) {
				e.preventDefault()
				jQuery('#checkout-step-3 .submit-btn').click();
			}

		});

		function string_to_slug (str) {
	    str = str.replace(/^\s+|\s+$/g, ''); // trim
	    str = str.toLowerCase();

	    // remove accents, swap ñ for n, etc
	    var from = "àáãäâèéëêìíïîòóöôùúüûñç·/_,:;";
	    var to   = "aaaaaeeeeiiiioooouuuunc------";

	    for (var i=0, l=from.length ; i<l ; i++) {
	        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	    }

	    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
	        .replace(/\s+/g, '-') // collapse whitespace and replace by -
	        .replace(/-+/g, '-'); // collapse dashes

	    return str;
		}

	</script>
