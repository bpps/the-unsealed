<?php
/**
 * Template Name:  Account
 *
 * The template for displaying the account pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Unsealed
*/

get_header(null, ["white_nav" => true]);
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php
    while ( have_posts() ) :
    the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="container py-3 py-md-5">
          <header class="entry-header">
            <?php
            if ( is_singular() ) :
            the_title( '<h1 class="entry-title text-center script-font display-3">', '</h1>' );
            else :
            the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
            endif;

            if ( 'post' === get_post_type() ) :
            ?>
            <?php endif; ?>
          </header><!-- .entry-header -->

          <div class="entry-content text-center">
            <?php
            the_content( sprintf(
            wp_kses(
                /* translators: %s: Name of current post. Only visible to screen readers */
                __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'wowgulpwpstarter' ),
                array(
                    'span' => array(
                        'class' => array(),
                    ),
                )
            ),
            get_the_title()
            ) );

            wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wowgulpwpstarter' ),
            'after'  => '</div>',
            ) );
            ?>
          </div><!-- .entry-content -->
        </div>
      </article><!-- #post-<?php the_ID(); ?> -->

      <?php

      // If comments are open or we have at least one comment, load up the comment template.
      if ( comments_open() || get_comments_number() ) :
          comments_template();
      endif;

    endwhile; // End of the loop.
    ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
