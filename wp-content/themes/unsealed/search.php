<?php
/*
Template Name: Search Page
*/

get_header(); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <header class="entry-header py-5 text-center">
      <h1 class="mb-5"><?php the_search_query(); ?></h1>
      <?php echo get_search_form(); ?>
    </header>
    <?php
    $bp_search_args = ['search_terms' => get_search_query(), 'per_page' => 20, 'page' => 1];
    if ( bp_has_activities($bp_search_args) ) {
      $activities = bp_activity_get($bp_search_args); ?>
      <div class="pb-5 container-wide">
        <h2 class="text-center">The Unsealed Member Letters</h2>
        <div class="letters d-flex flex-wrap">
          <?php
          foreach( $activities['activities'] as $activity ) {
            $link = bp_activity_get_permalink($activity->id);
            $thumbnail = bp_activity_get_meta($activity->id, 'featured_image');
            $thumbnail = wp_get_attachment_image_url($thumbnail, 'small-medium');
            $title = bp_activity_get_meta($activity->id, 'title');
            $excerpt = $activity->content;
            if (strlen($excerpt) > 80) {
              $excerpt = substr($excerpt, 0, 80) . '...';
            } ?> 
            <div class="col-sm-6 col-md-4 col-lg-3">
              <div class="letter-block col-12 large-letter col-md-12">
                <div class="row narrow-gutter pt-3 pb-3 underlined underlined-light">
                  <div class="col-md-12">
                    <div class="letter-image-container image-ratio bg-2-1">
                      <a href="<?php echo $link; ?>" class="letter-image bg-cover" style="background-image:url(<?php echo $thumbnail; ?>)" aria-label="<?php echo $title; ?>" tabindex="-1">
                      </a>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="pr-md-4">
                      <div class="pt-2"><a href="<?php echo $link; ?>" aria-label="<?php echo $title; ?>"><b><?php echo $title; ?></b></a></div>
                      <?php
                      if ( $excerpt ) { ?>
                        <div class="pt-3">
                          <?php echo $excerpt; ?>
                        </div>
                      <?php
                      } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php 
          }
        } ?>
      </div>
    </div>
    <?php 
    if ( have_posts() ) : ?>
      <div class="pb-5 container-wide mt-5">
        <h2 class="text-center">The Unsealed Content</h2>
        <div class="letters d-flex flex-wrap">
          <?php
          while ( have_posts() ) : the_post(); ?>
            <div class="col-sm-6 col-md-4 col-lg-3">
              <?php
              echo get_letter_lockup( get_the_ID(), true ); ?>
            </div>
          <?php
          endwhile; ?>
        </div>
        <div class="pagination container d-flex justify-content-center pt-5 pb-3">
          <?php
          $big = 999999999;
          echo paginate_links( array(
            'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'prev_text' => '<',
            'next_text' => '>'
          ) ); ?>
        </div>
      </div>
    <?php
    else :
    endif; ?>

 </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
