<?php
get_header();
$video_page = home_url('videos');
$current_id = get_the_ID();
$video_slug = basename(get_permalink());
$position_query = array( 'post_type' => 'video', 'numberposts' => -1 );
$position_posts = get_posts($position_query);
$count = $position = 0;
foreach ($position_posts as $position_post) {
  $count++;
  if ($position_post->ID == $current_id) {
    $position = $count;
    break;
  }
}
$posts_per_page = get_option('posts_per_page');
$result = $position/$posts_per_page;
$current_page = ceil($result);
if ( $current_page > 1 ) {
  $video_page .= '/page/' . $current_page;
}
$video_page .= '?video_id=' . $video_slug; ?>
<div id="primary" class="content-area vh-100 bg-black position-relative">
  <main id="main" class="site-main h-100">
    <article style="height: 100%;" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <div class="container redirect-video h-100" data-redirect="<?php echo $video_page; ?>">
        <div class="d-flex justify-content-center px-12 h-100 align-items-center">
          <div class="text-center">
            <p>Redirecting to <?php the_title(); ?></p>
          </div>
        </div>
      </div>
    </article>
  </main>
</div>
<?php
get_footer(); ?>
