<?php
/**
 * Block Name: Unsealed Video
 *
 */
$unsealed_video = get_field('video');
$unsealed_video_embed = get_field('video_embed_link');
$unsealed_video_url = $unsealed_video_embed ? $unsealed_video_embed : $unsealed_video['url'];
$placeholder = get_field('video_placeholder');
$placeholder = $placeholder ? $placeholder['sizes']['medium'] : get_template_directory_uri() . '/screenshot.png';
$video_caption = get_field('video_caption');
$blocked = get_field('blocked');
$is_black_bg = get_field('black_background');

if ( $blocked ) {
  if (function_exists( 'pmpro_hasMembershipLevel' ) )   {
    $blocked = !pmpro_hasMembershipLevel();
  }
}
?>
<section class="unsealed-video-block pb-5 <?=($is_black_bg ? 'black-background' : '');?>">
  <?php
  if ( $is_black_bg ) { ?>
    <div class="unsealed-video-background"></div>
  <?php
  } ?>
  <div class="unsealed-video-container">
    <?php
    if ( $unsealed_video_url ) { ?>
      <div class="unsealed-video position-relative mb-2">
        <div class="image-ratio bg-3-2">
          <div class="bg-cover" style="background-image:url(<?php echo $placeholder; ?>);">
          </div>
        </div>
        <?php
        if ( $blocked ) { ?>
          <div class="overlay video-overlay d-flex justify-content-center align-items-center">
            <div class="video-overlay-content text-white text-center px-2 px-md-5">
              <h3 class="h1 mb-3">Premium Content</h3>
              <p class="font-weight-bold">
                <?php if ( $video_caption ) { echo $video_caption . ' '; } ?>
                <span>You must be a <a class="teal hover-white" href="<?php echo pmpro_url('checkout', '', 'https'); ?>">subscriber</a> to watch this video.</span>
              </p>
            </div>
          </div>
        <?php
        } ?>
        <button
          class="play-icon rounded-circle d-flex justify-content-center align-items-center p-0<?php if ( $blocked ) { echo ' disabled'; } ?>"
          data-video-src="<?php echo $unsealed_video_url; ?>"
          data-toggle="modal"
          data-target="#letter-video-<?php echo $block['id']; ?>">
				</button>
      </div>
      <?php
      if ( !$blocked ) {
        if ( $video_caption ) { ?>
          <figcaption class="font-light text-center thin px-md-3"><?php echo $video_caption; ?></figcaption>
        <?php
        }
        $video_type = isset($unsealed_video['filename']) ? $unsealed_video['filename'] : '';
        if ( $video_type ) {
          $video_type = explode( '.', $video_type );
          $video_type = $video_type[1];
        } ?>
        <div class="modal fade video-modal" id="letter-video-<?php echo $block['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="ourMissionModal" aria-hidden="true">
        	<div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
        		<div class="modal-content">
        			<div class="modal-body">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
      						<span aria-hidden="true">&times;</span>
      					</button>
        				<!-- 16:9 aspect ratio -->
        				<div class="embed-responsive embed-responsive-16by9">
                  <video controls poster="<?php echo $placeholder; ?>" src="<?php echo $unsealed_video_url; ?>" class="video">
                    <source src="<?php echo $unsealed_video_url; ?>" type="video/<?php echo $video_type; ?>">
                    Your browser does not support the video tag.
                  </video>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
      <?php
      }
    } ?>
  </div>
</section>
