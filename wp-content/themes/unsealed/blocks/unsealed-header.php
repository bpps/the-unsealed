<?php
/**
 * Block Name: Unsealed Page Header
 *
 */
$title = get_field('header_title');
$logged_in_title = get_field('logged_in_title');
$post_title = get_field('header_post_title');
$description = get_field('header_description');
$logged_in_description = get_field('logged_in_description');
$video = get_field('header_video');
$placeholder = get_field('header_video_placeholder');
$banner_text = get_field('banner_text');
$banner_link = get_field('banner_link');
$login_link = get_field('login_link');
if ( pmpro_hasMembershipLevel() ) {
  if ( $logged_in_description ) {
    $description = $logged_in_description;
  }
} ?>
<header class="entry-header custom-header">
  <div class="container">
    <div class="d-flex align-items-center row">
      <div class="col-md-5 pt-4 pb-5">
        <h1 class="mb-5 font-weight-light <?php if ( $title ) { echo 'h3'; } ?>">
          <?php
          echo pmpro_hasMembershipLevel() && $logged_in_title ? $logged_in_title : get_the_title(); ?>
        </h1>
        <?php
        if ( $title ) { ?>
          <div class="header-title my-0 h1">
            <?php
            echo $title; ?>
          </div>
        <?php
        }
        if ( $post_title ) { ?>
          <p class="h5 mt-4"><?php echo $post_title; ?></p>
        <?php
        }
        if ( $description ) { ?>
          <p class="mt-5"><?php echo $description; ?></p>
        <?php
        }
        if ( $login_link ) {
          $btn_label = 'Write A Letter Now';
          $btn_url = pmpro_url("login", '?redirect_to=' . wp_get_referer(), "https");
          if ( is_user_logged_in() ) {
            if ( pmpro_hasMembershipLevel() ) {
              $btn_url = bp_loggedin_user_domain();
            } else {
              $btn_url = pmpro_url("checkout", '?redirect_to=' . bp_loggedin_user_domain(), "https");
            }
          }
          echo unsealed_btn($btn_label, $btn_url, 'large');
        } ?>
      </div>
      <div class="col-md-7 py-5 px-md-5">
        <div class="inline-video-wrapper">
          <div class="embed-responsive embed-responsive-16by9 position-relative unsealed-video-block inline-video">
            <video poster="<?php echo $placeholder['sizes']['medium-large']; ?>">
              <source src="<?php echo $video['url']; ?>" type="video/mp4">
              Your browser does not support the video tag.
            </video>
            <button class="play-icon rounded-circle d-flex justify-content-center align-items-center p-0">
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</header><!-- .entry-header -->
<?php
if ( $banner_text || $banner_link ) { ?>
  <div id="announcement" class="content bg-black px-2 py-3 sticky-top">
    <div class="container">
      <div class="row justify-content-center align-items-center h5 mb-0">
        <?php
        if ( $banner_text ) { ?>
          <div class="col-auto">
            <?php echo $banner_text; ?>
          </div>
        <?php
        }
        if ( $banner_link ) { ?>
          <div class="col-auto">
            <?php echo unsealed_btn($banner_link['title'], $banner_link['url'], 'small'); ?>
          </div>
        <?php
        } ?>
      </div>
    </div>
  </div>
<?php
} ?>
