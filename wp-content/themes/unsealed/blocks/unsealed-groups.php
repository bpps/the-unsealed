<?php
/**
 * Block Name: Unsealed Groups
 *
 */
$groups_title = get_field('title');
$randomize = get_field('randomize'); ?>
<div id="unsealed-groups" class="pb-4">
  <div class="container">
    <?php
    if ( $groups_title ) { ?>
      <h2 class="text-center my-5"><?php echo $groups_title; ?></h2>
    <?php
    }
    $vgroups = BP_Groups_Group::get(
      [
         // 'type'=>'alphabetical',
         'per_page'=> - 1
      ]
    );

    foreach ($vgroups as $type => $vgroup) {
      if( $type == 'groups' ){
        if( is_array( $vgroup ) ){
          if ( $randomize ) {
            shuffle($vgroup);
          }
          foreach( $vgroup as $penpal_group ) {
            $penpal_group = (array) $penpal_group;
            // print_r( $group ); // this will be useful to help you get the keys
            $group_id  = $penpal_group['id'];
            $name  = $penpal_group['name'];
            $slug  = $penpal_group['slug'];
            $description  = $penpal_group['description'];
            $group = groups_get_group($group_id);
            $args = [
              'action' => 'activity_update',
              'secondary_item_id' => 0,
              'primary_id' => $group_id,
              'per_page' => 3,
              'display_comments' => false,
              'filter' => ['object' => 'groups']
            ];
            if ( bp_has_activities( $args ) ) : ?>
              <div class="group-block underlined underlined-light pt-5 pb-3">
                <div class="group-block-header row align-items-center pb-5">
                  <?php
                  $thumbnail = get_template_directory_uri() . '/img/placeholder.png';
                  // $avatar_options = array ( 'item_id' => $group_id, 'object' => 'group', 'type' => 'full', 'avatar_dir' => 'group-avatars', 'alt' => 'Group avatar', 'class' => 'avatar', 'width' => 200, 'height' => 200, 'html' => true );
                  $avatar_url = bp_get_group_avatar_url( $group, 'full' ); ?>
                  <div class="col-12 col-md-3 position-relative">
                    <div class="group-block-avatar-container">
                      <img src="<?php echo $avatar_url; ?>"/>
                    </div>
                  </div>
                  <div class="group-block-header-content text-center text-md-left col-12 col-md-9">
                    <h3 class="h4"><?php echo $penpal_group['name']; ?></h3>
                    <?php
                    if ( $description ) { ?>
                      <div class="group-block-desc">
                        <?php echo apply_filters('the_content', $description); ?>
                      </div>
                    <?php
                    }
                    $btn_label = 'Join Group';
                    $btn_url = pmpro_url("login", '?redirect_to=' . wp_nonce_url( bp_get_group_permalink( $group ) . 'join', 'groups_join_group' ), "https");

                    if ( is_user_logged_in() ) {
                      if ( pmpro_hasMembershipLevel() ) {
                        if ( groups_is_user_member( bp_loggedin_user_id(), $group_id ) ) {
                          $btn_label = 'Visit Group';
                          $btn_url = bp_get_group_permalink( $group );
                        } else {
                          $btn_url = wp_nonce_url( bp_get_group_permalink( $group ) . 'join', 'groups_join_group' );
                        }
                      } else {
                        $btn_url = pmpro_url("checkout", '?redirect_to=' . wp_nonce_url( bp_get_group_permalink( $group ) . 'join', 'groups_join_group' ), "https");
                      }
                    }
                    echo unsealed_btn($btn_label, $btn_url, 'small'); ?>
                  </div>
                </div>
                <div class="group-block-groups row">
                  <?php
                  while ( bp_activities() ) : bp_the_activity();
                    if ( $group_id = bp_get_activity_item_id() ) :
                      $link = bp_activity_get_permalink(bp_get_activity_id());
                      $title = bp_activity_get_meta( bp_get_activity_id(), 'title' );
                      $image_id = bp_activity_get_meta(bp_get_activity_id(), 'featured_image'); ?>
                      <div class="col-12 col-sm-6 col-md-4" data-activity-id="<?php echo bp_get_activity_id(); ?>">
                        <div class="pt-3 pb-3">
                          <div class="w-100">
                            <div class="group-block-image position-relative">
                              <a href="<?php echo $link; ?>" class="d-block">
                                <?php echo wp_get_attachment_image($image_id, 'small-medium', false, [ 'class' => 'w-100' ] ); ?>
                              </a>
                            </div>
                            <h4 class="h5 mt-3"><a href="<?php echo $link; ?>" class="d-block"><?php echo $title; ?></a></h4>
                            <?php
                            if ( bp_activity_has_content() ) { ?>
                              <div class="small">
                                <?php
                                $content = bp_get_activity_content_body();
                                $character_limit = 80;
                                if ( $content ) {
                                  $content = wp_strip_all_tags($content, true);
                                  $shortened_content = substr($content, 0, $character_limit);
                                  if (strlen($content) > $character_limit) {
                                    $shortened_content .= '...';
                                  }
                                  echo $shortened_content;
                                } ?>
                              </div>
                              <?php
                              $activity_user_id = bp_get_activity_user_id();
                              $activity_user = get_userdata($activity_user_id); ?>
                              <a class="teal mt-3 font-weight-bold d-block" href="<?php echo bp_activity_user_link(); ?>">- <?php echo $activity_user->display_name; ?></a>

                              <?php
                              $btn_url = pmpro_url("login", '?redirect_to=' . $link, "https");

                              if ( is_user_logged_in() ) {
                                if ( pmpro_hasMembershipLevel() ) {
                                  $btn_url = $link;
                                } else {
                                  $btn_url = pmpro_url("checkout", '?redirect_to=' . $link, "https");
                                }
                              } ?>
                              <div class="text-right mt-2">
                                <a class="h2 text-right script-font" href="<?php echo $link; ?>">Write back</a>
                              </div>
                            <?php
                            } ?>
                          </div>
                        </div>
                      </div>
                      <?php
                    endif;
                  endwhile; ?>
                </div>
                <div class="text-right mt-4">
                  <a href="<?php echo bp_get_group_permalink( $group ); ?>">All letters in group</a>
                </div>
              </div>
            <?php
            endif;
          }
        }
      }
    } ?>
  </div>
</div>
