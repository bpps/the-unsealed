<?php
/**
 * Block Name: Unsealed Page Columns
 *
 */
$title = get_field('title');
$column1 = get_field('column_1');
$column2 = get_field('column_2');
$background = get_field('background_image'); ?>
<div class="columns-wrapper py-5 position-relative">
  <div class="overlay bg-cover" style="background-image:url(<?php echo $background['sizes']['large']; ?>)"></div>
  <div class="overlay video-overlay"></div>
  <?php
  if ( $title ) { ?>
    <h2 class="my-4 font-weight-light display-3 teal text-center script-font position-relative"><?php echo $title; ?></h2>
  <?php
  }
  if ( $column1 || $column2 ) { ?>
    <div class="container text-white py-5">
      <div class="row">
        <?php
        if ( $column1 ) { ?>
          <div class="column col-12 col-md-6">
            <?php echo $column1; ?>
          </div>
        <?php
        }
        if ( $column2 ) { ?>
          <div class="column col-12 col-md-6">
            <?php echo $column2; ?>
          </div>
        <?php
        } ?>
      </div>
    </div>
  <?php
  } ?>
</div>
