<?php
/**
 * Block Name: Unsealed Testimonials
 *
 */
$testimonials = array();
if( have_rows('testimonials') ):
  while ( have_rows('testimonials') ) : the_row();
    $testimonial_author = get_sub_field('testimonial_author');
    $testimonial_image = get_sub_field('testimonial_image');
    $testimonial_text = get_sub_field('testimonial_text');
    array_push($testimonials, (object)[
      'author' => $testimonial_author,
      'image' => $testimonial_image,
      'text' => $testimonial_text,
    ]);
  endwhile;
else :
  // no rows found
endif;
?>
<div class="unsealed-testimonials">
  <span class="my-4 font-weight-light display-3 text-center script-font position-relative">
    Testimonials
  </span>
  <div class="unsealed-arrow testimonial-arrow arrow-left">
  </div>
  <div class="testimonials">
    <?php
      foreach ($testimonials as $testimonial) {
        $image = $testimonial->image;
    ?>
      <div class="slide-item">
        <?php
        if ( $image ) { ?>
        <div class="image-container">
          <img src="<?php echo $image['sizes']['medium-large']; ?>" alt="<?php echo $image['title']; ?>" />
        </div>
        <?php
        } ?>
        <div class="testimonial-content<?php if ( !$image ) { echo ' no-image'; } ?>">
          <p class="testimonial-mark teal script-font display-1">"</p>
          <p class="h4 mb-3"><?php echo $testimonial->text; ?></p>
          <p class="script-font display-4"><?php echo $testimonial->author; ?></p>
        </div>
      </div>
    <?php
      }
    ?>
  </div>
  <div class="unsealed-arrow testimonial-arrow arrow-right">
  </div>
</div>
