<?php
/**
 * Block Name: Unsealed Page Columns
 *
 */
$button = get_field('button');
if ( $button ) { ?>
<div class="container pb-5">
  <div class="row justify-content-center">
    <?php
    echo unsealed_btn($button['title'], $button['url'], '', false, $button['target']) ?>
  </div>
</div>
<?php
} ?>
