<?php
/**
 * Block Name: Unsealed Signature
 *
 */
$unsealed_signature = get_field('signature'); ?>
<div class="unsealed-signature">
  <span class="script-font h1">
    <?php echo $unsealed_signature ? $unsealed_signature : 'Lauren Brill'; ?>
  </span>
</div>
