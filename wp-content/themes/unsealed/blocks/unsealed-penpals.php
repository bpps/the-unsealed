<?php
/**
 * Block Name: Unsealed Penpals
 *
 */
$members_title = get_field('title');
$members_text = get_field('sub_text'); ?>
<div id="unsealed-penpals" class="bg-black pt-3 pb-5">
  <div class="container">
    <?php
    if ( $members_title ) { ?>
      <h2 class="text-center my-5 text-white"><?php echo $members_title; ?></h2>
    <?php
    }
    if ( $members_text) { ?>
      <div class="text-center text-white" style="max-width: 600px; margin: 0 auto;"><?php echo $members_text; ?></div>
    <?php
    } ?>
    <div class="row mt-5">
      <?php
      echo get_recent_pen_pals(4); ?>
    </div>
    <div class="mt-5 pt-2 text-center">
      <?php echo unsealed_btn('Search Pen Pals', home_url('members'), 'small'); ?>
    </div>
  </div>
</div>
