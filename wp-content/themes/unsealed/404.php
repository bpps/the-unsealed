<?php
/*
Template Name: Search Page
*/

get_header(); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <header class="entry-header py-5 text-center">
      <h1 class="mb-5">That page doesn't exist!</h1>
      <h2 class="h3">Try searching for something else or explore some of these featured letters.</h2>
      <?php echo get_search_form(); ?>
    </header>
    <style>
      .section-header {
        display: none;
      }
    </style>
    <?php
    echo get_featured_letters(); ?>

 </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
