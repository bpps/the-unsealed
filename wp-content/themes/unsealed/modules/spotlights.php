<?php
function get_spotlights($id = false) {
  ob_start();
  $id = $id ? $id : get_option('page_on_front');
  if ( $spotlights = get_field('spotlights', $id ) ) { ?>
    <section class="container wide-container py-3 py-md-5 spotlights">
      <?php
      if ( $header = get_field('spotlights_header', $id ) ) { ?>
        <div class="section-header">
          <h2 class="text-center mb-4"><?php echo $header; ?></h2>
        </div>
      <?php
      } ?>
      <div class="row">
        <?php
        foreach( $spotlights as $spotlight ) { ?>
          <div class="col-md-6 spotlight-wrapper position-relative">
            <?php echo get_spotlight_lockup($spotlight); ?>
          </div>
        <?php
        } ?>
      </div>
    </section>
  <?php
  }
  return ob_get_clean();
} ?>
