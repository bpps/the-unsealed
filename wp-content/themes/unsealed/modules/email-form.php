<?php
function email_form($id = false) {
  ob_start(); ?>
  <section class="py-3 py-md-5 bg-black email-form" id="footer-email-form-wrapper">
    <div class="container">
			<form action="" id="footer-email-form" class="justify-content-center">
        <input type="email" placeholder="Get email updates" required/>
        <input type="submit"/>
      </form>
      <div class="email-response text-center"></div>
		</div>
  </section>
  <?php
  return ob_get_clean();
} ?>
