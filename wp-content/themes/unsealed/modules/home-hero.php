<?php
function get_home_hero($id = false) {
  ob_start();
  $id = $id ? $id : get_option('page_on_front');
  $large_image = get_field('large_image', $id); ?>
  <section class="py-3 pt-5 py-md-5 bg-black parallax<?php echo $large_image ? ' has-hero-image' : ''; ?>" id="home-hero">
    <div class="container overflow-hidden">
      <?php
      if ( $header = get_field('home_hero_header', $id ) ) { ?>
        <div class="section-header mb-5">
          <div class="text-center">
            <?php echo $header; ?>
          </div>
        </div>
      <?php
      }
      if ( $large_image ) { ?>
        <div class="header-images-container">
          <div class="lead-image">
            <img src="<?php echo $large_image['sizes']['large']; ?>" alt="lead image" />
          </div>
          <?php
          if ( $images = get_field('images', $id) ) { ?>
            <div class="image-grid" tabindex="-1">
              <?php
              foreach($images as $image) { ?>
                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="image grid" />
              <?php
              } ?>
            </div>
          <?php
          } ?>
          <div class="header-accents">
            <img src="<?php echo get_template_directory_uri() . '/img/accent-4.png' ?>" alt="" class="accent-top">
            <img src="<?php echo get_template_directory_uri() . '/img/accent-4.png' ?>" alt="" class="accent-bottom">
          </div>
          <div class="join-button d-flex justify-content-center">
            <?php
            echo pmpro_hasMembershipLevel()
            ? unsealed_btn("My Profile", bp_loggedin_user_domain())
            : unsealed_btn("Write A Letter Now", pmpro_url("checkout", "", "https")); ?>
          </div>
          <?php
          $intro_video = get_field('intro_video', 'option');
          if ( $intro_video ) { ?>
            <div class="home-play-button">
              <button class="play-button" data-video-type="upload" data-video-src="<?php echo $intro_video['url']; ?>" data-toggle="modal" data-target="#homepage-modal" aria-label="play-button"></button>
            </div>
          <?php
          } ?>
        </div>
      <?php
    } else { ?>
      <div class="join-button d-flex justify-content-center">
        <?php echo unsealed_btn("Join Now", pmpro_url("checkout", "", "https")); ?>
      </div>
    <?php
    } ?>
    </div>
  </section>
  <?php
  if ( $intro_video ) { ?>
    <div class="modal fade video-modal" id="homepage-modal" tabindex="-1" role="dialog" aria-labelledby="homepageModal" aria-hidden="true">
      <div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            <div class="embed-responsive embed-responsive-16by9">
              <video class="embed-responsive-item video">
                <source type="video/mp4" src="">
              </video>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php
  }
  echo get_announcement();
  return ob_get_clean();
} ?>
