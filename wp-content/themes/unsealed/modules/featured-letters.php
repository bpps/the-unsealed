<?php
function get_featured_letters($id = false) {
  ob_start();
  $id = $id ? $id : get_option('page_on_front');
  $selected_letters = get_field('featured_letters', $id ); ?>

  <section class="container wide-container py-3 py-md-5 featured-letters">
    <?php
    if ( $header = get_field('featured_letters_header', $id ) ) { ?>
      <div class="section-header">
        <h2 class="text-center mb-4"><?php echo $header; ?></h2>
      </div>
    <?php
    } ?>
    <div class="row">
      <?php
      $args = [
        'post_type' => ['post', 'contest-submissions'],
        'post_status' => 'publish',
        'posts_per_page' => $selected_letters ? count($selected_letters) : 5,
      ];
      if ( $selected_letters ) {
        $args['post__in'] = $selected_letters;
        $args['orderby'] = 'post__in';
      }
      $letters_query = new WP_Query( $args );
      if ( $letters_query->have_posts() ) :
        while ( $letters_query->have_posts() ) : $letters_query->the_post();
          $letter_ind = $letters_query->current_post;
          if ( $letter_ind == 0 ) { ?>
            <div class="col-md-6 col-xl-7">
              <?php
              // first letter
              echo get_letter_lockup(get_the_id(), true); ?>
            </div>
            <?php
            continue;
          }
          if ( $letter_ind == 1) { ?>
            <div class="col-md-6 col-xl-5">
              <div class="letter-list overflow-hidden row">
          <?php
          }
          echo get_letter_lockup(get_the_id());
          if ( $letter_ind == $letters_query->post_count - 1 ) { ?>
              </div>
              <div class="d-flex justify-content-end">
                <a class="btn-sm teal mt-2 d-block" href="<?php echo home_url('letters'); ?>">
                  All featured letters
                </a>
              </div>
            </div>
          <?php
          }
        endwhile;
        wp_reset_postdata();
      endif; ?>
    </div>
  </section>
  <?php
  return ob_get_clean();
} ?>
