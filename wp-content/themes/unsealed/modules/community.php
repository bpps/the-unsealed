<?php
function get_from_our_community($id = false) {
  ob_start();
  $id = $id ? $id : get_option('page_on_front');
  $last_image = get_field('last_box_image', $id);
  $last_link = get_field('last_box_link', $id);
  $selected_posts = get_field('community_posts', $id );
  $args = [
    'post_type' => ['community-voices', 'ask-the-unsealed'],
    'post_status' => 'publish',
    'posts_per_page' => $selected_posts ? count($selected_posts) : 3,
  ];
  if ( $selected_posts ) {
    unset($args->post_type);
    $args['post__in'] = $selected_posts;
  }
  $letters_query = new WP_Query( $args );
  if ( $letters_query->have_posts() ) : ?>
    <section class="container community-section py-3 py-md-5 underlined underlined-top">
      <?php
      if ( $header = get_field('from_community_header', $id ) ) { ?>
        <div class="section-header">
          <h2 class="text-center mb-4"><?php echo $header; ?></h2>
        </div>
      <?php
      } ?>
      <div class="row up-3-container">
        <?php
        while ( $letters_query->have_posts() ) : $letters_query->the_post();
          echo get_post_lockup(get_the_ID(), true);
        endwhile;
        wp_reset_postdata(); ?>
      </div>
      <?php
      if ( $last_link ) { ?>
        <div class="d-flex justify-content-center mt-5">
          <?php echo unsealed_btn( $last_link['title'], $last_link['url'], 'small') ?>
        </div>
      <?php
      } ?>
    </section>
  <?php
  endif;

  return ob_get_clean();
} ?>
