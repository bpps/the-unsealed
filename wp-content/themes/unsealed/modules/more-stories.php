<?php
	function more_stories() {
		ob_start();
		global $post;
		$related_ids = [];
		// $tags = get_the_tags($post->ID);
		// if ( $tags ) {
		// 	$tags = array_map( function($tag) { return $tag->term_id; }, $tags );
		// }
		$topics = wp_get_post_terms( $post->ID, 'topic', [ 'fields' => 'ids' ] );
		$args = [
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => 3,
			'orderby' => 'rand',
			'post__not_in' => [$post->ID],
			'tax_query' => [
				// 'relation' => 'OR',
    		[
		      'taxonomy' => 'topic',
		      'field' => 'id',
		      'terms' => $topics
    		],
				// [
		    //   'taxonomy' => 'post_tag',
		    //   'field' => 'id',
		    //   'terms' => $tags
    		// ]
			]
		];
		// NOTE: Could combine these two instances of letters_query to one function
	  $letters_query = new WP_Query( $args );
		if ( $letters_query->have_posts() ) :
			while ( $letters_query->have_posts() ) : $letters_query->the_post();
				array_push( $related_ids, get_the_id() );
			endwhile;
			wp_reset_postdata();
		endif;
		if ( count ( $related_ids ) < 3 ) {
			$args = [
				'post_type' => 'post',
				'orderby' => 'rand',
				'posts_per_page' => 3 - count ( $related_ids ),
				'post_status' => 'publish',
				'post__not_in' => $related_ids
			];
			$letters_query = new WP_Query( $args );
			if ( $letters_query->have_posts() ) :
				while ( $letters_query->have_posts() ) : $letters_query->the_post();
					array_push( $related_ids, get_the_id() );
				endwhile;
				wp_reset_postdata();
			endif;
		}
		if ( count( $related_ids ) ) { ?>
			<section class="container py-3 py-md-5 more-stories">
				<div class="section-header">
					<h2 class="text-center mb-4">More Letters</h2>
				</div>

				<div class="row three-up-container">
					<?php
					foreach ( $related_ids as $rel_id ) {
						echo get_post_lockup( $rel_id );
					} ?>
				</div>
			</section>
		<?php
		}
		$id = get_option('page_on_front');
		$posts = get_field('community_posts', $id );
  return ob_get_clean();
}

function more_contests() {
	ob_start();
	global $post;
	$related_ids = [];
	$args = [
		'post_type' => 'contest',
		'post_status' => 'publish',
		'posts_per_page' => 3,
		'post__not_in' => [$post->ID],
		'orderby' => 'meta_value',
		'meta_key' => 'contest_end_date',
		'order' => 'DESC'
	];
	// NOTE: Could combine these two instances of letters_query to one function
	$contests_query = new WP_Query( $args );
	if ( $contests_query->have_posts() ) :
		while ( $contests_query->have_posts() ) : $contests_query->the_post();
			array_push( $related_ids, get_the_id() );
		endwhile;
		wp_reset_postdata();
	endif;
	if ( count( $related_ids ) ) { ?>
		<section class="container py-3 py-md-5 more-stories">
			<div class="section-header">
				<h2 class="text-center mb-4">More Contests</h2>
			</div>

			<div class="row three-up-container">
				<?php
				foreach ( $related_ids as $rel_id ) {
					echo get_post_lockup( $rel_id );
				} ?>
			</div>
		</section>
	<?php
	}
	return ob_get_clean();
} ?>
