<?php
function unsealed_btn($text = 'Sign Up', $link="", $size = '', $props = false, $target="_self") {
	ob_start();

		if ( $link ) { ?>
			<a class='btn-unsealed <?php echo $size; ?>' target="<?php echo $target; ?>" href="<?php echo $link; ?>"<?php if ($props) { echo ' ' . $props; } ?>>
		<?php
		} else { ?>
			<div class='btn-unsealed <?php echo $size; ?>'
				<?php if ($props) { echo ' ' . $props; } ?>>
		<?php
		} ?>
			<span class="btn-unsealed-text"><?php echo $text; ?></span>
			<div class="btn-background-text" aria-hidden="true">

				<?php for($y = 0; $y < 3; $y++){
					echo '<span>';
					for($x = 0; $x < 30; $x++){
						echo $text . '  ';
					}
					echo '</span>';
				} ?>
			</div>
		<?php
		if ( $link ) { ?>
			</a>
		<?php
		} else { ?>
			</div>
		<?php
		}
  return ob_get_clean();
} ?>
