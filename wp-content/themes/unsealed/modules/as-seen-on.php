<?php
function get_as_seen_on() {
  ob_start(); ?>
  <section class="container pt-5 pb-3" id="as-seen-on">
    <?php
    $header = get_field('as_seen_on_header', 'option');
    $header = $header ? $header : 'As Seen On';
    $content = get_field('as_seen_on_content', 'option'); ?>
    <h2 class="as-seen-header text-center">
      <?php echo $header; ?>
    </h2>
    <?php
    if ( $logos = get_field('as_seen_on_logos', 'option') ) { ?>
      <div class="as-seen-logos mt-5">
        <?php
        foreach( $logos as $logo ) { ?>
          <div class="as-seen-logo-wrapper">
            <div class="as-seen-logo-container">
              <div class="as-seen-logo d-flex justify-content-center align-items-center px-3">
                <img src="<?php echo $logo['sizes']['medium']; ?>"/>
              </div>
            </div>
          </div>
        <?php
        } ?>
      </div>
    <?php
    } ?>
  </section>
  <?php
  return ob_get_clean();
} ?>
