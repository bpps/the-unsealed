<?php
function get_social_platform_module() {
  ob_start();
  // global $wpdb;
	// $SQL = "SELECT * FROM wp_bp_activity activity, wp_bp_activity_meta meta WHERE activity.type='activity_update' AND meta.activity_id = activity.id AND meta.meta_key = 'view_type' AND meta.meta_value = 'community';";
	// $rowCount = $wpdb->get_var($SQL);
	// if ($rowCount > 0) {
	// 	$activity = $wpdb->get_results($SQL);
	// 	foreach($activity as $row){
	// 		$activity_id = $row->id;
	// 	}
	// }
	// $recent_activity = bp_activity_get($args);
	// print_r($recent_activity); ?>
	<div id="social-platform-callout" class="activity no-ajax container wide-container py-3 py-md-5">
    <?php
    $id = get_option('page_on_front');
    $header = get_field('social_callout_header', $id ) ? get_field('social_callout_header', $id ) : 'The Unsealed Community'; ?>
    <div class="section-header">
      <h2 class="text-center mb-4"><?php echo $header; ?></h2>
    </div>
    <div class="row align-items-center">
      <div class="col-md-6 col-xl-7 mb-4 mb-md-0">
        <div class="meet-lauren-images row justify-content-center" data-sal="" data-sal-delay="800">
          <?php
          if ( $images = get_field('social_callout_images', $id) ) {
            foreach( $images as $image ) { ?>
              <div class="meet-lauren-image">
                <img class="img-fluid" alt="<?php echo $image['alt']; ?>" src="<?php echo $image['sizes']['small-medium']; ?>"/>
              </div>
            <?php
            }
          } else { // Pull images randomly from users
            $users = get_users([ 'role' => 'subscriber' ]);
            $user_count = 0;
            shuffle($users);
            foreach( $users as $user ) {
              if ( $user_count == 3 ) {
                break;
              }
              $avatar = validate_gravatar($user->data->user_email);
              if ( $avatar == 1 ) {
                $avatar_url = get_avatar_url($user->data->user_email, [ 'size' => '200' ]); ?>
                <div class="meet-lauren-image">
                  <img class="img-fluid" src="<?php echo $avatar_url; ?>"/>
                </div>
                <?php
                $user_count++;
              }
            }
          } ?>
        </div>
        <?php
        if ( $description_header = get_field('social_description_header', $id) ) { ?>
          <div class="social-platform-description">
            <h3 class="display-2 text-center script-font mb-4 mx-auto" style="line-height:.6; max-width: 80%"><?php echo $description_header; ?></h3>
          </div>
        <?php
        }
        if ( $description = get_field('social_description', $id) ) { ?>
            <p class="text-center"><?php echo $description; ?></p>
        <?php
        } ?>
        <div class="text-center">
          <?php
          $link_title = get_field('social_platform_link_title') ? get_field('social_platform_link_title') : 'Join the conversation';
          if ( pmpro_hasMembershipLevel() ) {
            $link_title = get_field('logged_in_link_title') ? get_field('logged_in_link_title') : 'My Profile';
            echo unsealed_btn($link_title, bp_loggedin_user_domain(), 'large');
          } else {
            echo unsealed_btn($link_title, home_url('sign-up'), 'large');
          }
          ?>
        </div>
      </div>
      <div class="col-md-6 col-xl-5">
        <div class="letter-list overflow-hidden row">
          <?php
          $args = [
            'action' => 'activity_update',
            'secondary_item_id' => 0,
            // 'primary_id' => $group_id,
            // 'scope' => 'groups', // Shows posts from the logged in users groups
            'per_page' => 4,
            'display_comments' => false,
            'filter' => ['object' => 'groups']
          ];
          if ( bp_has_activities( $args ) ) :

            while ( bp_activities() ) : bp_the_activity();
              if ( $group_id = bp_get_activity_item_id() ) :
                $link = bp_activity_get_permalink(bp_get_activity_id());
                $title = bp_activity_get_meta( bp_get_activity_id(), 'title' );
                $thumbnail = get_template_directory_uri() . '/img/placeholder.png';
                if ( $image_id = bp_activity_get_meta(bp_get_activity_id(), 'featured_image') ) {
                  $thumbnail = wp_get_attachment_image_src($image_id, 'medium')[0];
                } ?>
                <div class="letter-block col-12 col-sm-6 col-md-12" data-activity-id="<?php echo bp_get_activity_id(); ?>">
                  <div class="row narrow-gutter pt-3 pb-3 underlined underlined-light">
                    <div class="letter-image-wrapper col-12 col-md-4">
                      <div class="letter-image-container image-ratio bg-3-2">
                        <a href="<?php echo $link; ?>" class="letter-image bg-cover" style="background-image:url(<?php echo $thumbnail; ?>)">
                        </a>
                      </div>
                    </div>
                    <div class="letter-column col-12 col-md-8">
                      <div>
                        <?php
                        if ( $group = groups_get_group( [ 'group_id' => $group_id ] ) ) {
                          $group_link = bp_get_group_permalink( $group );
                          $group_title = $group->name; ?>
                          <span>
                            <a class="teal" href="<?php echo $group_link; ?>">
                              <?php echo $group_title; ?>
                            </a>
                          </span>
                        <?php
                        } ?>
                        <h5 class="pt-2 pt-md-0"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h5>
                        <div>
                          <?php
                          global $wpdb;
                          $activity_id = bp_get_activity_id();
                          $activity_content_query = "SELECT content FROM wp_bp_activity WHERE id = $activity_id";
                          $activity_content = $wpdb->get_results( $activity_content_query );
                          if ( $activity_content ) {
                            $activity_content = $activity_content[0]->content;
                            $excerpt_array = explode(' ', $activity_content);
                            $max_words = 12;
                            if ( count( $excerpt_array) > $max_words ) {
                              $activity_content = implode( ' ', array_slice( $excerpt_array, 0, $max_words ) ) . '...';
                            }

                            echo $activity_content;
                          }
                          // $rowCount = $wpdb->get_var($SQL);
                          // if ($rowCount > 0) {
                          // 	$activity = $wpdb->get_results($SQL);
                          // 	foreach($activity as $row){
                          // 		$activity_id = $row->id;
                          // 	}
                          // }
                          // echo bp_get_activity_content(); ?>
                        </div>
                        <?php
                        $user_id = bp_get_activity_user_id(); ?>
                        <div class="activity-avatar mt-2">
                          <a href="<?php bp_activity_user_link($user_id); ?>">
                            <?php bp_activity_avatar($user_id); ?>
                            <span class="ml-2"><?php echo bp_core_get_username($user_id); ?></span>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php
              endif;
            endwhile;
          endif; ?>
        </div>
        <div class="d-flex justify-content-end">
          <a class="btn-sm teal mt-2 d-block" href="<?php echo home_url('groups'); ?>">All Groups</a>
        </div>
      </div>
  	</div>
  </div>
  <?php
  return ob_get_clean();
} ?>
