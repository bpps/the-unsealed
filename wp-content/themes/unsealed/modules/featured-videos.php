<?php
function get_videos_module($id = false) {
  ob_start();
  $id = $id ? $id : get_option('page_on_front');
  $selected_videos = get_field('featured_videos', $id );
  $args = [
    'post_type' => 'video',
    'post_status' => 'publish',
    'posts_per_page' => $selected_videos ? count($selected_videos) : 3,
  ];
  if ( $selected_videos ) {
    $args['post__in'] = $selected_videos;
  }
  $videos_query = new WP_Query( $args );
  if ( $videos_query->have_posts() ) : ?>
    <section class="py-3 py-md-5 bg-black featured-videos">
      <div class="container">
        <?php
        if ( $header = get_field('featured_videos_header', $id ) ) { ?>
          <div class="section-header">
            <h2 class="text-center mb-4"><?php echo $header; ?></h2>
          </div>
        <?php
        } ?>
        <div class="row">
          <?php
          while ( $videos_query->have_posts() ) : $videos_query->the_post();
            echo get_video_post_lockup(get_the_ID());
          endwhile;
          wp_reset_postdata(); ?>
        </div>
        <div class="d-flex justify-content-end mt-3">
          <a class="btn-sm teal mt-2 d-block" href="<?php echo home_url('videos'); ?>">
            <span class="mr-5 teal">Watch more videos</span>
          </a>
        </div>
      </div>
    </section>
    <div class="modal fade video-modal" id="video-page-modal" tabindex="-1" role="dialog" aria-labelledby="video-page-title" aria-hidden="true">
      <div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            <div class="embed-responsive embed-responsive-16by9">
              <video controls class="embed-responsive-item video">
                <source type="video/mp4" src="">
              </video>
              <iframe class="video-iframe" src="" frameborder="0" allowfullscreen allow="autoplay"></iframe>
            </div>
            <div class="popup-login bg-cover">
              <div class="popup-login-content video-overlay d-flex overlay justify-content-center align-items-center">
                <div class="video-overlay-content text-white text-center px-2 px-md-5">
                  <h3 class="h1 mb-3">Premium Content</h3>
                  <span>You must be a subscriber to watch this video.</span>
                  <?php echo login_or_subscribe(home_url('videos'), false); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php
  endif;
  return ob_get_clean();
} ?>
