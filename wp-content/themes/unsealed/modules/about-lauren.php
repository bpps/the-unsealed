<?php
function get_about_lauren($id = false) {
  ob_start();
  $id = $id ? $id : get_option('page_on_front'); ?>
  <section class="container wide-container py-3 py-md-5" id="meet-lauren">
    <?php
    if ( $header = get_field('meet_lauren_header', $id ) ) { ?>
      <div class="section-header">
        <h2 class="text-center mb-4"><?php echo $header; ?></h2>
      </div>
    <?php
    } ?>
    <div class="row align-items-center">
      <div class="col-md-6 col-xl-7 mb-4 mb-md-0">
        <?php
        if ( $images = get_field('meet_lauren_images', $id) ) { ?>
          <div class="meet-lauren-images row justify-content-center" data-sal="" data-sal-delay="800">
            <?php
            foreach( $images as $image ) { ?>
              <div class="meet-lauren-image">
                <img class="img-fluid" alt="<?php echo $image['alt']; ?>" src="<?php echo $image['sizes']['small-medium']; ?>"/>
              </div>
            <?php
            } ?>
          </div>
        <?php
        }
        if ( $quote = get_field('meet_lauren_quote', $id) ) { ?>
          <div class="meet-lauren-quote">
            <p class="h1 text-center script-font"><?php echo $quote; ?></p>
          </div>
        <?php
        }
        if ( $meet_lauren_link = get_field('meet_lauren_link') ) { ?>
          <div class="text-center">
            <?php echo unsealed_btn($meet_lauren_link['title'], $meet_lauren_link['url'], ''); ?>

          </div>
        <?php
        } ?>
      </div>
      <?php
      if ( $letters = get_field('meet_lauren_letters', $id ) ) { ?>
        <div class="col-md-6 col-xl-5">
          <div class="letter-list overflow-hidden row">
            <?php
            foreach( $letters as $i => $letter ) {
              echo get_letter_lockup($letter);
            } ?>
          </div>
          <?php
          if ( $letters_link = get_field('meet_lauren_letters_link') ) { ?>
            <div class="d-flex justify-content-end">
              <a class="btn-sm ocean" href="<?php echo $letters_link['url']; ?>">
                <?php echo $letters_link['title']; ?>
              </a>
            </div>
          <?php
          } ?>
        </div>
      <?php
      } ?>
    </div>
  </section>
  <?php
  return ob_get_clean();
} ?>
