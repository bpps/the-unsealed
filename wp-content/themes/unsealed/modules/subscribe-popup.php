<?php
function subscribe_popup_form($id = false) {
  ob_start();
  $timeout = get_field('intro_video_timeout', 'option') ? get_field('intro_video_timeout', 'option') * 1000 : 10000; ?>
  <div class="modal fade subscribe-modal" id="subscribe-popup" data-timeout="<?php echo $timeout; ?>" tabindex="-1" role="dialog" aria-labelledby="subscribe-popup-header" aria-hidden="true">
    <div role="document" class="modal-dialog fortune-popup-wrapper d-flex align-items-center w-100 h-100 justify-content-center">
  		<div class="modal-body fortune-popup-container container d-flex align-items-center justify-content-center flex-column">
        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
  			<div class="brand">
  				<?php include get_template_directory() . '/img/unsealed_logo.svg'; ?>
  			</div>
        <div class="subscribe-popup-body">
    			<div class="sub-text text-center" id="subscribe-popup-header">
    				<p style="line-height: 1.2">Do you want to improve your writing?</p>
    				<p style="font-size: 1.4rem; line-height: 1.3;">Here is your chance to learn from an <span class="color-lightblue">award-winning</span> journalist.</p>
                    <p style="font-size: 1.1rem; line-height: 1.3; margin-top: 1rem;">Add your email for updates on our free writing workshops and more.</p>
    			</div>
    			<div class="fortune-email-input d-flex align-items-center">
            <form id="subscribe-popup-form" action="">
              <div class="justify-content-center d-flex">
                <div class="fortune-email-input-container">
                  <input type="email" placeholder="Email" required>
                </div>
                <div>
                  <button type="submit" class="fortune-btn">
                    <?php echo unsealed_btn('Subscribe', '', 'small'); ?>
                  </button>
                </div>
              </div>
            </form>
    			</div>
        </div>
        <div class="email-response text-center font-white"></div>
  			<div class="follow-lauren d-flex flex-column align-items-center justify-content-center mt-4">
  				<p>Follow Lauren</p>
  				<div class="social-links d-flex">
  					<a href="<?php the_field('facebook_link', 'options'); ?>" target="_blank">
  						<?php include get_template_directory() . '/img/facebook.svg'; ?>
  					</a>
  					<a href="<?php the_field('twitter_link', 'options'); ?>" target="_blank">
  						<?php include get_template_directory() . '/img/twitter.svg'; ?>
  					</a>
  					<a href="<?php the_field('instagram_link', 'options'); ?>" target="_blank">
  						<?php include get_template_directory() . '/img/instagram.svg'; ?>
  					</a>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>
  <?php
  return ob_get_clean();
} ?>
