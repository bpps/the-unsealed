<?php
function footer_signup($id = false) {
	ob_start();
		?>

		<section id="footer-signup" class='py-5 py-md-5 px-3 bg-black d-flex justify-content-center align-items-center min-50vh bg-cover overflow-hidden' style="background-image: url(<?php echo get_field('footer_image', 'options')['sizes']['large']; ?>)">
			<span id="footer-signature" class="position-absolute teal hover-teal script-font">The Unsealed</span>
			<div class="container py-3 py-md-5">
				<div class="h5">
					<?php the_field('footer_title', 'options'); ?>
				</div>
				<?php echo unsealed_btn('Sign Up', home_url('/accounts/sign-up')); ?>
				<?php the_field('footer_caption', 'options') ?>
			</div>
		</section>
	<?php
  return ob_get_clean();
} ?>
