<?php
//Template Name: Videos
get_header(null, ["white_nav" => true]);
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <article>
      <div class="pt-3 pt-md-5">
        <header class="entry-header text-center">
          <?php
          the_title( '<h1 id="video-page-title" class="entry-title title-xxl script-font">', '</h1>' ); ?>
        </header><!-- .entry-header -->

        <div class="entry-content mt-5">
          <?php
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
          $args = [
      			'post_type' => 'video',
      			'post_status' => 'publish',
      			'posts_per_page' => get_option( 'posts_per_page' ),
            'paged' => $paged
      		];
      		// NOTE: Could combine these two instances of letters_query to one function
      	  $videos_query = new WP_Query( $args );
      		if ( $videos_query->have_posts() ) : ?>
            <div class="bg-black pt-5">
              <div class="container-med">
                <div class="row featured-videos">
                  <?php
            			while ( $videos_query->have_posts() ) : $videos_query->the_post();
                    echo get_video_post_lockup(get_the_id(), $videos_query->current_index);
            			endwhile;
            			wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="position-relative pagination container d-flex justify-content-center pt-5 pb-5">
                <?php
                $big = 999999999;
                echo paginate_links( array(
                  'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                  'format' => '?paged=%#%',
                  'current' => max( 1, get_query_var('paged') ),
                  'total' => $videos_query->max_num_pages,
                  'prev_text' => '<',
                  'next_text' => '>'
                ) ); ?>
              </div>
            </div>
            <div class="modal fade video-modal" id="video-page-modal" tabindex="-1" role="dialog" aria-labelledby="video-page-title" aria-hidden="true">
              <div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
        						<button class="play-button" aria-label="play-button"></button>
                    <div class="embed-responsive embed-responsive-16by9">
                      <video class="embed-responsive-item video">
                        <source type="video/mp4">
                      </video>
                      <iframe class="video-iframe" src="" frameborder="0" allowfullscreen allow="autoplay"></iframe>
                      <div class="popup-login bg-cover">
                        <div class="popup-login-content video-overlay d-flex overlay justify-content-center align-items-center">
                          <div class="video-overlay-content text-white text-center px-2 px-md-5">
                            <h3 class="h1 mb-3">Premium Content</h3>
                            <span>You must be a subscriber to watch this video.</span>
                            <?php echo login_or_subscribe(home_url('videos'), false); ?>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="teal-share-links video-share-links pt-2"></div>
                  </div>
                </div>
              </div>
            </div>
          <?php
      		endif; ?>
        </div><!-- .entry-content -->
      </div>
    </article><!-- #post-<?php the_ID(); ?> -->
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
