<?php
/**
 * The template for displaying all single posts
 */

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php
    while ( have_posts() ) :
    the_post();
    $advisors = get_the_terms( get_the_id(), 'advisor' );
    $show_prompt = $should_show_prompt = get_field('show_prompt');
    $today = date( 'Ymd' );
    $prompt_end_date = get_field('advice_end_date');
    if ( $prompt_end_date < $today ) {
      $show_prompt = false;
    }
    if ( $infinite_prompt = get_field('never_ending_advice') && $should_show_prompt ) {
      $show_prompt = true;
    }
    $advisor = $advisors ? $advisors[0] : false;
    $advisor_alt = $advisor ? get_field('alt_name', $advisor) : false; ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
          <div class="d-flex flex-column align-items-center">
            <?php if(get_the_post_thumbnail()){ ?>
              <div class="header-bg-img">
                <?php echo get_the_post_thumbnail(); ?>
              </div>
              <div class="header-img">
                <?php echo get_the_post_thumbnail(); ?>
              </div>
            <?php } ?>
            <div class="container">
              <?php
              if ( is_singular() ) :
              the_title( '<h1 class="entry-title">', '</h1>' );
              else :
              the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
              endif;

              if ( 'post' === get_post_type() ) :
              ?>
              <?php endif; ?>

              <div class="single-to-from mb-3 mb-md-5">
                <?php
                if ( get_field('to_destination') ) { ?>
                  <p>To: <?php the_field('to_destination') ?></p>
                <?php
                }
                if ( $from_override = get_field('from_override') ) { ?>
                  <p>From: <?php echo $from_override; ?></p>
                <?php
                } else {
                  if ( $advisor ) { ?>
                    <p>
                      From: <?php echo $advisor_alt ? $advisor_alt : $advisor->name;
                      if ( get_field('include_appended_from') && get_field('from_appended_text', 'option') ) { ?>
                        <span> <?php the_field('from_appended_text', 'option'); ?></span>
                      <?php
                      } ?>
                    </p>
                  <?php
                  }
                } ?>
              </div>
              <?php
              if ( $sponsor = get_field('sponsor') ) {
                $sponsor_link = get_field('sponsor_link'); ?>
                <div class="d-flex justify-content-center pb-3">
                  <div class="col-auto">
                    <p class="mb-0 d-inline-block">
                      <?php
                      if ( $sponsor_link ) { ?>
                        <a class="teal" href="<?php echo $sponsor_link; ?>" target="_blank">
                      <?php
                      }
                      echo $sponsor;
                      if ( $sponsor_link ) { ?>
                        </a>
                      <?php
                      } ?>
                    </p>
                  </div>
                </div>
              <?php
              } ?>
              <div class="divider-h mt-3 mb-3 mb-md-5 d-flex justify-content-center">
                <img src="<?php echo get_template_directory_uri(); ?>/img/brush-btn-1.png"/>
              </div>
            </div>
          </div>
        </header><!-- .entry-header -->
        <?php echo get_announcement(); ?>

        <!-- <div class="container py-5 container-narrow"> -->

        <div id="main-content" class="d-flex py-5 flex-wrap flex-lg-row align-items-start overflow-hidden">
          <?php
          echo new_to_unsealed(); ?>
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-xl-6 offset-xl-0">
            <div class="entry-content mb-5">
              <?php
              the_content( sprintf(
              wp_kses(
                  /* translators: %s: Name of current post. Only visible to screen readers */
                  __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'wowgulpwpstarter' ),
                  array(
                      'span' => array(
                          'class' => array(),
                      ),
                  )
              ),
              get_the_title()
              ) );

              wp_link_pages( array(
              'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wowgulpwpstarter' ),
              'after'  => '</div>',
              ) );
              ?>
            </div><!-- .entry-content -->

            <?php
            if ( $advisor ) { ?>
              <div class="author-single-container d-flex flex-column align-items-center mb-4 pt-3">
                <?php
                if ( $advisor_image = get_field('featured_image', $advisor) ) { ?>
                  <div class="author-single-image">
                    <div class="image-ratio rounded-circle mx-auto">
                      <div class="bg-cover d-flex justify-content-center align-items-center"
                          style="background-image:url(<?php echo $advisor_image['sizes']['small']; ?>)">
                      </div>
                    </div>
                  </div>
                <?php
                } ?>
                <h5 class="mt-1"><?php echo $advisor->name; ?></h5>

              </div>
            <?php
            } ?>
            <section class="comments-container py-2">
              <div class="d-flex flex-column-reverse flex-md-row justify-content-md-between">
                <?php
                if ( comments_open() || get_comments_number() ){ ?>
                  <a <?php echo pmpro_hasMembershipLevel() ? 'class="show-comments"' : 'href="'.pmpro_url('checkout', '?redirect_to=' . get_the_permalink(), 'https').'"'; ?>><span><?php echo get_comments_number() == 1  ? get_comments_number() . " </span> comment" : get_comments_number() . '</span> comments'; ?></a>
                <?php
                } ?>
                <div class='ml-4 d-flex flex-1 justify-content-end align-items-center'>
                  <?php
                  if ( $social_promo = get_field('social_promo') ) { ?>
                    <span><?php echo $social_promo; ?></span>
                  <?php
                  } else { ?>
                    <span class="script-font h2 mb-0">Share this letter</span>
                  <?php
                  } ?>
                  <div class="d-flex ml-2">
                    <?php echo do_shortcode('[addtoany]'); ?>
                  </div>
                </div>
              </div>
              <?php
              if ( comments_open() || get_comments_number() ) { ?>
                <div class="all-comments">
                  <?php comments_template(); ?>
                </div>
              <?php
              } ?>
            </section>

            <div class="tell-us-story d-flex align-items-center flex-column py-5 mb-4 underlined underlined-top">
              <h2 class='mb-4'>Tell us your story</h2>
              <div class="row justify-content-center">
                <div class="col-md-8">
                  <p class='mb-4'>Write a letter of your own and respond to letters from the Unsealed community.</p>
                </div>
              </div>
              <?php
              if ( !pmpro_hasMembershipLevel() ) {
                echo unsealed_btn('Write A Letter Now', pmpro_url('checkout', '?redirect_to=' . get_the_permalink(), 'https'), 'large'); ?>
                <div class="mt-1">
                  <?php echo login_or_subscribe(get_the_permalink()); ?>
                </div>
              <?php
              } else {
                echo unsealed_btn('Write A Letter Now', bp_loggedin_user_domain() , 'large');
              } ?>
              <section class="mt-5 pt-5 border-top">
                <h3 class="script-font display-4">Find A Pen Pal</h3>
                <div class="row mt-5">
                  <?php echo get_recent_pen_pals(); ?>
                </div>
                <div class="mt-2 pt-2 text-center">
          	      <?php echo unsealed_btn('Search Pen Pals', home_url('members'), 'small'); ?>
          	    </div>
              </section>
            </div>
          </div>
        </div>

        <?php
        echo more_stories(); ?>

      </article><!-- #post-<?php the_ID(); ?> -->


    <?php endwhile; ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
