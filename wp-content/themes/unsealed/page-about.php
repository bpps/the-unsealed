<?php  //Template Name: About ?>
<?php get_header(null, ["white_nav" => true]); ?>

<section class="py-3 py-md-5" id="about-hero">
	<div class="container-narrow about-hero-container">
		<h1>About</h1>
		<?php $aboutImages = get_field('header_images');?>
		<?php if ( $aboutImages ) { ?>
			<div id="about-images" class='mb-3'>
				<?php foreach( $aboutImages as $image ){
					?>
					<div class="about-image">
						<?php echo wp_get_attachment_image($image['ID'], 'small-medium'); ?>
					</div>
					<?php
				} ?>
			</div>
		<?php } ?>

		<div id="our-mission" class="mt-md-5 d-flex justify-content-center flex-column align-items-center">
			<div class="spotlight-image-container">
				<div class="image-ratio rounded-circle mx-auto mb-4">
					<?php
					$video_upload = get_field('video_upload');
				  $video_src = $video_upload ? $video_upload['url'] : get_field('video_url');
				  $video_type = $video_upload ? 'upload' : 'video'; ?>
					<button
						class="bg-cover d-flex justify-content-center align-items-center p-0"
						style="background-image:url(<?php echo get_field('video_cover_image')['sizes']['small']; ?>)"
						data-toggle="modal"
						data-target="#our-mission-modal"
						href="#new-to-unsealed"
						data-video-src="<?php echo $video_src; ?>"
						data-toggle="modal"
						data-video-type="<?php echo $video_type; ?>">
						<span class="play-icon rounded-circle"></span>
					</button>
				</div>
			</div>
			<h5 class="script-font text-center text-black">
				Our Mission
			</h5>
		</div>
	</div>
</section>

<div class="modal fade video-modal" id="our-mission-modal" tabindex="-1" role="dialog" aria-labelledby="ourMissionModal" aria-hidden="true">
	<div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				<div class="embed-responsive embed-responsive-16by9">
					<video class="embed-responsive-item video">
						<source type="video/mp4">
					</video>
					<iframe class="video-iframe" src="" frameborder="0" allowfullscreen allow="autoplay"></iframe>
				</div>
			</div>
		</div>
	</div>
</div>


<section id="about-content" class="bg-black">
	<div class="container py-4 py-md-5">
		<div class="row">
			<div id="main-content" class="content-container overflow-hidden col-md-10 offset-md-2">
				<div class="row">
					<div class="col-md-10">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php echo get_as_seen_on(); ?>

<?php get_footer(); ?>
