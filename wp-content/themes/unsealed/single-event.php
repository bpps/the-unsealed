<?php
/**
 * The template for displaying all single posts
 */

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php
    global $EM_Event;
    while ( have_posts() ) :
    the_post();
    $advisors = get_the_terms( get_the_id(), 'advisor' );
    $show_prompt = $should_show_prompt = get_field('show_prompt');
    $today = date( 'd/m/Y' );
    $prompt_end_date = get_field('advice_end_date');
    if ( $prompt_end_date < $today ) {
      $show_prompt = false;
    }
    if ( $infinite_prompt = get_field('never_ending_advice') && $should_show_prompt ) {
      $show_prompt = true;
    }
    $advisor = $advisors ? $advisors[0] : false;
    $advisor_alt = $advisor ? get_field('alt_name', $advisor) : false;
    $intro_video = get_field('conversations_video', 'option');
    $intro_video = $intro_video['url'];
    $intro_video_placeholder = get_field('conversations_video_placeholder_image', 'option'); ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
          <div class="container">
            <div class="d-flex align-items-center row">
              <div class="col-md-5 pt-4 pb-5">
                <?php
                if ( $EM_Event->start_date ) { ?>
                  <p class="my-0 teal"><?php echo date('l, F jS', strtotime($EM_Event->start_date)); ?></p>
                <?php
                }
                if ( $EM_Event->start_time ) { ?>
                  <p class="my-0 teal">
                    <?php
                    echo date('g:ia', strtotime($EM_Event->start_time));
                    if ( $EM_Event->end_time ) {
                      echo ' - ' . date('g:ia', strtotime($EM_Event->end_time));
                    } ?>
                  </p>
                <?php
                }
                the_title( '<h1 class="script-font display-3 event-title mt-3">', '</h1>' );
                if ( $advisor ) { ?>
                  <p class="h5 mt-1">An Unsealed Conversation with <?php echo $advisor_alt ? $advisor_alt : $advisor->name; ?></p>
                <?php
                } ?>
              </div>
              <div class="col-md-7 py-5 px-md-5">
                <div class="inline-video-wrapper">
                  <div class="embed-responsive embed-responsive-16by9 position-relative unsealed-video-block inline-video">
                    <video poster="<?php echo $intro_video_placeholder['sizes']['medium']; ?>">
                      <source src="<?php echo $intro_video; ?>" type="video/mp4">
                      Your browser does not support the video tag.
                    </video>
                    <button class="play-icon rounded-circle d-flex justify-content-center align-items-center p-0">
            				</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header><!-- .entry-header -->
        <?php echo get_announcement(); ?>

        <!-- <div class="container py-5 container-narrow"> -->

        <div class="d-flex py-3 py-md-5 flex-wrap flex-lg-row align-items-start">
          <?php
          echo new_to_unsealed(); ?>
          <div class="col-12 col-sm-10 offset-sm-1 col-md-9 col-lg-7 col-xl-6 offset-md-0">
            <div class="entry-content mb-5">
              <?php
              if ( $advisor ) { ?>
                <div class="author-single-container d-flex flex-column align-items-center mb-2">
                  <?php
                  if ( $advisor_image = get_field('featured_image', $advisor) ) { ?>
                    <div class="author-single-image">
                      <div class="image-ratio rounded-circle mx-auto">
                        <div class="bg-cover d-flex justify-content-center align-items-center"
                            style="background-image:url(<?php echo $advisor_image['sizes']['small']; ?>)">
                        </div>
                      </div>
                    </div>
                  <?php
                  } ?>
                  <h5 class="mt-1"><?php echo $advisor->name; ?></h5>

                </div>
              <?php
              } ?>
              <?php
              echo wpautop($EM_Event->post_content); ?>
              <section class="mt-5">
                <h3 class="mb-4">Join the conversation</h3>
                <div class="d-flex justify-content-between">
                  <div class="event-details">
                    <?php
                    if ( $EM_Event->start_date ) { ?>
                      <p class="my-0"><?php echo date('l, F jS', strtotime($EM_Event->start_date)); ?></p>
                    <?php
                    }
                    if ( $EM_Event->start_time ) { ?>
                      <p class="my-0">
                        <?php
                        echo date('g:ia', strtotime($EM_Event->start_time));
                        if ( $EM_Event->end_time ) {
                          echo ' - ' . date('g:ia', strtotime($EM_Event->end_time));
                        } ?>
                      </p>
                    <?php
                    } ?>
                  </div>
                  <div>
                    <?php
                    if ( $EM_Event->event_location_data ) {
                      if ( pmpro_hasMembershipLevel() ) {
                        echo unsealed_btn('Join on Zoom', $EM_Event->event_location_data['url'], 'small');
                      } else { ?>
                        <div>
                          <?php
                          echo unsealed_btn('Subscribe to Join', pmpro_url("checkout", "", "https"), 'small'); ?>
                          <div class="mt-1">
                            <?php echo login_or_subscribe(); ?>
                          </div>
                        </div>
                      <?php
                      }
                    }
                    if ( pmpro_hasMembershipLevel() ) {
                      $start_date = $EM_Event->start_date ? date('Ymd', strtotime($EM_Event->start_date)) : '';
                      $start_time = $EM_Event->start_time ? date('His', strtotime($EM_Event->start_time)) : '';
                      $end_date = $EM_Event->end_date ? date('Ymd', strtotime($EM_Event->end_date)) : '';
                      $end_time = $EM_Event->end_time ? date('His', strtotime($EM_Event->end_time)) : '';
                      $date_str = $start_date . 'T' . $start_time;
                      if ( $end_date ) {
                        $date_str .= '/' . $start_date . 'T' . $start_time;
                      } ?>
                      <!-- <div class="">
                        <a href="<?php echo 'http://www.google.com/calendar/event?action=TEMPLATE&text=' . urlencode(get_the_title()) . '&dates=' . $date_str . '&details=' . urlencode(get_the_excerpt()) . '&location=&trp=false&sprop=' . urlencode($EM_Event->event_location_data['url']) . '&sprop=name:Join+on+Zoom&ctz=UTC'; ?>">
                          Add to GCal
                        </a>
                      </div> -->
                    <?php
                    } ?>
                  </div>
                </div>
              </section>
            </div><!-- .entry-content -->

            <section class="py-4 mb-4">
              <div class="d-flex justify-content-end">
                <div class='ml-4 d-flex flex-1 justify-content-end align-items-center'>
                  <?php
                  if ( $social_promo = get_field('social_promo') ) { ?>
                    <span><?php echo $social_promo; ?></span>
                  <?php
                  } else { ?>
                    <span class="script-font h2 mb-0">Share this event</span>
                  <?php
                  } ?>
                  <div class="d-flex ml-2">
                    <?php echo do_shortcode('[addtoany]'); ?>
                  </div>
                </div>
              </div>
            </section>
            <?php
            if ( $advisor && $show_prompt ) {
              $pronoun = get_field('pronoun', $advisor); ?>
              <div class="single-advice mb-4">
                <h5>Ask for advice</h5>
                <div class="d-flex flex-column flex-md-row align-items-center">
                  <p class='flex-1 mr-4 mb-md-0'>Do you want advice from <?php echo $advisor_alt ? $advisor_alt : $advisor->name; ?>? Ask<?php if ( $pronoun ) { echo ' ' . strtolower($pronoun); } ?> a question here and join the conversation.</p>
                  <?php
                    if ( pmpro_hasMembershipLevel() ) {
                      echo unsealed_btn('Ask for advice', '#ask-for-advice', 'small', 'data-toggle="modal" data-target="#ask-for-advice"');
                    } else { ?>
                      <div>
                        <?php
                        echo unsealed_btn('Get Advice', pmpro_url("checkout", "", "https"), 'small'); ?>
                        <div class="mt-1">
                          <?php echo login_or_subscribe(); ?>
                        </div>
                      </div>
                    <?php
                    } ?>
                </div>
              </div>
            <?php
            } ?>

          </div>
          <div class="col-12 col-lg-3"></div>
        </div>

        <div class="container">
          <div class="tell-us-story d-flex align-items-center flex-column py-5 mb-4">
            <h2 class='mb-4'>Tell us your story</h2>
            <?php
            $share_prompt = get_field('share_prompt');
            $prompt = $share_prompt ? $share_prompt : 'See other stories from <a class="teal font-weight-bold" href="'.home_url('community-voices').'">The Unsealed community</a> and share your own'; ?>
            <div class="row justify-content-center">
              <div class="col-md-8">
                <p class='mb-4'><?php echo $prompt; ?></p>
              </div>
            </div>
            <?php
            if ( !pmpro_hasMembershipLevel() ) {
              echo unsealed_btn('Share Your Story', pmpro_url('checkout', '?redirect_to=' . get_the_permalink(), 'https'), 'small'); ?>
              <div class="mt-1">
                <?php echo login_or_subscribe(); ?>
              </div>
            <?php
            } else {
              if ( $share_prompt ) {
                echo unsealed_btn('Share Your Story', '#share-your-story' , 'small', 'data-toggle="modal" data-target="#share-your-story"');
              } else {
                echo unsealed_btn('Share Your Story', home_url('/community-voices') , 'small');
              }
            } ?>
          </div>
        </div>

        <?php
        echo more_stories();

        if ( $advisor && $show_prompt ) { ?>
          <div id="ask-for-advice-label" class="d-none">Ask for advice</div>
          <div class="modal fade form-modal" id="ask-for-advice" tabindex="-1" role="dialog" aria-labelledby="ask-for-advice-label" aria-hidden="true">
          	<div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
          		<div class="modal-content">
          			<div class="modal-body">
          				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        						<span aria-hidden="true">&times;</span>
        					</button>
                  <p class="text-center mb-3 h5">Ask <?php echo $advisor_alt ? $advisor_alt : $advisor->name; ?> a question.</p>
                  <?php echo do_shortcode('[ninja_form id=7]'); ?>
          			</div>
          		</div>
          	</div>
          </div>
        <?php
        }
        if ( $share_prompt ) { ?>
          <div id="share-your-story-label" class="d-none">Share your story</div>
          <div class="modal fade form-modal" id="share-your-story" tabindex="-1" role="dialog" aria-labelledby="share-your-story-label" aria-hidden="true">
          	<div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
          		<div class="modal-content">
          			<div class="modal-body">
                  <h2 class="script-font display-3 text-center">Tell your story</h2>
                  <p class="text-center mb-3 h5"><?php echo $prompt; ?></p>
          				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        						<span aria-hidden="true">&times;</span>
        					</button>
                  <?php echo do_shortcode('[ninja_form id=2]'); ?>
          			</div>
          		</div>
          	</div>
          </div>
        <?php
        } ?>

      </article><!-- #post-<?php the_ID(); ?> -->


    <?php endwhile; ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
