<?php
/**
 * BuddyPress - Groups Cover Image Header.
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 * @version 3.0.0
 */

/**
 * Fires before the display of a group's header.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_group_header' ); ?>
<div id="item-actions">

	<?php if ( bp_group_is_visible() ) : ?>

		<p><?php _e( 'Group Leaders', 'buddypress' ); ?></p>

		<?php bp_group_list_admins();

		/**
		 * Fires after the display of the group's administrators.
		 *
		 * @since 1.1.0
		 */
		do_action( 'bp_after_group_menu_admins' );

		if ( bp_group_has_moderators() ) :

			/**
			 * Fires before the display of the group's moderators, if there are any.
			 *
			 * @since 1.1.0
			 */
			do_action( 'bp_before_group_menu_mods' ); ?>

			<h2><?php _e( 'Group Mods' , 'buddypress' ); ?></h2>

			<?php bp_group_list_mods();

			/**
			 * Fires after the display of the group's moderators, if there are any.
			 *
			 * @since 1.1.0
			 */
			do_action( 'bp_after_group_menu_mods' );

		endif;

	endif; ?>

</div><!-- #item-actions -->
<div id="cover-image-container">
	<a id="header-cover-image" href="<?php echo esc_url( bp_get_group_permalink() ); ?>"></a>
	<div id="item-header-cover-image" class="d-flex justify-content-center align-items-center h-100">
		<div id="item-header-content" class="m-0">
			<span class="highlight">
				<?php echo bp_get_group_name( groups_get_group( bp_get_group_id() )); ?>
			</span>
			<span class="activity" data-livestamp="<?php bp_core_iso8601_date( bp_get_group_last_active( 0, array( 'relative' => false ) ) ); ?>">
				<?php
				/* translators: %s: last activity timestamp (e.g. "Active 1 hour ago") */
				printf( __( 'Active %s', 'buddypress' ), bp_get_group_last_active() );
				?>
			</span>


			<?php

			/**
			 * Fires before the display of the group's header meta.
			 *
			 * @since 1.2.0
			 */
			do_action( 'bp_before_group_header_meta' ); ?>
			<div id="item-meta" class="mt-2">
				<?php bp_group_description(); ?>
				<?php bp_group_type_list(); ?>
				<div id="item-buttons">
					<?php
					/**
					 * Fires in the group header actions section.
					 *
					 * @since 1.2.6
					 */
					do_action( 'bp_group_header_actions' ); ?>

				</div><!-- #item-buttons -->

				<?php

				/**
				 * Fires after the group header actions section.
				 *
				 * @since 1.2.0
				 */
				do_action( 'bp_group_header_meta' ); ?>
			</div>
		</div><!-- #item-header-content -->
	</div><!-- #item-header-cover-image -->
</div><!-- #cover-image-container -->

<?php

/**
 * Fires after the display of a group's header.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_group_header' ); ?>

<?php
if ( groups_is_user_admin( bp_loggedin_user_id(), bp_get_group()->id ) ) { ?>
	<div id="manage-group" class="d-flex align-items-center">
		<?php if ( ! bp_disable_group_avatar_uploads() ) : ?>
				<?php bp_group_avatar(); ?>
		<?php else: ?>
			<img src="http://localhost/unsealed/wp-content/themes/unsealed/img/manage-group-avatar.png" />
		<?php endif; ?>
		<a type="button" class="edit-button ml-4 mt-0 mr-0" href="<?php echo home_url('groups/' . bp_get_group()->slug . '/admin'); ?>">
			Manage Group
			<i class="fa fa-pencil ml-1"></i>
		</a>
	</div>
<?php
} ?>
