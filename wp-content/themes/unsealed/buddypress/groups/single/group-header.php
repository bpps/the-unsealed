<?php
/**
 * BuddyPress - Groups Header
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 * @version 3.0.0
 */

/**
 * Fires before the display of a group's header.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_group_header' );

?>

<div id="item-actions">

	<?php if ( bp_group_is_visible() ) : ?>

		<p><?php _e( 'Group Leaders', 'buddypress' ); ?></p>

		<?php bp_group_list_admins();

		/**
		 * Fires after the display of the group's administrators.
		 *
		 * @since 1.1.0
		 */
		do_action( 'bp_after_group_menu_admins' );

		if ( bp_group_has_moderators() ) :

			/**
			 * Fires before the display of the group's moderators, if there are any.
			 *
			 * @since 1.1.0
			 */
			do_action( 'bp_before_group_menu_mods' ); ?>

			<h2><?php _e( 'Group Mods' , 'buddypress' ); ?></h2>

			<?php bp_group_list_mods();

			/**
			 * Fires after the display of the group's moderators, if there are any.
			 *
			 * @since 1.1.0
			 */
			do_action( 'bp_after_group_menu_mods' );

		endif;

	endif; ?>

</div><!-- #item-actions -->


<div id="item-header-content">
	<span class="highlight">
		<?php echo bp_get_group_name( groups_get_group( bp_get_group_id() )); ?>
	</span>
	<span class="activity" data-livestamp="<?php bp_core_iso8601_date( bp_get_group_last_active( 0, array( 'relative' => false ) ) ); ?>">
		<?php
		/* translators: %s: last activity timestamp (e.g. "Active 1 hour ago") */
		printf( __( 'Active %s', 'buddypress' ), bp_get_group_last_active() );
		?>
	</span>

	<?php

	/**
	 * Fires before the display of the group's header meta.
	 *
	 * @since 1.2.0
	 */
	do_action( 'bp_before_group_header_meta' ); ?>

	<div id="item-meta" class="mt-3">

		<?php bp_group_description(); ?>

		<?php bp_group_type_list(); ?>

		<?php
		$user = wp_get_current_user();
		$group_id = bp_get_group_id();
		$group = groups_get_group($group_id);
		$isSubscriber = pmpro_hasMembershipLevel();
		if ( $isSubscriber && !groups_is_user_member( $user->ID, $group_id ) ) { ?>
			<div class="mt-3 d-block">
				<?php echo bp_get_group_join_button( $group ); ?>
			</div>
		<?php
		}
		if ( !$isSubscriber ) { ?>
			<div class="mt-3 d-block">
				<p><a class="teal teal-btn font-weight-bold" href="<?php echo pmpro_url("checkout", '?redirect_to=' . bp_get_group_permalink( $group ), "https"); ?>">Sign up</a> or <a class="teal teal-btn font-weight-bold" href="<?php echo pmpro_url("login", '?redirect_to=' . bp_get_group_permalink( $group ), "https"); ?>">Log in</a> to join this group</p>
			</div>
		<?php
		} ?>

		<?php

		/**
		 * Fires after the group header actions section.
		 *
		 * @since 1.2.0
		 */
		do_action( 'bp_group_header_meta' ); ?>

	</div>
</div><!-- #item-header-content -->

<?php

/**
 * Fires after the display of a group's header.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_group_header' );  ?>


<div id="manage-group" class="d-flex align-items-center">
	<?php if ( ! bp_disable_group_avatar_uploads() ) : ?>
			<?php bp_group_avatar(); ?>
	<?php else: ?>
		<img src="http://localhost/unsealed/wp-content/themes/unsealed/img/manage-group-avatar.png" />
	<?php endif; ?>
	<a type="button" class="button ml-4 mt-0 mr-0" href="<?php echo bp_get_group()->slug; ?>">
		Manage Group
		<i class="fa fa-pencil ml-1"></i>
	</a>
</div>
