<?php
/**
 * BuddyPress - Groups Loop
 *
 * Querystring is set via AJAX in _inc/ajax.php - bp_legacy_theme_object_filter().
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 * @version 3.0.0
 */

?>

<?php

/**
 * Fires before the display of groups from the groups loop.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_groups_loop' ); ?>

<?php if ( bp_get_current_group_directory_type() ) : ?>
	<p class="current-group-type"><?php bp_current_group_directory_type_message() ?></p>
<?php endif; ?>

<?php if ( bp_has_groups( bp_ajax_querystring( 'groups' ) ) ) : ?>
	<?php if ( bp_is_my_profile() ) { ?>
		<div class="d-flex justify-content-end mb-3">
			<a href="<?php echo home_url('groups'); ?>" class="btn-unsealed small">
					<span class="btn-unsealed-text"><?php esc_attr_e( 'Find Groups', 'buddypress' ); ?></span>
					<div class="btn-background-text" aria-hidden="true">
						<?php for($y = 0; $y < 3; $y++){
							echo '<span>';
							for($x = 0; $x < 10; $x++){
								echo 'Find Groups' . '  ';
							}
							echo '</span>';
						} ?>
					</div>
			</a>
		</div>
	<?php } ?>
	<div id="pag-top" class="pagination">

		<div class="pag-count" id="group-dir-count-top">

			<?php bp_groups_pagination_count(); ?>

		</div>

		<div class="pagination-links" id="group-dir-pag-top">

			<?php bp_groups_pagination_links(); ?>

		</div>

	</div>

	<?php

	/**
	 * Fires before the listing of the groups list.
	 *
	 * @since 1.1.0
	 */
	do_action( 'bp_before_directory_groups_list' ); ?>

	<ul id="groups-list" class="item-list" aria-live="assertive" aria-atomic="true" aria-relevant="all">

	<?php
	$isSubscriber = pmpro_hasMembershipLevel();
	$user = wp_get_current_user();

	while ( bp_groups() ) : bp_the_group(); ?>

		<li <?php bp_group_class(); ?>>
			<?php if ( ! bp_disable_group_avatar_uploads() ) : ?>
				<div class="item-avatar">
					<a href="<?php bp_group_permalink(); ?>"><?php bp_group_avatar( 'type=thumb&width=50&height=50' ); ?></a>
				</div>
			<?php endif; ?>

			<div class="item">
				<div class="item-title"><?php bp_group_link(); ?></div>
				<div class="item-meta">
					<span class="activity" data-livestamp="<?php bp_core_iso8601_date( bp_get_group_last_active( 0, array( 'relative' => false ) ) ); ?>">
						<?php
						/* translators: %s: last activity timestamp (e.g. "Active 1 hour ago") */
						printf( __( 'Active %s', 'buddypress' ), bp_get_group_last_active() );
						?>
					</span>
				</div>

				<div class="item-desc"><?php bp_group_description_excerpt(); ?></div>

				<?php

				/**
				 * Fires inside the listing of an individual group listing item.
				 *
				 * @since 1.1.0
				 */
				do_action( 'bp_directory_groups_item' ); ?>
			</div>

			<div class="action">

				<?php

				/**
				 * Fires inside the action section of an individual group listing item.
				 *
				 * @since 1.1.0
				 */
				// do_action( 'bp_directory_groups_actions' );

				$group_id = bp_get_group_id();
				$group = groups_get_group($group_id);

					if ( $isSubscriber ) {
						if ( !groups_is_user_member( $user->ID, $group_id ) ) { ?>
							<a class="group-button border btn join-group" href="<?php echo wp_nonce_url( bp_get_group_permalink( $group ) . 'join', 'groups_join_group' ); ?>">
								Join Group
							</a>
						<?php
						} else { ?>
							<a class="group-button border btn join-group" href="<?php echo wp_nonce_url( bp_get_group_permalink( $group ) . 'leave-group', 'groups_leave_group' ); ?>">
								Leave Group
							</a>
						<?php
						}
					} else { ?>
						<p><a class="teal teal-btn font-weight-bold" href="<?php echo pmpro_url("checkout", '?redirect_to=' . bp_get_group_permalink( $group ), "https"); ?>">Sign up</a> or <a class="teal teal-btn font-weight-bold" href="<?php echo pmpro_url("login", '?redirect_to=' . bp_get_group_permalink( $group ), "https"); ?>">Log in</a> to join this group</p>
					<?php
					} ?>

				<div class="meta">

					<?php bp_group_type(); ?> / <?php bp_group_member_count(); ?>

				</div>

			</div>

			<div class="clear"></div>
		</li>

	<?php endwhile; ?>

	</ul>

	<?php

	/**
	 * Fires after the listing of the groups list.
	 *
	 * @since 1.1.0
	 */
	do_action( 'bp_after_directory_groups_list' ); ?>

	<div id="pag-bottom" class="pagination">

		<div class="pag-count" id="group-dir-count-bottom">

			<?php bp_groups_pagination_count(); ?>

		</div>

		<div class="pagination-links" id="group-dir-pag-bottom">

			<?php bp_groups_pagination_links(); ?>

		</div>

	</div>

<?php else:

	if ( bp_is_my_profile() ) { ?>
		<div class="text-center mt-5">
			<p><strong>Join a group in the Unsealed Community</strong></p>
			<a href="<?php echo home_url('groups'); ?>" class="btn-unsealed small">
					<span class="btn-unsealed-text"><?php esc_attr_e( 'Find Groups', 'buddypress' ); ?></span>
					<div class="btn-background-text" aria-hidden="true">
						<?php for($y = 0; $y < 3; $y++){
							echo '<span>';
							for($x = 0; $x < 10; $x++){
								echo 'Find Groups' . '  ';
							}
							echo '</span>';
						} ?>
					</div>
			</a>
		</div>
	<?php
	} else { ?>
		<div id="message" class="info text-center">
			<p><?php _e( 'This Unsealers groups are not available.', 'buddypress' ); ?></p>
		</div>
	<?php
	} ?>

<?php endif; ?>

<?php

/**
 * Fires after the display of groups from the groups loop.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_groups_loop' );
