<?php
/**
 * BuddyPress Letters camera template.
 *
 * This template is used to create the camera Backbone views.
 *
 * @since 2.3.0
 *
 * @package BuddyPress
 * @subpackage bp-attachments
 * @version 3.0.0
 */

?>
<script id="tmpl-bp-cover-image-webcam" type="text/html">
	<# if ( ! data.user_media ) { #>
		<div id="bp-webcam-message">
			<p class="warning"><?php esc_html_e( 'Your browser does not support this feature.', 'buddypress' );?></p>
		</div>
	<# } else { #>
		<div id="letter-to-crop"></div>
		<div class="letter-crop-management">
			<div id="letter-crop-pane" class="letter" style="width:{{data.w}}px; height:{{data.h}}px"></div>
			<div id="letter-crop-actions">
				<a class="button letter-webcam-capture" href="#"><?php esc_html_e( 'Capture', 'buddypress' );?></a>
				<a class="button letter-webcam-save" href="#"><?php esc_html_e( 'Save', 'buddypress' );?></a>
			</div>
		</div>
	<# } #>
</script>
