<?php
/**
 * BuddyPress Letter images main template.
 *
 * This template is used to inject the BuddyPress Backbone views
 * dealing with letters.
 *
 * It's also used to create the common Backbone views.
 *
 * @since 2.3.0
 *
 * @package BuddyPress
 * @subpackage bp-attachments
 * @version 3.0.0
 */

/**
 * This action is for internal use, please do not use it
 */
// do_action( 'bp_attachments_letter_check_template' );
?>
<div class="bp-letter-image-nav"></div>
<div class="bp-letter-image"></div>
<div class="bp-letter-image-status"></div>

<script type="text/html" id="tmpl-bp-letter-image-nav">
	<a href="{{data.href}}" class="bp-letter-image-nav-item" data-nav="{{data.id}}">{{data.name}}</a>
</script>

<?php bp_attachments_get_template_part( 'uploader' ); ?>

<?php bp_attachments_get_template_part( 'letters/crop' ); ?>

<?php bp_attachments_get_template_part( 'letters/camera' ); ?>

<script id="tmpl-bp-letter-image-delete" type="text/html">
	<# if ( 'user' === data.object ) { #>
		<p><?php _e( "If you'd like to delete your current letter image but not upload a new one, please use the delete letter image button.", 'buddypress' ); ?></p>
		<p><a class="button edit" id="bp-delete-letter" href="#"><?php esc_html_e( 'Delete My Profile Photo', 'buddypress' ); ?></a></p>
	<# } else if ( 'group' === data.object ) { #>
		<p><?php _e( "If you'd like to remove the existing group profile photo but not upload a new one, please use the delete group profile photo button.", 'buddypress' ); ?></p>
		<p><a class="button edit" id="bp-delete-letter" href="#"><?php esc_html_e( 'Delete Group Profile Photo', 'buddypress' ); ?></a></p>
	<# } else { #>
		<?php do_action( 'bp_attachments_letter_delete_template' ); ?>
	<# } #>
</script>

<?php do_action( 'bp_attachments_letter_main_template' );
