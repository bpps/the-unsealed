<?php
/**
 * BuddyPress Avatars crop template.
 *
 * This template is used to create the crop Backbone views.
 *
 * @since 2.3.0
 *
 * @package BuddyPress
 * @subpackage bp-attachments
 * @version 3.0.0
 */

?>
<script id="tmpl-bp-cover-image-item" type="text/html">
	<div id="letter-to-crop">
		<img src="{{{data.url}}}"/>
	</div>
	<div class="letter-crop-management">
		<div id="letter-crop-pane" class="letter" style="width:{{data.full_w}}px; height:{{data.full_h}}px">
			<img src="{{{data.url}}}" id="letter-crop-preview"/>
		</div>
		<div id="letter-crop-actions">
			<a class="button letter-crop-submit" href="#"><?php esc_html_e( 'Crop Image', 'buddypress' ); ?></a>
		</div>
	</div>
</script>
