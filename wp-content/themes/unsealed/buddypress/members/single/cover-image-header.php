<?php
/**
 * BuddyPress - Users Cover Image Header
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 * @version 3.0.0
 */

?>

<?php

/**
 * Fires before the display of a member's header.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_member_header' ); ?>
<?php
		$profile_link = trailingslashit( bp_loggedin_user_domain() . bp_get_profile_slug() );
?>
<div id="cover-image-container">
	<a id="header-cover-image" href="<?php bp_displayed_user_link(); ?>">
	</a>
	<?php if (bp_is_my_profile()): ?>
	<span class="btn-change-avatar d-flex justify-content-center align-items-center" id="btn-change-cover-image">
		<a href="<?=trailingslashit( $profile_link . 'change-cover-image' );?>" class="d-flex justify-content-center align-items-center" data-toggle="tooltip" data-placement="top" title="Change Cover Image">
			<i class="fa fa-edit"></i>
		</a>
	</span>
	<?php endif; ?>
	<div id="item-header-cover-image">
		<div class="d-flex info-wrapper">
			<div id="item-header-avatar">
				<a href="<?php bp_displayed_user_link(); ?>">
					<?php bp_displayed_user_avatar( 'type=full' ); ?>
				</a>
				<?php if (bp_is_my_profile()): ?>
				<span class="btn-change-avatar d-flex justify-content-center align-items-center">
					<a href="<?=trailingslashit( $profile_link . 'change-avatar' );?>" class="d-flex justify-content-center align-items-center" data-toggle="tooltip" data-placement="top" title="Change Avatar">
						<i class="fa fa-edit"></i>
					</a>
				</span>
				<?php endif; ?>
			</div><!-- #item-header-avatar -->

			<div id="item-header-content">

				<?php if ( bp_is_active( 'activity' ) && bp_activity_do_mentions() ) : ?>
					<div class="top d-flex align-items-center mt-2">
						<?php
						$user_id = bp_displayed_user_id();
						$display_name = bp_get_displayed_user_fullname();
						$hide_full_name = bp_get_user_meta( $user_id, 'username_only', true );
						if ( $hide_full_name && !bp_is_my_profile() ) {
							$display_name = bp_get_displayed_user_mentionname();
						} ?>
						<h2 class="user-nicename mb-0">
							<?php
							echo $display_name;
							if ( $hide_full_name && bp_is_my_profile() ) { ?>
								<small class="small">(Publicly hidden)</small>
							<?php
							} ?>
						</h2>
						<?php
						if (bp_is_my_profile()): ?>
							<a type="button" class="button ml-4 mt-0 mr-0" href="<?=trailingslashit( $profile_link . 'edit' );?>">
								Edit Profile
								<i class="fa fa-pencil ml-1"></i>
							</a>
						<?php endif; ?>
					</div>
					<?php
					if ( !$hide_full_name || ($hide_full_name && bp_is_my_profile()) ) { ?>
						<span class="mention-name underline-shape">@<?php bp_displayed_user_mentionname(); ?></span>
					<?php
					}

				endif; ?>
				<?php

				/**
				 * Fires before the display of the member's header meta.
				 *
				 * @since 1.2.0
				 */
				do_action( 'bp_before_member_header_meta' ); ?>

				<div id="item-meta">

					<?php if ( bp_is_active( 'activity' ) ) : ?>

						<div id="latest-update">

							<?php bp_activity_latest_update( bp_displayed_user_id() ); ?>

						</div>

					<?php endif; ?>

					<?php

					/**
						* Fires after the group header actions section.
						*
						* If you'd like to show specific profile fields here use:
						* bp_member_profile_data( 'field=About Me' ); -- Pass the name of the field
						*
						* @since 1.2.0
						*/
					do_action( 'bp_profile_header_meta' );

					?>

				</div><!-- #item-meta -->

			</div><!-- #item-header-content -->
			<?php if (bp_is_my_profile()):
				$user_domain = bp_loggedin_user_domain(); ?>
				<div class="ml-auto d-flex flex-column align-items-end">
					<div class="ml-auto mt-2 item-icons-button-wrapper">
						<a href="<?php echo $user_domain . 'friends/requests'; ?>" class="header-link-groups position-relative mr-1" data-toggle="tooltip" data-placement="top" title="Friends Request">
							<i class="fa fa-users"></i>
							<span class="d-flex justify-content-center align-items-center position-absolute" data-cnt="<?=bp_friend_get_total_requests_count(bp_loggedin_user_id()); ?>"><?=bp_friend_get_total_requests_count(bp_loggedin_user_id()); ?></span>
						</a>
						<?php
						$notifications = bp_notifications_get_notifications_for_user( bp_loggedin_user_id(), 'object' );
						$notification_count = 0;
						if ( is_array($notifications) ) {
							foreach( $notifications as $notification ) {
								$notification_count = $notification_count + $notification->total_count;
							}
						} ?>
						<a href="<?php echo $user_domain . 'notifications'; ?>" class="header-link-notification position-relative mr-1" data-toggle="tooltip" data-placement="top" title="Notifications">
							<i class="fa fa-bell"></i>
							<?php if ( $notification_count > 0 ) { ?>
								<span class="d-flex justify-content-center align-items-center position-absolute"><?php echo $notification_count; ?></span>
							<?php } ?>
						</a>
						<?php
						if ( bp_is_active( 'messages' ) ) {
							$unread_messages = messages_get_unread_count( bp_loggedin_user_id() ); ?>
							<a href="<?php echo $user_domain . 'messages'; ?>" class="header-link-message position-relative mr-1" data-toggle="tooltip" data-placement="top" title="Messages">
								<i class="fa fa-envelope"></i>
								<?php if ( $unread_messages > 0 ) { ?>
									<span class="d-flex justify-content-center align-items-center position-absolute" data-cnt="0"></span>
								<?php } ?>
							</a>
						<?php
						} ?>
						<a href="<?php echo $user_domain . 'settings'; ?>" class="header-link-settings position-relative" data-toggle="tooltip" data-placement="top" title="Settings">
							<i class="fa fa-cog"></i>
						</a>
					</div>
					<a class="d-block" href="<?php echo home_url('members'); ?>"><u>Search members</u></a>
				</div>
			<?php else: ?>
				<div class="ml-auto mt-2" id="other-user-account-button-wrapper">
					<?php do_action( 'bp_member_header_actions' ); ?>
					<?php
					if ( friends_check_friendship_status( bp_loggedin_user_id(), bp_displayed_user_id() ) == 'not_friends' ) { ?>
						<div id="send-private-message" class="generic-button"><span class="send-message inactive" data-toggle="tooltip" data-placement="top" title="request friend first" data-original-title="Private Message"></span></div>
					<?php
					} ?>
				</div>
			<?php endif;?>
		</div>
	</div><!-- #item-header-cover-image -->
</div><!-- #cover-image-container -->

<?php

/**
 * Fires after the display of a member's header.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_member_header' ); ?>

<div id="template-notices" role="alert" aria-atomic="true">
	<?php

	/** This action is documented in bp-templates/bp-legacy/buddypress/activity/index.php */
	do_action( 'template_notices' ); ?>

</div>
