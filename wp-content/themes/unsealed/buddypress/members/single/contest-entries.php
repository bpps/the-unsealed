<?php
$type = 'contest-submissions';
$args = array (
  'post_type' => $type,
  'author' => bp_displayed_user_id(),
  'post_status' => 'publish',
  'paged' => $paged,
  'posts_per_page' => -1
);
$temp = $wp_query; // assign ordinal query to temp variable for later use
$wp_query = null;
$wp_query = new WP_Query($args); ?>
<h2 class="text-center mt-3 mb-5">Contest Entries</h2>
<?php 
if ( $wp_query->have_posts() ) : ?>
   <div class="letters d-flex flex-wrap">
      <?php
      while ( $wp_query->have_posts() ) : $wp_query->the_post();
         echo get_contest_entry_lockup(get_the_id());
      endwhile; ?>
   </div>
<?php
else : ?>
   <div><h2>No contest entries</h2></div>
<?php
endif;
$wp_query = $temp;
