<?php
  if ( is_user_logged_in() && bp_is_my_profile() ) {
    do_action( 'bp_before_member_activity_post_form' );
    bp_get_template_part( 'activity/post-form' );
    do_action( 'bp_after_member_activity_post_form' );
  }

  $userDescription = bp_get_profile_field_data(array('field' => 2, 'user_id' => bp_current_user_id()));
  $userQuote = bp_get_profile_field_data(array('field' => 3, 'user_id' => bp_current_user_id()));

  $facebook_url = bp_get_profile_field_data(array('field' => 'Facebook', 'user_id' => bp_current_user_id()));
  $twitter_url = bp_get_profile_field_data(array('field' => 'Twitter', 'user_id' => bp_current_user_id()));
  $instagram_url = bp_get_profile_field_data(array('field' => 'Instagram', 'user_id' => bp_current_user_id())); ?>

    <?php
    if ( $userDescription || $userQuote ) {
      if ( $userDescription ) { ?>
        <h2 class="text-center">About me</h2>
        <div class="text-center mt-3 ml-auto mr-auto" style="max-width: 930px;">
          <?php echo apply_filters('the_content', $userDescription); ?>
        </div>
      <?php
      }
      if ( $userQuote ) {
        if ( $userQuote[0] == '"' ) {
          $userQuote = ltrim($userQuote, '"');
        }
        if ( $userQuote[strlen($userQuote) - 1] == '"' ) {
          $userQuote = rtrim($userQuote, '"');
        }
        $userQuote = '"' . $userQuote . '"'; ?>
        <h3 class="text-center mt-5">My favorite quote</h2>
        <div class="text-center mt-3 ml-auto mr-auto" style="max-width: 930px;">
          <?php echo apply_filters('the_content', $userQuote); ?>
        </div>
      <?php
      }
    } else if ( bp_is_my_profile() ) { ?>
      <p class="text-center mt-5 ml-auto mr-auto" style="max-width: 930px;">
        <a href="<?php echo bp_loggedin_user_domain() . 'profile/edit'; ?>"><u>Complete your profile</u></a>
      </p>
    <?php
    } ?>
  </p>
  <?php
  if ( $facebook_url || $twitter_url || $instagram_url ) { ?>
    <div class="d-flex justify-content-end align-items-center social-list mt-5 pt-2">
      <label class="m-0">Follow </label>
      <div class="d-flex justify-content-end align-items-center">
        <?php
        if ( $facebook_url ) { ?>
          <a href="https://facebook.com/<?=$facebook_url;?>" class="ml-4"><i class="fa fa-facebook"></i></a>
        <?php
        }
        if ( $twitter_url ) { ?>
          <a href="http://twitter.com/<?=$twitter_url;?>" class="ml-3"><i class="fa fa-twitter"></i></a>
        <?php
        }
        if ( $instagram_url ) { ?>
          <a href="https://www.instagram.com/<?=$instagram_url;?>" class="ml-3"><i class="fa fa-instagram"></i></a>
        <?php
        } ?>
      </div>
    </div>
  <?php
  } ?>
