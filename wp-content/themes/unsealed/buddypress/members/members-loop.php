<?php
/**
 * BuddyPress - Members Loop
 *
 * Querystring is set via AJAX in _inc/ajax.php - bp_legacy_theme_object_filter()
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 * @version 3.0.0
 */

/**
 * Fires before the display of the members loop.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_members_loop' ); ?>

<?php if ( bp_get_current_member_type() ) : ?>
	<p class="current-member-type"><?php bp_current_member_type_message() ?></p>
<?php endif; ?>

<?php if ( bp_has_members( bp_ajax_querystring( 'members' ) ) ) : ?>

	<div id="pag-top" class="pagination">

		<!-- <div class="pag-count" id="member-dir-count-top">

			<?php bp_members_pagination_count(); ?>

		</div> -->

		<div class="pagination-links" id="member-dir-pag-top">

			<?php bp_members_pagination_links(); ?>

		</div>

	</div>

	<?php

	/**
	 * Fires before the display of the members list.
	 *
	 * @since 1.1.0
	 */
	do_action( 'bp_before_directory_members_list' ); ?>

	<ul id="members-list" class="item-list" aria-live="assertive" aria-relevant="all">

	<?php
	// echo $_REQUEST['s'];
	// $args = [
	// 	's' => $_REQUEST['s'],
	// 	'meta_key' => 'username_only',
	// 	'meta_value' => undefined,
	// ];
	while ( bp_members() ) : bp_the_member(); ?>

		<li <?php bp_member_class(); ?>>
			<div class="item-avatar">
				<a href="<?php bp_member_permalink(); ?>"><?php bp_member_avatar(); ?></a>
			</div>

			<div class="item">
				<div class="item-title">
					<a href="<?php bp_member_permalink(); ?>"><?php bp_member_name(); ?></a>

					<!--<?php if ( bp_get_member_latest_update() ) : ?>

						<span class="update"> <?php bp_member_latest_update(); ?></span>

					<?php endif; ?>-->
					<?php
					if ( $bio = bp_get_profile_field_data(array('field' => 3, 'user_id' => bp_get_member_user_id())) ) {
						$shortened_bio = $bio;
						if (strlen($shortened_bio) > 80) {
			        $shortened_bio = substr($shortened_bio, 0, 80) . '...';
				    } ?>
						<span><?php echo strlen($shortened_bio); echo $shortened_bio; ?></span>
					<?php
					} ?>

				</div>

				<div class="item-meta"><span class="activity" data-livestamp="<?php bp_core_iso8601_date( bp_get_member_last_active( array( 'relative' => false ) ) ); ?>"><?php bp_member_last_active(); ?></span></div>

				<?php

				/**
				 * Fires inside the display of a directory member item.
				 *
				 * @since 1.1.0
				 */
				do_action( 'bp_directory_members_item' ); ?>

				<?php
				 /***
				  * If you want to show specific profile fields here you can,
				  * but it'll add an extra query for each member in the loop
				  * (only one regardless of the number of fields you show):
				  *
				  * bp_member_profile_data( 'field=the field name' );
				  */
				?>
			</div>

			<div class="action">

				<?php

				/**
				 * Fires inside the members action HTML markup to display actions.
				 *
				 * @since 1.1.0
				 */
				do_action( 'bp_directory_members_actions' ); ?>

			</div>

			<div class="clear"></div>
		</li>

	<?php endwhile; ?>

	</ul>

	<?php

	/**
	 * Fires after the display of the members list.
	 *
	 * @since 1.1.0
	 */
	do_action( 'bp_after_directory_members_list' ); ?>

	<?php bp_member_hidden_fields(); ?>

	<div id="pag-bottom" class="pagination">

		<div class="pag-count" id="member-dir-count-bottom">

			<?php bp_members_pagination_count(); ?>

		</div>

		<div class="pagination-links" id="member-dir-pag-bottom">

			<?php bp_members_pagination_links(); ?>

		</div>

	</div>

<?php else:

	if ( bp_is_my_profile() ) { ?>
		<div class="text-center mt-5">
			<p><strong>Find a friend in the Unsealed Community</strong></p>
			<a href="<?php echo home_url('members'); ?>" class="btn-unsealed small">
					<span class="btn-unsealed-text"><?php esc_attr_e( 'Find Friends', 'buddypress' ); ?></span>
					<div class="btn-background-text" aria-hidden="true">
						<?php for($y = 0; $y < 3; $y++){
							echo '<span>';
							for($x = 0; $x < 10; $x++){
								echo 'Find Friends' . '  ';
							}
							echo '</span>';
						} ?>
					</div>
			</a>
		</div>
	<?php
	} else { ?>
		<div id="message" class="info text-center">
			<p><?php _e( "This Unsealer's friends list is not available", 'buddypress' ); ?></p>
		</div>
	<?php
	}

	 endif; ?>

<?php

/**
 * Fires after the display of the members loop.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_members_loop' );
