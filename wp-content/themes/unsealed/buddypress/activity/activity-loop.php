<?php
/**
 * BuddyPress - Activity Loop
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 * @version 3.0.0
 */

/**
 * Fires before the start of the activity loop.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_activity_loop' ); ?>



<?php if ( bp_has_activities( bp_ajax_querystring( 'activity' ) ) ) : ?>

	<?php if ( empty( $_POST['page'] ) ) : ?>

		<ul id="activity-stream" class="activity-list item-list">

	<?php endif; ?>

	<?php while ( bp_activities() ) : bp_the_activity(); ?>

		<?php bp_get_template_part( 'activity/entry' ); ?>

	<?php endwhile; ?>

	<?php if ( bp_activity_has_more_items() ) : ?>

		<li class="load-more">
			<a href="<?php bp_activity_load_more_link() ?>" class="btn-unsealed small">
				<span class="btn-unsealed-text"><?php _e( 'Load More', 'buddypress' ); ?></span>
				<div class="btn-background-text" aria-hidden="true">
					<?php for($y = 0; $y < 3; $y++){
						echo '<span>';
						for($x = 0; $x < 10; $x++){
							echo 'Load more' . '  ';
						}
						echo '</span>';
					} ?>
				</div>
			</a>
		</li>

	<?php endif; ?>

	<?php if ( empty( $_POST['page'] ) ) : ?>

		</ul>

	<?php endif; ?>

<?php else : ?>
	<div id="message" class="info">
		<p><?php _e( 'Sorry, there was no activity found. Please try a different filter.', 'buddypress' ); ?></p>
	</div>

<?php endif; ?>
<div class="modal fade form-modal" id="upvote-entry" tabindex="-1" role="dialog" aria-labelledby="upvote-entry-label" aria-hidden="true">
	<div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
		<div class="modal-content">
			<div class="modal-body text-center">
				<div class="py-5 text-center">
					<div class="py-4">
						<h2 class="h3 mb-4" id="upvote-entry-label">Become a member or log in to vote</h2>
						<div class="row justify-content-center">
							<div class="col-auto">
								<?php echo unsealed_btn('Log in', home_url('login?redirect_to=' . bp_get_requested_url()), 'small'); ?>
							</div>
							<div class="col-auto">
								<?php echo unsealed_btn('Subscribe', home_url('sign-up?redirect_to=' . bp_get_requested_url()), 'small'); ?>
							</div>
						</div>
					</div>
					<div class="pt-4" id="contest-newsletter-signup">
						<h3 class="mb-4 h4">Or sign up for our newsletter</h3>
						<form id="upvote-entry-form">
							<input class="p-1" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required />
							<button class="ml-2" type="submit">submit</button>
						</form>
					</div>
					<div class="pt-4 d-none" id="contest-verify-code">
						<h3 class="mb-4 h5">Enter the verification code that was sent to your email</h3>
						<form id="contest-verify-form">
							<input class="p-1" type="text" placeholder="Verification code" required/>
							<input type="hidden" value=""/>
							<button class="ml-2" type="submit">Verify</button>
							<button class="ml-2" id="resend-code" type="button">Resend Code</button>
							<p class="alert d-none mt-2">Incorrect verification code</p>
							<p class="code-resent d-none mt-2">Your code has been sent</p>
						</form>
					</div>
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</div>
	</div>
</div>
<?php

/**
 * Fires after the finish of the activity loop.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_activity_loop' ); ?>

<?php if ( empty( $_POST['page'] ) ) : ?>

	<form action="" name="activity-loop-form" id="activity-loop-form" method="post">

		<?php wp_nonce_field( 'activity_filter', '_wpnonce_activity_filter' ); ?>

	</form>

<?php endif;
