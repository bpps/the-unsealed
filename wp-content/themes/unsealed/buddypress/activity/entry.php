<?php
/**
 * BuddyPress - Activity Stream (Single Item)
 *
 * This template is used by activity-loop.php and AJAX functions to show
 * each activity.
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 * @version 3.0.0
 */

/**
 * Fires before the display of an activity entry.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_activity_entry' ); ?>
<?php
	$user = wp_get_current_user();
	$roles = $user->roles;
	$allowed_types = ['activity_comment', 'activity_update', 'contest_entry'];
	$isSubscriber = pmpro_hasMembershipLevel();
	$activity_type = bp_get_activity_type();
	$activity_item_id = bp_get_activity_item_id();
	// $activity_id = bp_get_activity_item_id() !== 1 ? $activity_item_id : bp_get_activity_id();
	$activity_id = bp_get_activity_id();
	$activity_group = in_array($activity_type, $allowed_types) ? groups_get_group( ['group_id' => $activity_item_id] ) : null;
	$group_id = $activity_item_id;

	if ( $has_group_id = bp_activity_get_meta( $activity_id, '_group_id', true ) ) {
		$group_id = $has_group_id;
		$activity_group = groups_get_group( ['group_id' => $has_group_id] );
	}

	$view_type = bp_activity_get_meta( $activity_id, 'view_type' );
	$contest = bp_activity_get_meta( $activity_id, 'contest_entry' ); ?>

	<li class="<?php bp_activity_css_class(); ?>" id="activity-<?php bp_activity_id(); ?>">
		<div class="activity-avatar">
			<a href="<?php bp_activity_user_link(); ?>">
				<?php bp_activity_avatar(); ?>
			</a>
		</div>

		<div class="activity-content">

			<div class="activity-header">
				<?php 
				bp_activity_action(); 
				if ( $view_type == 'community' ) { ?>
					<p class="small mt-0">This post is viewable by the Unsealed community only.</p>
				<?php
				} ?>
			</div>
				
			<?php
			$reports_obj = get_bp_activity_report( bp_get_activity_id(), get_current_user_id() );
			if ( bp_activity_has_content() && bp_get_activity_content_body() !== '' ) :
				$isMyPost = is_user_logged_in() && bp_get_activity_user_id() === bp_loggedin_user_id(); ?>
				<div class="activity-content-letter-wrapper">
					<div class="activity-content-letter">
						<?php
						if ( $reports_obj['reported'] ) { ?>
							<span class="letter-reported" aria-role="alert">⚠️ This letter has been reported</span>
						<?php
						}
						if ( $image_id = bp_activity_get_meta(bp_get_activity_id(), 'featured_image') ) {
							$image_url = wp_get_attachment_image_src($image_id, 'medium')[0]; ?>
							<div class="activity-featured-image" style="background-image: url(<?php echo $image_url; ?>);"></div>
						<?php
						}
						if ( $title = bp_activity_get_meta(bp_get_activity_id(), 'title') ) { ?>
							<h3 class="mb-3 activity-data-field"><?php echo $title; ?></h3>
							<?php
							if ( $isMyPost ) { ?>
								<div class="activity-edit-field">
									<label for="activity-edit-title"><small class="d-block">Title</small>
										<input placeholder="Letter Title" name="activity-edit-title" id="activity-edit-title-<?php echo bp_get_activity_id(); ?>" class="activity-edit-title mt-2" type="text" value="<?php echo $title; ?>"/>
									</labe>
								</div>
							<?php 
							}
						}

						if ( ( $isSubscriber == false && $view_type !== 'community') || $isSubscriber == true ) { ?>
							<div class="activity-inner">
								<?php bp_activity_content_body(); ?>
							</div>
							<?php
							if ( $signature = bp_activity_get_meta(bp_get_activity_id(), 'signature') ) { ?>
								<span class="teal script-font text-end mt-3 mb-0 h2 activity-data-field"><?php echo $signature; ?></span>
								<?php
								if ( $isMyPost ) { ?>
									<div class="activity-edit-field">
										<label for="activity-edit-signature"><small class="d-block">Signature</small>
											<input placeholder="Signature" name="activity-edit-signature" id="activity-edit-signature-<?php echo bp_get_activity_id(); ?>" class="script-font activity-edit-signature mt-2" type="text" value="<?php echo $signature; ?>"/>
										</label>
									</div>
								<?php 
								} 
							}
							if ( $isMyPost ) { ?>
								<div class="activity-edit-field activity-edit-submit-wrapper">
									<input id="activity-edit-submit-<?php echo bp_get_activity_id(); ?>" class="mt-3 btn activity-edit-submit" type="Submit" value="Update Letter"/>
								</div>
							<?php 
							} 
							if ( bp_get_activity_type() == 'activity_update' ) {
								$icon_fb = get_template_directory() . '/img/facebook.svg';
								$icon_twitter = get_template_directory() . '/img/twitter.svg';
								$icon_linkedin = get_template_directory() . '/img/linkedin.svg';
								$icon_copy = get_template_directory() . '/img/copy-link.svg';
								$share_url = bp_activity_get_permalink(bp_get_activity_id()); ?>
								<div class="activity-share-links-wrapper justify-content-end d-flex align-items-center mt-3"><span class="h6 mb-0 mr-2">Share </span>
									<ul class="activity-share-links">
										<li>
											<a class="copy-activity-link" href="<?php echo $share_url; ?>">
												<span class="copied-activity-url">Copied URL</span>
												<?php echo file_get_contents($icon_copy); ?>
											</a>
										</li>
										<li>
											<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $share_url; ?>" name="Share on Facebook" target="_blank">
												<?php echo file_get_contents($icon_fb); ?>
											</a>
										</li>
										<li>
											<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode($share_url);?>&title=<?php echo urlencode(bp_activity_get_meta(bp_get_activity_id(), 'title') ); ?>" name="Share on LinkedIn" target="_blank">
												<?php echo file_get_contents($icon_linkedin); ?>
											</a>
										</li>
										<li>
											<a href="https://twitter.com/intent/tweet?url=<?php echo urlencode($share_url);?>&text=<?php echo urlencode(bp_activity_get_meta(bp_get_activity_id(), 'title') ); ?>" name="Share on Twitter" target="_blank">
												<?php echo file_get_contents($icon_twitter); ?>
											</a>
										</li>
									</ul>
								</div>
							<?php
							}
						} else { ?>
							<div class="entry-content text-center d-flex align-items-center">
								<div class="activity-content-logged-out text-center">
									<?php
									if ( $activity_type == 'activity_comment' ) { ?>
										<div class="text-center">
											<span class="small">Response to</span>
											<h3 class="text-center w-100"><?php echo bp_activity_get_meta($activity_id, 'title'); ?></h3>
											<span class="mt-2 mb-0 text-center text-secondary">—</span>
										</div>
									<?php
									} ?>
									<h4 class="mx-auto mt-3" style="line-height: 1.4;">This letter is only available to The Unsealed subscribers. Subscribe or login to get access!</h4>
									<div class="mt-1">
										<?php echo login_or_subscribe(get_the_permalink()); ?>
									</div>
								</div>
							</div>
						<?php
						} 
						//if ( $isMyPost ) { ?>
							<!--<a href="#edit-activity" data-activity-id="<?php echo bp_get_activity_id(); ?>" class="activity-edit">Edit Letter</a>-->
						<?php
						//} 
						?>
					</div>
				</div>

			<?php
			endif;

			if ( ( $isSubscriber == false && $view_type !== 'community') || $isSubscriber == true ) {
				/**
				 * Fires after the display of an activity entry content.
				 *
				 * @since 1.2.0
				 */
				do_action( 'bp_activity_entry_content' );
				// NOTE: changed to look for activity group first
				if ( $isSubscriber && !groups_is_user_member( get_current_user_id(), $group_id ) && $activity_group !== null && $activity_type != 'contest_entry' ) {
					if ( $activity_group->id !== 0 && in_array($activity_type, $allowed_types) ) { ?>
						<div class="d-flex align-items-center mb-3 flex-wrap justify-content-end">
							<div class="d-flex align-items-center">
								Join the &nbsp<a class="teal font-weight-bold" href="<?php echo home_url('groups/' . $activity_group->slug); ?>">group</a>&nbsp and reply&nbsp
								<?php
								echo bp_get_group_join_button( $activity_group ); ?>
							</div>
						</div>
					<?php
					}
				}
				// NOTE: changed to look for activity group first
				if ( !$isSubscriber && $activity_group !== null ) { ?>
					<div class="d-flex justify-content-end justify-content-md-between w-100 align-items-center flex-wrap flex-md-nowrap" style="font-size: .9rem;">
						<div>
							<p class="text-center text-md-right justify-content-end">
								<a class="teal font-weight-bold" href="<?php echo home_url('sign-up'); ?>">Subscribe&nbsp</a> or&nbsp
								<a href="<?php echo home_url('login'); ?>" class="teal font-weight-bold">log in&nbsp</a>
								<?php if ( $activity_group->id !== 0 && $activity_group->id !== null ) { ?>
									and join the group&nbsp<?php
								} ?>to reply
							</p>
						</div>
						<div class="activity-meta">
							<?php entry_favorites(); ?>
						</div>
					</div>
				<?php
				} ?>

				<div class="activity-meta">
					<div>
						<?php
						if ( is_user_logged_in() && bp_get_activity_user_id() !== bp_loggedin_user_id() ) {
							$icon_flag = get_template_directory() . '/img/flag.svg'; ?>
							<a href="#report-letter" class="report-letter d-flex align-items-center<?php if ($reports_obj['user_reported']) { echo ' reported'; } ?>" data-user-id="<?php echo get_current_user_id(); ?>" data-activity-user-id="<?php echo bp_get_activity_user_id(); ?>" data-letter-id="<?php echo bp_get_activity_id(); ?>">
								<?php echo file_get_contents($icon_flag); ?><span class="ml-1">Report Abuse</span></a>
						<?php }
						if ( bp_get_activity_type() == 'activity_comment' ) : ?>
							<a href="<?php bp_activity_thread_permalink(); ?>" class="button view bp-secondary-action"><?php _e( 'View Conversation', 'buddypress' ); ?></a>
						<?php
						endif; ?>
					</div>
					<?php
					if ( is_user_logged_in() ) : ?>
						<div>
							<?php
							if ( in_array($activity_type, $allowed_types) ) :
								if ( bp_activity_can_comment() ) : ?>

									<a href="<?php bp_activity_comment_link(); ?>" class="px-2 mr-0 d-flex align-items-center button acomment-reply bp-primary-action" id="acomment-comment-<?php bp_activity_id(); ?>">
										<?php 
										if ( $isSubscriber ) { ?>
											<small class="mr-2 h2 mb-0 script-font" style="text-transform: none;">Write me back</small>
										<?php 
										} ?>
										<div style="max-width: 15px; top: 1px; margin-right: .25rem;">
											<?php include get_template_directory() . '/img/comment.svg'; ?>
										</div>
										<?php
										printf('<span class="count ml-2">' . bp_activity_get_comment_count() > 0 ? bp_activity_get_comment_count() : '' . '</span>' ); ?>
									</a>

								<?php
								endif;
							endif;

							entry_favorites();

							if ( bp_activity_user_can_delete() ) bp_activity_delete_link(); ?>

						</div>

						<?php

						/**
						 * Fires at the end of the activity entry meta data area.
						 *
						 * @since 1.2.0
						 */
						do_action( 'bp_activity_entry_meta' ); ?>

					<?php
					endif; ?>

				</div>
			<?php
			} ?>

		</div>

		<?php
		if ( ( $isSubscriber == false && $view_type !== 'community') || $isSubscriber == true ) {
			/**
			 * Fires before the display of the activity entry comments.
			 *
			 * @since 1.2.0
			 */
			do_action( 'bp_before_activity_entry_comments' ); ?>

			<?php if ( ( bp_activity_get_comment_count() || bp_activity_can_comment() ) || bp_is_single_activity() ) : ?>

				<div class="activity-comments">

					<?php bp_activity_comments(); ?>

					<?php if ( is_user_logged_in() && bp_activity_can_comment() ) : ?>

						<form action="<?php bp_activity_comment_form_action(); ?>" method="post" id="ac-form-<?php bp_activity_id(); ?>" class="ac-form"<?php bp_activity_comment_form_nojs_display(); ?>>
							<div class="ac-reply-avatar"><?php bp_loggedin_user_avatar( 'width=' . BP_AVATAR_THUMB_WIDTH . '&height=' . BP_AVATAR_THUMB_HEIGHT ); ?></div>
							<div class="ac-reply-content">
								<div class="d-flex justify-content-end mb-1">
									<?php
										$wordCount = get_field('form_validation_word_count', 'option');
										$wordMax = get_field('form_validation_word_max', 'option') ?? null;
										if (!$wordCount) {
											$wordCount = 100;
										}
									?>
									<span class="reply-validation mt-auto text-end small"><span class="current-words-cnt mr-2 small"></span><?=$wordCount; ?> words min.</span>
									<?php
									if ( $wordMax ) { ?>
										<span class="small word-max">&nbsp / <?php echo $wordMax; ?> words max</span>
									<?php
									} ?>
								</div>
								<div class="ac-textarea">
									<label for="ac-input-<?php bp_activity_id(); ?>" class="bp-screen-reader-text"><?php
										/* translators: accessibility text */
										_e( 'Comment', 'buddypress' );
									?></label>
									<textarea id="ac-input-<?php bp_activity_id(); ?>" class="ac-input bp-suggestions" name="ac_input_<?php bp_activity_id(); ?>"></textarea>
								</div>
								<input type="submit" name="ac_form_submit" value="<?php esc_attr_e( 'Post', 'buddypress' ); ?>" /> &nbsp; <a href="#" class="ac-reply-cancel"><?php _e( 'Cancel', 'buddypress' ); ?></a>
								<input type="hidden" name="comment_form_id" value="<?php bp_activity_id(); ?>" />
							</div>

							<?php

							/**
							 * Fires after the activity entry comment form.
							 *
							 * @since 1.5.0
							 */
							do_action( 'bp_activity_entry_comments' ); ?>

							<?php wp_nonce_field( 'new_activity_comment', '_wpnonce_new_activity_comment_' . bp_get_activity_id() ); ?>

						</form>

					<?php endif; ?>

				</div>

			<?php
			endif;

			/**
			 * Fires after the display of the activity entry comments.
			 *
			 * @since 1.2.0
			 */
			do_action( 'bp_after_activity_entry_comments' );
		} ?>

	</li>
<?php


/**
 * Fires after the display of an activity entry.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_activity_entry' );
