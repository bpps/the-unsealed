<?php
/**
 * BuddyPress - Activity Post Form
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 * @version 3.0.0
 */

$group_ids = groups_get_user_groups( bp_loggedin_user_id() );
if ( $group_ids['total'] ) { ?>
	<form action="<?php bp_activity_post_form_action(); ?>" enctype="multipart/form-data" method="post" id="whats-new-form" name="whats-new-form">
		<?php
		/**
		 * Fires before the activity post form.
		 *
		 * @since 1.2.0
		 */
		do_action( 'bp_before_activity_post_form' ); ?>

		<div id="whats-new-avatar" class="d-none md-d-block" style="padding-top: 20px;">
			<a href="<?php echo bp_loggedin_user_domain(); ?>">
				<?php bp_loggedin_user_avatar( 'width=' . bp_core_avatar_thumb_width() . '&height=' . bp_core_avatar_thumb_height() ); ?>
			</a>
		</div>

		<!-- <p class="activity-greeting">
			<?php
			if ( bp_is_group() ) {
				/* translators: 1: group name. 2: member name. */
				printf( __( 'What\'s new in %1$s, %2$s?', 'buddypress' ), bp_get_group_name(), bp_get_user_firstname( bp_get_loggedin_user_fullname() ) );
			} else {
				/* translators: %s: member name */
				printf( __( "What's new, %s?", 'buddypress' ), bp_get_user_firstname( bp_get_loggedin_user_fullname() ) );
			}

			// Define unsplash key in js variable
			wp_localize_script('unsealed-scripts', 'unsplash_key', ['unsplash_key' => UNSPLASH_KEY] );
			?>
		</p> -->

		<div id="whats-new-content">
			<p class="post-confirmation text-center" style="display: none;">Your letter has been submitted! View your letter <span></span></p>

			<div id="toggle-letter-contest">
				<label for="toggle-letter">
					<input id="toggle-letter" type="radio" name="toggle-letter-contest" checked value="letter">
					<span>Letter</span>
				</label>
				<label for="toggle-contest">
					<input id="toggle-contest" type="radio" name="toggle-letter-contest" value="contest">
					<span>Contest</span>
				</label>
			</div>

			<div id="contest-prompt" class="d-none text-center"><h3 class="h4 my-4" style="line-height: 1.2"></h3></div>

			<div id="whats-new-textarea">
				<input type="file" name="unsealed-letter-upload" accept=".png, .jpeg, .jpg, .gif" style="display:none;" value="post_update" id="unsealed-letter-upload" />
				<div id="letter-crop-container">
					<div class="text-right crop-btn">
						<a href="crop-image" class="mt-2 btn crop-finished">Finished Cropping</a>
					</div>
					<canvas name="unsealed-letter-image-canvas" class="d-none" id="unsealed-letter-image-canvas"></canvas>
				</div>
				<input type="hidden" name="canvas-image"/>
				<div id="letter-crop-complete">
					<img class="w-100 h-auto" name="unsealed-letter-image-preview" src="" id="unsealed-letter-image-preview"/>
					<div class="text-right crop-btn">
						<a class="mt-2 activity-remove-image btn" id="remove-activity-image" href="#remove-image">Remove Image</a>
					</div>
				</div>
				<input id="letter-image-x" name="x" type="hidden">
				<input id="letter-image-y" name="y" type="hidden">
				<input id="letter-image-w" name="w" type="hidden">
				<input id="letter-image-h" name="h" type="hidden">
				<input id="letter-image-file" type="hidden" name="letter_image" style="display:none;" />

				<div class="contests-form-content d-none">
					<?php 
					$date = date('Y-m-d H:i:s',strtotime("today"));
					$contests = get_posts( [
						'post_type' => 'contest',
						'meta_query' => [
							'relation' => 'AND',
							[
								'key'       => 'contest_end_date',
								'value'     => $date,
								'compare'   => '>=',
								'type'      => 'DATETIME',
							],
							[
								'key'       => 'contest_start_date',
								'value'     => $date,
								'compare'   => '<=',
								'type'      => 'DATETIME',
							],
						]
					]);
					if ( $contests ) { ?>
						<div id="available-contests">
							<?php
							foreach ( $contests as $contest ) {
								$args = [
									'per_page' => 1,
									'display_comments' => false,
									'user_id' => bp_loggedin_user_id(),
									'meta_query' => [
										[
											'key'       => 'contest_id',
											'value'     => $contest->ID,
											'compare'   => '='
										],
									]
								];
								$has_entered_contest = false;
								if ( bp_has_activities( $args ) ) :
									$has_entered_contest = true;
								endif;
								$image = get_the_post_thumbnail_url($contest->ID, 'small-medium'); ?>
								<div class="w-100 contest-entry contest-<?php echo $contest->post_name; ?>" data-contest-id="<?php echo $contest->ID; ?>">
									<div data-contest-id="<?php echo $contest->ID; ?>" class="w-100 d-flex">
										<?php
										if ( $image ) { ?>
											<div class="dropdown-tag-img-cont col-6 d-none d-md-block">
												<img src="<?php echo $image; ?>" alt="<?php echo $contest->post_title; ?>" class='dropdown-tag-image'/>
											</div>
										<?php
										} ?>
										<div>
											<span class="contest-prompt"><?php echo $contest->post_title; ?></span>
											<div class="mt-2">
												<?php 
												if ( $has_entered_contest ) { ?>
													<p class="mt-3" style="line-height: 1.3; font-size: .85rem;">You have already submitted an entry for this contest.</p>
													<p class="mt-3" style="line-height: 1.3; font-size: .85rem;">We will notify you when a winner is chosen!</p>
												<?php 
												} else {
													echo unsealed_btn('Submit your story', '#submit-contest-entry', 'small', 'data-contest-id="' . $contest->ID . '"');
												 } ?>
											</div>
										</div>
									</div>
								</div>
							<?php
							} ?>
						</div>
						<input class="contest-id" type="hidden" name="contest-id" value />
					<?php 
					} else { ?>
						<p class="text-center">Check back for new contests!</p>
					<?php
					} ?>
				</div>
				<div class="letter-form-image-content <?php if ( !$contests ) { echo 'no-contests'; } ?>">
					<div id="activity-add-image-wrapper" class="mb-2">
						<div class="activity-add-image w-100">
							<div class="activity-add-image-inner">
								<a href="#" class="d-flex justify-content-center align-items-center w-100 h-100 activity-upload-image"><span>Upload Photo</br><small style="display:block;">Please follow our uploaded imagery guidelines and only upload an image that you own and have rights to.</small></span></a>
							</div>
						</div>
					</div>
					<input class="dst-file" type="hidden" name="dst-file" value="" />
					<input class="letter-image-upload-file" type="hidden" name="letter-image-upload-file" value="" />
					<input class="validate-image" type="hidden" name="entry-img" value="">
				</div>
				<!-- <div>
					<p class="text-center">Or search unsplash for stock photography</p>
					<div class="d-flex w-100 unsplash-search-container">
						<input id="unsplash-keywords" class="unsplash-keywords" type="text" placeholder="search image keywords"/>
						<button class="unsplash-search" id="unsplash-search">Search</button>
					</div>
				</div> -->
				<div id="unsplash-popup" class="<?php if ( !$contests ) { echo 'no-contests'; } ?>">
					<div id="unsplash-popup__header">
						<div id="unsplash-popup__close" class="uppercase text-right mb-2"><span>Close</span></div>
						<div class="d-flex w-100 unsplash-search-container">
							<input id="unsplash-keywords-inner" class="unsplash-keywords" type="text" placeholder="search image keywords"/>
							<button class="unsplash-search" id="unsplash-search-inner">Search</button>
						</div>
					</div>
					<div id="unsplash-popup__content-wrapper">
						<div id="unsplash-popup__loader">
						</div>
						<div id="unsplash-popup__content-wrapper-nav" class="mb-2">
							<div class="unsplash-prev unsplash-nav-button">
								<?php include get_template_directory() . '/img/arrow.svg'; ?>
							</div>
							<div class="unsplash-count"></div>
							<div class="unsplash-next unsplash-nav-button">
								<?php include get_template_directory() . '/img/arrow.svg'; ?>
							</div>
						</div>
						<div id="unsplash-popup__content-wrapper-content">
						</div>
						<div class="text-right">
							<button id="unsplash-submit" class="unsplash-popup__content-wrapper-submit" disabled="disabled">
								Submit
							</button>
						</div>
						</button>
					</div>
				</div>
				<div class="letter-form-content">
					<label for="activity_title" class="mt-3 validate-title">
					<input id="activity_title" required class="py-1 px-2 h4" type="text" name="activity_title" placeholder="Letter Title"></input>
						*Required
					</label>
					<div class="d-flex justify-content-end mb-1">
						<?php
							$wordCount = get_field('form_validation_word_count', 'option') ?? 100;
							$wordMax = get_field('form_validation_word_max', 'option') ?? null;
						?>
						<span id="validation" class="mt-auto text-end small"><span class="current-words-cnt mr-2 small"></span><?=$wordCount; ?> words min </span>
						<?php
						if ( $wordMax ) { ?>
							<span class="small word-max">&nbsp / <?php echo $wordMax; ?> words max</span>
						<?php } ?>
					</div>
					<textarea class="bp-suggestions py-1 px-2 validate-count" name="whats-new" id="whats-new" cols="50" rows="10" placeholder="Dear Unsealers..."
						<?php if ( bp_is_group() ) : ?>data-suggestions-group-id="<?php echo esc_attr( (int) bp_get_current_group_id() ); ?>" <?php endif; ?>
					><?php if ( isset( $_GET['r'] ) ) : ?>@<?php echo esc_textarea( $_GET['r'] ); ?> <?php endif; ?></textarea>
					<div class="d-flex justify-content-end mb-2"><span class="teal font-weight-bold">Please remember this is a safe space. Be kind. Be truthful.</span></div>
					<input id="activity_signature" class="pt-1 px-2 mt-2 mb-2 script-font teal h2" type="text" name="activity_signature" placeholder="Signature"></input>
				</div>
			</div>
			<div class="writing-links d-flex justify-content-end px-2 mt-3<?php if ( !$contests ) { echo ' no-contests'; } ?>">
				<ul class="list-unstyled d-flex">
					<li class="px-2">
						<a class="small text-underline" target="_blank" href="<?php echo home_url('rules-for-submissions'); ?>"><u>rules for submitting</u></a>
					</li>
					<li class="px-2">
						<a class="small text-underline" target="_blank" href="<?php echo home_url('resources'); ?>"><u>get help</u></a>
					</li>
					<li class="px-2">
						<a class="small text-underline" target="_blank" href="<?php echo home_url('how-to-write-a-great-letter'); ?>"><u>writing tips</u></a>
					</li>
					<li class="px-2">
						<a class="small text-underline feedback-link" href="mailto:lauren@theunsealed.com" data-mail-to="lauren@theunsealed.com"><u>email us for feedback on writing</u></a>
					</li>
				</ul>
			</div>
			<div class="after-form-options d-flex flex-column flex-md-row justify-content-end align-items-center mt-2 md-mt-5 pt-2 pb-3 validate-group<?php if ( !$contests ) { echo ' no-contests'; } ?>">
				<div id="after-form-options-selectors" class="d-flex align-items-center">
					<div class="metavalue-selector d-flex align-items-center pr-3">
						<?php
							$tooltipContent = get_field('post_meta_value_tooltip', 'option');
							if (!$tooltipContent) {
								$tooltipContent = 'Selecting Unsealed Community will limit this post to Unsealed Subscribers like you!';
							}
						?>
						<select name="metavalue" class="mr-2">
							<option value="public">Public</option>
							<option value="community">Unsealed Community</option>
						</select>
						<span class="icon" data-toggle="tooltip" data-placement="top" title="<?=$tooltipContent;?>">
							<svg width="15" height="19" viewBox="0 0 15 19" fill="none" xmlns="http://www.w3.org/2000/svg">
								<circle cx="7.5" cy="9.5" r="7" fill="#69CDC7" stroke="black"/>
								<path d="M8.11 11.35C8.11 11.5033 8.11 11.6267 8.11 11.72C8.11 11.8067 8.11333 11.8667 8.12 11.9C8.14 12.1 8.17 12.2 8.21 12.2C8.20333 12.2 8.22 12.1967 8.26 12.19C8.3 12.1833 8.35 12.1733 8.41 12.16L8.4 12.17C8.42 12.1633 8.44333 12.1567 8.47 12.15C8.50333 12.1433 8.54333 12.14 8.59 12.14C8.93667 12.14 9.11 12.2867 9.11 12.58C9.11 12.7133 9.05333 12.8233 8.94 12.91C8.83333 12.9967 8.69667 13.04 8.53 13.04C8.47667 13.04 8.41 13.0367 8.33 13.03C8.25 13.0233 8.15667 13.0133 8.05 13C7.95 12.9867 7.86333 12.9767 7.79 12.97C7.71667 12.9567 7.65667 12.95 7.61 12.95C7.53667 12.95 7.45333 12.9533 7.36 12.96C7.27333 12.9667 7.18 12.9767 7.08 12.99C6.96 13.0033 6.85667 13.0133 6.77 13.02C6.69 13.0333 6.62333 13.04 6.57 13.04C6.21 13.04 6.03 12.8867 6.03 12.58C6.03 12.2867 6.19333 12.14 6.52 12.14C6.53333 12.14 6.57 12.1433 6.63 12.15C6.69667 12.1567 6.75 12.1633 6.79 12.17C6.83667 12.1833 6.87333 12.19 6.9 12.19C6.92667 12.19 6.94667 12.19 6.96 12.19C7.01333 12.19 7.05 12.03 7.07 11.71V9.3C7.05 8.98667 7.01 8.83 6.95 8.83C6.93667 8.83 6.91333 8.83333 6.88 8.84C6.84667 8.84667 6.80667 8.85667 6.76 8.87C6.72667 8.88333 6.68667 8.89333 6.64 8.9C6.6 8.9 6.56667 8.9 6.54 8.9C6.2 8.9 6.03 8.75333 6.03 8.46C6.03 8.16 6.22333 8.01 6.61 8.01C6.68333 8.01 6.74333 8.01 6.79 8.01C6.83667 8.01 6.87 8.01333 6.89 8.02C6.98333 8.03333 7.06 8.04333 7.12 8.05C7.18667 8.05667 7.23 8.06 7.25 8.06C7.26333 8.06 7.29 8.06 7.33 8.06C7.37667 8.05333 7.42 8.04667 7.46 8.04L7.72 8.01C8.00667 8.01 8.15 8.17667 8.15 8.51V8.65C8.14333 8.67667 8.14 8.7 8.14 8.72C8.14 8.73333 8.14 8.74333 8.14 8.75C8.13333 8.77 8.13 8.86333 8.13 9.03C8.13 9.07667 8.12667 9.12667 8.12 9.18C8.12 9.22667 8.12 9.27667 8.12 9.33C8.11333 9.46333 8.11 9.59667 8.11 9.73C8.11 9.85667 8.11 9.98667 8.11 10.12V11.35ZM7.45 7.77C7.25667 7.77 7.09333 7.70667 6.96 7.58C6.82667 7.45333 6.76 7.29667 6.76 7.11C6.76 6.92333 6.82667 6.77 6.96 6.65C7.09333 6.52333 7.25667 6.46 7.45 6.46C7.64333 6.46 7.80667 6.52333 7.94 6.65C8.08 6.77 8.15 6.92333 8.15 7.11C8.15 7.29667 8.08 7.45333 7.94 7.58C7.80667 7.70667 7.64333 7.77 7.45 7.77Z" fill="black"/>
							</svg>
						</span>
					</div>
					<div id="whats-new-options">
						<?php if ( bp_is_active( 'groups' ) && !bp_is_group() ) : ?>
							<div id="whats-new-post-in-box" class="position-relative">
									<label class="d-block position-absolute mb-0" style="bottom: 100%; left: 0;" for="whats-new-post-in">*Required</label>
									<select id="whats-new-post-in" required class="mt-0" name="whats-new-post-in">
										<option disabled selected="selected">Select a Group</option>
										<?php if ( bp_has_groups( 'user_id=' . bp_loggedin_user_id() . '&type=alphabetical&max=100&per_page=100&populate_extras=0&update_meta_cache=0' ) ) :
											while ( bp_groups() ) : bp_the_group(); ?>
												<option value="<?php bp_group_id(); ?>"><?php bp_group_name(); ?></option>
											<?php endwhile;
										endif; ?>
									</select>
							</div>
						<?php elseif ( bp_is_group_activity() ) : ?>
							<input type="hidden" id="whats-new-post-object" name="whats-new-post-object" value="groups" />
							<input type="hidden" id="whats-new-post-in" name="whats-new-post-in" value="<?php bp_group_id(); ?>" />

						<?php endif;

						/**
						 * Fires at the end of the activity post form markup.
						 *
						 * @since 1.2.0
						 */
						do_action( 'bp_activity_post_form_options' ); ?>

					</div><!-- #whats-new-options -->
				</div>
				<div id="whats-new-form-submit-wrapper">
					<div id="whats-new-submit-decoy" class="mt-0">
						<button type="button"  name="aw-whats-new-submit-decoy" class="btn-unsealed small" id="aw-whats-new-submit-decoy">
								<span class="btn-unsealed-text"><?php esc_attr_e( 'Submit', 'buddypress' ); ?></span>
								<div class="btn-background-text" aria-hidden="true">
									<?php for($y = 0; $y < 3; $y++){
										echo '<span>';
										for($x = 0; $x < 10; $x++){
											echo 'Submit' . '  ';
										}
										echo '</span>';
									} ?>
								</div>
						</button>
					</div>
					<div id="whats-new-submit" style="display: none;" class="mt-0">
						<button type="submit"  name="aw-whats-new-submit" class="btn-unsealed small" id="aw-whats-new-submit">
								<span class="btn-unsealed-text"><?php esc_attr_e( 'Submit', 'buddypress' ); ?></span>
								<div class="btn-background-text" aria-hidden="true">
									<?php for($y = 0; $y < 3; $y++){
										echo '<span>';
										for($x = 0; $x < 10; $x++){
											echo 'Submit' . '  ';
										}
										echo '</span>';
									} ?>
								</div>
						</button>
					</div>
				</div>
			</div>
		</div><!-- #whats-new-content -->

		<?php wp_nonce_field( 'post_update', '_wpnonce_post_update' ); ?>
		<?php

		/**
		 * Fires after the activity post form.
		 *
		 * @since 1.2.0
		 */
		do_action( 'bp_after_activity_post_form' ); ?>

	</form><!-- #whats-new-form -->
<?php
} else { ?>
	<div class="text-center">
		<p>To post a letter, you'll need to join a group</p>
		<a href="<?php echo home_url('groups'); ?>" id="join-groups-btn" class="btn-unsealed small">
				<span class="btn-unsealed-text"><?php esc_attr_e( 'Find Groups', 'buddypress' ); ?></span>
				<div class="btn-background-text" aria-hidden="true">
					<?php for($y = 0; $y < 3; $y++){
						echo '<span>';
						for($x = 0; $x < 10; $x++){
							echo 'Find Groups' . '  ';
						}
						echo '</span>';
					} ?>
				</div>
		</a>
	</div>
<?php
} ?>
