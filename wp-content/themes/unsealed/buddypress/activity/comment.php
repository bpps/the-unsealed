<?php
/**
 * BuddyPress - Activity Stream Comment
 *
 * This template is used by bp_activity_comments() functions to show
 * each activity.
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 * @version 3.0.0
 */

/**
 * Fires before the display of an activity comment.
 *
 * @since 1.5.0
 */
do_action( 'bp_before_activity_comment' );

$isSubscriber = pmpro_hasMembershipLevel();
$group_id = bp_activity_get_meta( bp_get_activity_comment_id(), '_group_id', true );
$activity_group = groups_get_group( ['group_id' => $group_id] ); ?>

<li id="acomment-<?php bp_activity_comment_id(); ?>">
	<div class="acomment-avatar">
		<a href="<?php bp_activity_comment_user_link(); ?>">
			<?php bp_activity_avatar( 'type=thumb&user_id=' . bp_get_activity_comment_user_id() ); ?>
		</a>
	</div>

	<div class="acomment-meta">
		<?php
		/* translators: 1: user profile link, 2: user name, 3: activity permalink, 4: ISO8601 timestamp, 5: activity relative timestamp */
		printf( __( '<a href="%1$s">%2$s</a> replied <a href="%3$s" class="activity-time-since"><span class="time-since" data-livestamp="%4$s">%5$s</span></a>', 'buddypress' ), bp_get_activity_comment_user_link(), bp_get_activity_comment_name(), bp_get_activity_comment_permalink(), bp_core_get_iso8601_date( bp_get_activity_comment_date_recorded() ), bp_get_activity_comment_date_recorded() );
		?>
	</div>
	<?php
	$reports_obj = get_bp_activity_report( bp_get_activity_comment_id(), get_current_user_id() );

	if ( $reports_obj['reported'] ) { ?>
		<span class="letter-reported" aria-role="alert">⚠️ This letter has been reported</span>
	<?php
	} ?>

	<div class="acomment-content"><?php bp_activity_comment_content(); ?></div>

	<?php if ( $isSubscriber && $group_id && !groups_is_user_member( get_current_user_id(), $group_id ) ) { ?>
		<div class="d-flex justify-content-end align-items-center mt-2 mb-2 flex-wrap">
			<p class="mr-3 d-flex align-items-center" style="font-size: .9rem;" ><small class="mr-2 h2 mb-0 script-font" style="text-transform: none;">Write me back&nbsp</small></p>
			<div class="d-flex align-items-center flex-wrap">
				<span>Join the &nbsp<a class="teal font-weight-bold" href="<?php echo home_url('groups/' . $activity_group->slug); ?>">group</a>&nbsp and reply&nbsp</span>
				<span class="text-nowrap">
					<?php echo bp_get_group_join_button( $activity_group ); ?>
				</span>
			</div>
		</div>
	<?php
	}

	if ( !$isSubscriber ) { ?>
		<div class="d-flex justify-content-end justify-content-md-between w-100 align-items-center flex-wrap flex-md-nowrap" style="font-size: .9rem;" >
			<small class="mr-2 h2 mb-0 script-font" style="text-transform: none;">Write me back&nbsp</small>
			<div>
				<p class="text-center text-md-right">
					<a class="teal font-weight-bold" href="<?php echo home_url('sign-up'); ?>">Subscribe&nbsp</a> or&nbsp
					<a href="<?php echo home_url('login'); ?>" class="teal font-weight-bold">log in&nbsp</a>
					<?php if ( $activity_group->id !== 0 ) { ?>
						and join the group<?php } ?>&nbspto reply
				</p>
			</div>
		</div>
	<?php
	} ?>

	<div class="acomment-options d-flex justify-content-between pl-4">

		<?php if ( is_user_logged_in() ) {
			$icon_flag = get_template_directory() . '/img/flag.svg'; ?>
			<a href="#report-letter" class="report-letter d-flex align-items-center<?php if ($reports_obj['user_reported']) { echo ' reported'; } ?>" data-user-id="<?php echo get_current_user_id(); ?>" data-activity-user-id="<?php echo bp_get_activity_user_id(); ?>" data-letter-id="<?php echo bp_get_activity_comment_id(); ?>">
				<?php echo file_get_contents($icon_flag); ?><span class="ml-1">Report Abuse</span></a>
		<?php } ?>
		<div>
			<?php if ( is_user_logged_in() && bp_activity_can_comment_reply( bp_activity_current_comment() ) ) : ?>

				<a href="#acomment-<?php bp_activity_comment_id(); ?>" class="acomment-reply bp-primary-action" id="acomment-reply-<?php bp_activity_id(); ?>-from-<?php bp_activity_comment_id(); ?>"><?php _e( 'Reply', 'buddypress' ); ?></a>

			<?php endif; ?>

			<?php if ( bp_activity_user_can_delete() ) : ?>

				<a href="<?php bp_activity_comment_delete_link(); ?>" class="delete acomment-delete confirm bp-secondary-action" rel="nofollow"><?php _e( 'Delete', 'buddypress' ); ?></a>

			<?php endif; ?>
		</div>
		<?php

		/**
		 * Fires after the default comment action options display.
		 *
		 * @since 1.6.0
		 */
		do_action( 'bp_activity_comment_options' ); ?>

	</div>

	<?php bp_activity_recurse_comments( bp_activity_current_comment() ); ?>
</li>

<?php

/**
 * Fires after the display of an activity comment.
 *
 * @since 1.5.0
 */
do_action( 'bp_after_activity_comment' );
