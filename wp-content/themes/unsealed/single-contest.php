<?php
/**
 * The template for displaying contest single
 */

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php
    while ( have_posts() ) :
    the_post();
      $prize = get_field('contest_prize');
      $contest_id = get_the_ID();
      $subscriber_id = get_current_user_id();
      $today = time();
      $contest_end_date = get_field('contest_end_date', $contest_id);
      $rules = get_field('contest_rules');
      $winners = get_field('contest_winners');
      $prompt = get_field('contest_prompt');
      $contest_active = false;
      // $dt = new DateTime('now', new DateTimeZone('America/New_York'));
      // $dt->setTimestamp(strtotime($contest_end_date));

      if ( $today < strtotime($contest_end_date)  ) {
        $contest_active = true;
      } ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
          <div class="d-flex flex-column align-items-center">
            <?php if(get_the_post_thumbnail()){ ?>
              <div class="header-bg-img">
                <?php echo get_the_post_thumbnail(); ?>
              </div>
              <div class="header-img">
                <?php echo get_the_post_thumbnail(); ?>
              </div>
            <?php } ?>
            <div class="container">
              <?php
              the_title( '<h1 class="entry-title">', '</h1>' );
              if ( $prize ) { ?>
                <div class="single-to-from mb-3 mb-md-5">
                  <h2><?php echo $prize ?></h2>
                </div>
              <?php
              }
              if ( $sponsor = get_field('sponsor') ) {
                $sponsor_link = get_field('sponsor_link'); ?>
                <div class="d-flex justify-content-center pb-3">
                  <div class="col-auto">
                    <p class="mb-0 d-inline-block">
                      <?php
                      if ( $sponsor_link ) { ?>
                        <a class="teal" href="<?php echo $sponsor_link; ?>" target="_blank">
                      <?php
                      }
                      echo $sponsor;
                      if ( $sponsor_link ) { ?>
                        </a>
                      <?php
                      } ?>
                    </p>
                  </div>
                </div>
              <?php
              } ?>
              <div class="divider-h mt-3 mb-3 mb-md-5 d-flex justify-content-center">
                <img src="<?php echo get_template_directory_uri(); ?>/img/brush-btn-1.png"/>
              </div>
            </div>
          </div>
        </header><!-- .entry-header -->
        <?php echo get_announcement(); ?>

        <!-- <div class="container py-5 container-narrow"> -->

        <div id="main-content" class="d-flex py-5 flex-wrap flex-lg-row align-items-start overflow-hidden">
          <?php
          echo new_to_unsealed(); ?>
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-xl-6 offset-xl-0">
            <section class="entry-content mb-5">
              <div id="contest-intro" class="mb-5">
                <?php
                if ( $intro = get_field('contest_intro') ) { ?>
                  <div class="intro-content mb-5">
                    <?php echo $intro; ?>
                  </div>
                <?php
                }
                if ( !$contest_active ) {
                  if ( $winners ) {
                    $ended_winners = '<p class="text-center">Congratulations to our winner' . ( count($winners) > 1 ? 's' : '' ) . '!</p>'; ?>
                    <h3><?php echo $ended_winners; ?></h3>
                    <div class="container py-5">
                      <div class="row justify-content-center">
                        <?php
                        foreach( $winners as $winner ) { ?>
                          <div class="column col-12 col-md-4">
                            <div class="text-center">
                              <?php
                              $winner_id = $winner->ID;
                              $winner_link = get_the_permalink($winner_id);
                              $privacy_level = get_field('privacy_level', $winner_id);
                              $winner_author = get_userdata(get_field('member', $winner_id));
                              $winner_image = has_post_thumbnail($winner_id)
                                ? get_the_post_thumbnail_url($winner_id, 'small-medium')
                                : get_template_directory_uri() . '/img/placeholder.png';
                              switch ($privacy_level) {
                                case '1':
                                  $winner_name = $winner_author->first_name[0] . $winner_author->last_name[0];
                                break;
                                case '2':
                                  $winner_name = $winner_author->first_name;
                                break;
                                case '3':
                                  $winner_name = $winner_author->first_name . ' ' . $winner_author->last_name;
                                break;
                                default:
                                  $winner_name = $privacy_level;
                                break;
                              } ?>
                              <a class="winner-image" href="<?php echo $winner_link; ?>">
                                <img src="<?php echo $winner_image; ?>"/>
                              </a>
                              <p class="script-font teal h1"><?php echo $winner_name; ?></p>
                              <a href="<?php echo $winner_link; ?>"><?php echo $winner->post_name; ?></a>
                            </div>
                          </div>
                        <?php
                        } ?>
                      </div>
                    </div>
                  <?php
                  } else {
                    $ended_no_winners = get_field('contest_ended_text') ? get_field('contest_ended_text') : '<p class="text-center">This contest has ended. The winner will be announced soon!</p>';
                    echo $ended_no_winners;
                  }
                } ?>
              </div>
              <?php
              if ( $rules && $contest_active ) { ?>
                <div id="contest-rules">
                  <h2 class="script-font text-center display-2 as-seen-header mb-2">The Rules</h2>
                  <?php echo $rules; ?>
                </div>
              <?php
              }
              if ( $contest_active ) { ?>
                <div class="text-center">
                  <?php
                  if ( !get_field('hide_submit_button') ) {
                    if ( pmpro_hasMembershipLevel() ) {
                      $args = [
                        'per_page' => 1,
                        'display_comments' => false,
                        'user_id' => $subscriber_id,
                        'meta_query' => [
                          [
                            'key'       => 'contest_id',
                            'value'     => $contest_id,
                            'compare'   => '='
                          ],
                        ]
                      ];
                      // FOR TESTING ONLY
                      // $has_entered_contest = false;
                      if ( bp_has_activities( $args ) ) :
                          $has_entered_contest = true;
                      endif;
                      if ( $has_entered_contest ) { ?>
                        <div class="my-5">
                          <h4>You've already submitted an entry to this contest!</h4>
                          <p>If you need to request a change to your entry, please contact us at <a class="teal fw-bold" href="mailto:contests@theunsealed.com">contests@theunsealed.com</a></p>
                          <p>Entries will be judged and announced at the end of the contest.</p>
                        </div>
                      <?php
                      } else {
                        $contest_text = get_field('submit_button_text') ? get_field('submit_button_text') : 'Submit your story';
                        $contest_link = bp_loggedin_user_domain() . '?contest=' . $contest_id;
                        $contest_props = get_field('submit_button_profile') ? false : 'data-toggle="modal" data-target="#submit-contest-entry"';
                        echo unsealed_btn($contest_text, $contest_link, 'large', $contest_props);
                      }
                    } else { ?>
                      <div>
                        <?php
                        $contest_text = get_field('submit_button_text') ? get_field('submit_button_text') : 'Submit your story';
                        $contest_redirect = get_field('submit_button_profile') ? bp_loggedin_user_domain() : get_the_permalink();
                        echo unsealed_btn($contest_text, pmpro_url("checkout", "?redirect_to" . $contest_redirect, "https"), 'large'); ?>
                        <div class="mt-1">
                          <?php echo login_or_subscribe($contest_redirect); ?>
                        </div>
                      </div>
                    <?php
                    }
                   } ?>
                </div>
              <?php
              } ?>
            </section><!-- .entry-content -->

            <section class="comments-container py-4 mb-4">
              <div class="d-flex flex-column-reverse flex-md-row justify-content-md-between">
                <?php
                if ( comments_open() || get_comments_number() ){ ?>
                  <a <?php echo pmpro_hasMembershipLevel() ? 'class="show-comments"' : 'href="'.pmpro_url('checkout', '?redirect_to=' . get_the_permalink(), 'https').'"'; ?>><span><?php echo get_comments_number() == 1  ? get_comments_number() . " </span> comment" : get_comments_number() . '</span> comments'; ?></a>
                <?php
                } ?>
                <div class='ml-4 d-flex flex-1 justify-content-end align-items-center'>
                  <span class="script-font h2 mb-0">Share this contest</span>
                  <div class="d-flex ml-2">
                    <?php echo do_shortcode('[addtoany]'); ?>
                  </div>
                </div>
              </div>
              <?php
              if ( comments_open() || get_comments_number() ) { ?>
                <div class="all-comments">
                  <?php comments_template(); ?>
                </div>
              <?php
              } ?>
            </section>

          </div>
          <div class="col-12 col-lg-3"></div>
        </div>

        <?php
        echo more_contests();
        if ( $contest_active ) { ?>
          <div id="contest-entry-label" class="d-none">Submit your contest entry</div>
          <div class="modal fade form-modal" id="submit-contest-entry" tabindex="-1" role="dialog" aria-labelledby="contest-entry-label" aria-hidden="true">
          	<div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
          		<div class="modal-content">
          			<div class="modal-body">
                  <h2 class="script-font display-4 text-center"><?php echo $prompt; ?></h2>
          				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        						<span aria-hidden="true">&times;</span>
        					</button>
                  <form data-contest-id="<?php echo $contest_id; ?>" data-member-id="<?php echo $subscriber_id; ?>" class="d-flex flex-column align-items-center w-75 mx-auto">
                    <input name="entry-title" type="text" placeholder="Entry title" required class="w-100 mb-3 px-2 py-2"/>
                    <textarea name="entry-content" required placeholder="Tell your story" class="w-100 mb-3 px-2 py-2"></textarea>
                    <input name="entry-link" type="text" placeholder="Blog/portfolio link (Optional)" class="w-100 mb-3 px-2 py-2"/>
                    <div class="text-left w-100">
                      <a href="#" class="upload-entry-image">Upload image</a>
                      <img id="heading_picture_preview" class="heading-picture" src="" />
                      <a href="#" class="remove-entry-image" style="display:none">Remove image</a>
                    </div>
                    <input type="hidden" id="heading_picture" name="entry-img" value="">
                    <input class="mt-4" type="submit" value="Submit"/>
                  </form>
          			</div>
          		</div>
          	</div>
          </div>
        <?php
        } ?>

      </article><!-- #post-<?php the_ID(); ?> -->


    <?php endwhile; ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
