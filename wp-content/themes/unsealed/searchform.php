<div id="page-search-bar" class="w-100">
	<form class="d-flex justify-content-center py-3 px-4" action="<?php echo home_url(); ?>/" method="get">
		<input type="text" placeholder="search" name="s" id="search" value="<?php the_search_query(); ?>" />
		<button type="submit" value="search" alt="Search" />
			<?php include get_template_directory() . '/img/search.svg'; ?>
		</button>
	</form>
</div>
