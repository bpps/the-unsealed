<?php
/**
 * Template Name:  Login
 *
 * The template for displaying the account pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Unsealed
*/

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php
    while ( have_posts() ) :
    the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

          <div class="container-fluid w-100 mw-100 bg-black two-column-left">
            <div class="row justify-content-center">
              <div class="col-md-5 offset-md-1 px-md-4 py-5 text-center d-flex align-items-center justify-content-center flex-column">
                <header class="entry-header">
                  <?php
                  the_title( '<h1 class="entry-title text-center script-font display-3">', '</h1>' ); ?>
                </header><!-- .entry-header -->
                <?php the_content(); ?>
              </div>
              <?php
              if ( $images = get_field('side_images') ) {
                $random_img = rand(0, count($images) - 1); ?>
                <div class="col-md-6 position-relative two-column-image d-none d-md-block">
                  <div class="bg-cover card-img-overlay" style="background-image: url(<?php echo $images[$random_img]['sizes']['large']; ?>);">
                  </div>
                </div>
              <?php
              } ?>
            </div>
          </div>
        </div>
      </article><!-- #post-<?php the_ID(); ?> -->

    <?php
    endwhile; // End of the loop. ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
