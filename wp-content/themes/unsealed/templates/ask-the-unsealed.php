<?php
/**
 * Template Name:  Ask The Unsealed
 *
 * The template for displaying the advice columns
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Unsealed
*/

get_header();

echo display_qas();

get_footer();
