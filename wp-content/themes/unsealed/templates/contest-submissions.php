<?php
/**
 * Template Name:  Contest Submissions
 *
 * The template for displaying the contest submissions
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Unsealed
*/

get_header();

echo display_contest_submissions ();

get_footer();
