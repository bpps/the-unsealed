<?php
/**
 * Template Name:  Letters
 *
 * The template for displaying the letters
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Unsealed
*/

get_header();

$topics = get_applicable_terms( 'topic', [
  'hide_empty' => false,
  'post_types' => ['post']
]);

$topics = array_map( function ($topic) {
  if ( $topic_image = get_field('featured_image', $topic) ) {
    $topic->image = $topic_image['sizes']['medium'];
  }
  return $topic;
}, $topics);

while ( have_posts() ) :
the_post();

$all_letters = [ 'image' => null, 'description' => null ];
$all_letters['image'] = has_post_thumbnail() ? get_the_post_thumbnail_url( get_the_id() ) : false;
if ( $content = get_the_content() ) {
  $all_letters['description'] = apply_filters('the_content', $content);
}

endwhile;

wp_localize_script( 'unsealed-letters', 'topics', $topics );
wp_localize_script( 'unsealed-letters', 'allLettersHeader', $all_letters ); ?>

<div id="primary" class="content-area">
 <main id="main" class="site-main">
   <div id="root"></div>
 </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
