<?php
/**
 * Template Name:  Page Builder
 *
 * The template for displaying a custom page using blocks
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Unsealed
*/
get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main blocks-page">

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

      <?php the_content(); ?>

    </article>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
