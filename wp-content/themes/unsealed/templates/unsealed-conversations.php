<?php
/**
 * Template Name:  Unsealed Conversations
 *
 * The template for displaying the conversation
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Unsealed
*/
get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php
    while ( have_posts() ) :
    the_post();
    $intro_video = get_field('conversations_video', 'option');
    $intro_video = $intro_video['url'];
    $intro_video_placeholder = get_field('conversations_video_placeholder_image', 'option'); ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
          <div class="container">
            <div class="d-flex align-items-center row">
              <div class="col-md-5 pt-4 pb-5">
                <?php
                the_title( '<h1 class="script-font display-3 event-title mt-3">', '</h1>' );
                if ( $post->post_content ) {
                  echo wpautop($post->post_content);
                } ?>
              </div>
              <div class="col-md-7 py-5 px-md-5">
                <div class="inline-video-wrapper">
                  <div class="embed-responsive embed-responsive-16by9 position-relative unsealed-video-block inline-video">
                    <video poster="<?php echo $intro_video_placeholder['sizes']['medium']; ?>">
                      <source src="<?php echo $intro_video; ?>" type="video/mp4">
                      Your browser does not support the video tag.
                    </video>
                    <button class="play-icon rounded-circle d-flex justify-content-center align-items-center p-0">
            				</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header><!-- .entry-header -->

        <!-- <div class="container py-5 container-narrow"> -->

        <div class="d-flex py-3 py-md-5 flex-wrap flex-lg-row align-items-start">
          <?php
          echo new_to_unsealed(); ?>
          <div class="col-12 col-sm-10 offset-sm-1 col-md-9 col-lg-7 col-xl-6 offset-md-0">
            <div class="entry-content mb-5">
              <?php
              $args = [
                'post_type' => 'event',
                'posts_per_page' => 1,
                'post_status' => 'publish',
                'meta_key' => '_event_start_date',
                'orderby' => 'meta_value_num',
                'order' => 'ASC',
                'meta_query' => [
                  [
                    'key' => '_event_start_date', // Check the start date field
                    'value' => date("Y-m-d"), // Set today's date (note the similar format)
                    'compare' => '>=', // Return the ones greater than or equal to today's date
                    'type' => 'DATE' // Let WordPress know we're working with date
                  ]
                ]
              ];
              $events_query = new WP_Query( $args );
            	if ( $events_query->have_posts() ) : ?>
                <h2 class="text-center script-font display-4 mb-5">Join the next conversation</h2>
                </div>
                  <?php
              		while ( $events_query->have_posts() ) : $events_query->the_post();
                    $id = get_the_id();
                    $advisors = get_the_terms( get_the_id(), 'advisor' );
                    $advisor = $advisors ? $advisors[0] : false;
                    $advisor_alt = $advisor ? get_field('alt_name', $advisor) : false;
                    if ( $advisor ) { ?>
                      <div class="author-single-container d-flex flex-column align-items-center mb-2">
                        <?php
                        if ( $advisor_image = get_field('featured_image', $advisor) ) { ?>
                          <div class="author-single-image">
                            <div class="image-ratio rounded-circle mx-auto">
                              <div class="bg-cover d-flex justify-content-center align-items-center"
                                  style="background-image:url(<?php echo $advisor_image['sizes']['small']; ?>)">
                              </div>
                            </div>
                          </div>
                        <?php
                        } ?>
                        <h5 class="mt-1"><?php echo $advisor->name; ?></h5>

                      </div>
                    <?php
                    }
                    echo wpautop($post->post_content); ?>
                    <div class="mb-3 mt-5 pb-4 event-list-event">
                      <h3 class="mb-4">Join the conversation</h3>
                      <div class="d-flex justify-content-between">
                        <div class="event-details">
                          <?php
                          $start_date = get_post_meta($id, '_event_start_date')[0];
                          $start_time = get_post_meta($id, '_event_start_time')[0];
                          $end_date = get_post_meta($id, '_event_end_date')[0];
                          $end_time = get_post_meta($id, '_event_end_time')[0];
                          $event_location_text = get_post_meta($id, '_event_location_url_text') ? get_post_meta($id, '_event_location_url_text')[0] : null;
                          $event_location_url = get_post_meta($id, '_event_location_url') ? get_post_meta($id, '_event_location_url')[0] : null;

                          if ( $start_date ) { ?>
                            <p class="my-0"><?php echo date('l, F jS', strtotime($start_date)); ?></p>
                          <?php
                          }
                          if ( $start_time ) { ?>
                            <p class="my-0">
                              <?php
                              echo date('g:ia', strtotime($start_time));
                              if ( $end_time ) {
                                echo ' - ' . date('g:ia', strtotime($end_time));
                              } ?>
                            </p>
                          <?php
                          } ?>
                        </div>
                        <?php

                        $EM_Event = em_get_event($id, 'post_id');

                        if ( $EM_Event ) {
                          if ( $EM_Event->event_location ) {
                            if ( pmpro_hasMembershipLevel() ) {
                              echo unsealed_btn('Join on Zoom', $event_location_url, 'small');
                            } else { ?>
                              <div>
                                <?php
                                echo unsealed_btn('Subscribe to Join', pmpro_url("checkout", '?redirect_to=' . home_url('unsealed-conversations'), "https"), 'small'); ?>
                                <div class="mt-1">
                                  <?php echo login_or_subscribe(home_url('unsealed-conversations')); ?>
                                </div>
                              </div>
                            <?php
                            }
                          }
                        } ?>
                      </div>
                    </div>
                  <?php
              		endwhile;
              		wp_reset_postdata(); ?>
                </div>
              <?php
              else: ?>
                <p class="mb-5 text-center">There are no upcoming Unsealed Conversations but check back often!</p>
              <?php
            	endif; ?>
            </div><!-- .entry-content -->
          </div>
        </div>

      </article><!-- #post-<?php the_ID(); ?> -->


    <?php endwhile; ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
