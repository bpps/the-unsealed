<?php
/**
 * Template Name:  Subscribe
 *
 * The template for displaying subscribe page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Unsealed
*/
require_once( PMPRO_DIR . "/preheaders/checkout.php" );

get_header();
global $pmpro_review, $skip_account_fields; $pmpro_error_fields; ?>

<div id="primary" class="content-area">
 <main id="main" class="site-main">
   <?php
   while ( have_posts() ) :
   the_post(); ?>
     <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
       <header class="entry-header pb4">
         <div class="d-flex flex-column align-items-center py-4 py-md-5">
           <?php if(get_the_post_thumbnail()){ ?>
             <div class="header-bg-img">
               <?php echo get_the_post_thumbnail(); ?>
             </div>
             <div class="header-img">
               <?php echo get_the_post_thumbnail(); ?>
             </div>
           <?php } ?>
           <div class="container">
             <?php the_title( '<h1 class="entry-title script-font">', '</h1>' ); ?>
           </div>
         </div>
       </header><!-- .entry-header -->
       <?php echo get_announcement(); ?>

       <div id="unsealed-checkout-header" class="py-3">
         <div id="unsealed-checkout-timeline" class="font-weight-bold d-flex container justify-content-center">
           <?php
           $is_active = true;
           if ( isset($_REQUEST['level']) || isset($_REQUEST['one-time']) ) {
             $is_active = false;
           }
           if ( count($pmpro_error_fields) > 0 ) {
             $is_active = false;
           }
           if ( $pmpro_review ) {
             $is_current = false;
             $is_active = false;
           } ?>
           <div data-step="1" onClick="checkoutNextStep(1)" class="timeline-option active col-auto<?php if ( $is_active ) { echo ' current'; } ?>">
             <p class="my-0">Subscription</p>
           </div>
           <?php
           $is_active = false;
           $is_current = false;
           if ( isset($_REQUEST['level']) || isset($_REQUEST['one-time']) ) {
             $is_current = true;
             $is_active = true;
             if ( count($pmpro_error_fields) > 0 ) {
               $is_current = false;
             }
             if ( $pmpro_review ) {
               $is_current = false;
               $is_active = false;
             }
           } ?>
           <div data-step="2" onClick="checkoutNextStep(2)" class="timeline-option col-auto<?php if ( $is_active ) { ?> active<?php } ?><?php if ( $is_current ) { ?> current<?php } ?>">
             <p class="my-0">Pricing</p>
           </div>
           <?php
           $is_active = false;
           $account_errors = ['username','password','password2', 'bemail', 'bconfirmemail'];
           foreach ( $pmpro_error_fields as $error_field ) {
             if ( in_array($error_field, $account_errors) ) {
               $is_active = true;
             }
           } ?>
           <div data-step="3" onClick="checkoutNextStep(3)" class="timeline-option col-auto<?php if ( $is_active ) { ?> current active<?php } ?>">
             <p class="my-0">Account</p>
           </div>
           <?php
           $is_active = false;
           $account_errors = ['username','password','password2', 'bemail', 'bconfirmemail'];
           if ( count($pmpro_error_fields) > 0 ) {
             $is_active = true;
             foreach ( $pmpro_error_fields as $error_field ) {
               if ( in_array($error_field, $account_errors) ) {
                 $is_active = false;
               }
             }
           }
           if ( $pmpro_review ) {
             $is_active = true;
             $is_current = true;
           } ?>
           <div data-step="4" onClick="checkoutNextStep(4)" class="timeline-option col-auto<?php if ( $is_active ) { ?> current active<?php } ?>">
             <p class="my-0">Payment</p>
           </div>
         </div>
       </div>

       <div class="d-flex pb-5 flex-column flex-lg-row">
         <div class="col-12 col-lg-8 offset-lg-2">
           <div class="entry-content">
             <?php the_content(); ?>
           </div>
         </div>
       </div>

     </article>
    <?php
    endwhile; ?>
    <?php echo get_as_seen_on(); ?>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
