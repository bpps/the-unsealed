<?php
/**
 * Template Name:  Community letters
 *
 * The template for displaying the community letters
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Unsealed
*/

get_header();

echo display_community_voices();

get_footer();
