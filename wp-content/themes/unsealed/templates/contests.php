<?php
/**
 * Template Name:  Contests
 *
 * The template for displaying the contests
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Unsealed
*/

get_header();

while ( have_posts() ) :
the_post();

$contests = [ 'image' => null, 'description' => null ];
$featured_contest_id = get_field('featured_contest', 'option');
$image_id = $featured_contest_id ? $featured_contest_id : get_the_ID();
$contests['image'] = get_the_post_thumbnail_url( $image_id );
if ( $featured_contest_id ) {
  $contests['title'] = get_the_title($featured_contest_id);
  $contests['description'] = apply_filters('the_content', get_field('contest_intro', $featured_contest_id) );
  $contests['link'] = get_the_permalink($featured_contest_id);
  $contests['prize'] = get_field('contest_prize', $featured_contest_id);
  $contests['has_featured'] = true;
} else {
  $contests['title'] = get_the_title();
  $contests['description'] = get_the_content() ? apply_filters('the_content', get_the_content()) : false;
  $contests['has_featured'] = false;
}

endwhile;

wp_localize_script( 'unsealed-contests', 'contestsHeader', $contests ); ?>

<div id="primary" class="content-area">
 <main id="main" class="site-main">
   <div id="root"></div>
 </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
