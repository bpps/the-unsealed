<?php
/**
 * The header for our theme
 *
*/ ?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<title>The Unsealed</title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=5.0">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="facebook-domain-verification" content="w4gr8a5s6zdtugl5g3lbis4fh6wmbx" />
	<meta name = "viewport" content = "width=device-width, minimum-scale=1.0, maximum-scale = 1.0, user-scalable = no">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143287550-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-143287550-1');
	</script>
	<!--
	<link href="https://vjs.zencdn.net/7.2.3/video-js.css" rel="stylesheet">
	<script src="https://vjs.zencdn.net/7.2.3/video.js"></script>
	<script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.14.1/videojs-contrib-hls.js"></script>-->
	<meta name="facebook-domain-verification" content="hdl6idrwafxl7aq3a059kn0wrcaxif" />
	<!-- Facebook Pixel Code -->
<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '876141596658531');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=876141596658531&ev=PageView&noscript=1"
	/>
</noscript>
<!-- End Facebook Pixel Code -->
</head>

<?php $whiteNav = false;
if ( isset($args) && array_key_exists('white_nav', $args) && bp_is_blog_page() ) {
	$whiteNav = true;
} ?>
<body <?php body_class(); ?>>
<div id="search-bar" class="w-100<?php echo $whiteNav ? '' : ' bg-black'; ?>">
	<form class="d-flex justify-content-center py-3 px-4" action="<?php echo home_url(); ?>/" method="get">
		<input type="text" placeholder="search" name="s" id="search" value="<?php the_search_query(); ?>" />
		<button type="submit" value="search" alt="Search" />
			<?php include get_template_directory() . '/img/search.svg'; ?>
		</button>
	</form>
</div>
<div id="page" class="site">

	<header id="masthead" class="site-header<?php echo $whiteNav ? '' : ' bg-black'; ?>">

	  <div class="d-flex container container-wide">
		  <!-- Begin Logo -->

			  <div class="logoarea d-flex align-items-center">
		      <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" aria-label="The Unsealed Logo">
						<?php include get_template_directory() . '/img/unsealed_logo.svg'; ?>
					</a>

			  </div>
	      <!-- End Logo -->

			 	<!-- Begin Menu -->

	 			<div id="bs4navbartop" class="nav-container collapse">

					<ul id="menu-main-menu" class="nav-ul align-items-center">
						<?php
							$wpMenu = wp_get_nav_menu_items('main-menu');
							$menu = buildTree($wpMenu);
							foreach( $menu as $item ){
								create_menu($item);
							}
							?>
							<li class="menu-search ml-2">
								<button aria-label="menu-search">
									<?php include get_template_directory() . '/img/search.svg'; ?>
								</button>
							</li>
					</ul>
					<?php global $wp; ?>
					<div class="login flex align-items-center tablet pt-5 pb-3">
						<div class="btn-link mr-4">
							<?php
							wp_loginout(home_url( $wp->request )); ?>
						</div>
						<?php
						if ( pmpro_hasMembershipLevel() ) {
							echo unsealed_btn('Account', home_url('account'), 'small');
						} else {
							echo unsealed_btn('Sign Up', home_url('sign-up'), 'small');
						} ?>
					</div>
	      </div>
				<?php if (is_user_logged_in()):?>
					<?php global $user_id; ?>
					<div class="ml-auto d-flex align-items-center item-icons-button-wrapper mr-3">
						<?php
						$notifications = bp_notifications_get_notifications_for_user( bp_loggedin_user_id(), 'object' );
						$notification_count = 0;
						if (is_array($notifications) ) {
							foreach( $notifications as $notification ) {
								$notification_count = $notification_count + $notification->total_count;
							}
						} ?>
						<a href="<?php echo bp_loggedin_user_domain() . 'notifications/'; ?>" class="header-link-notification position-relative mr-3" data-toggle="tooltip" data-placement="top" title="Notifications"><i class="fa fa-bell"></i>
							<?php if ( $notification_count > 0 ) { ?>
								<span><?php echo $notification_count; ?></span>
							<?php } ?>
						</a>
						<?php
						if ( bp_is_active( 'messages' ) ) {
							$unread_messages = messages_get_unread_count( bp_loggedin_user_id() ); ?>
							<a href="<?php echo bp_loggedin_user_domain() . 'messages/'; ?>" class="header-link-message position-relative mr-1" data-toggle="tooltip" data-placement="top" title="Messages"><i class="fa fa-envelope"></i>
								<?php if ( $unread_messages > 0 ) { ?>
									<span><?php echo $unread_messages; ?></span>
								<?php } ?>
							</a>
						<?php
						} ?>

					</div>
				<?php endif; ?>

				<div class="login d-flex align-items-center">
					<?php
					if ( pmpro_hasMembershipLevel() ) {?>
					<?php
						$user = new BP_Core_User( bp_loggedin_user_id() );
						$profile_link = bp_loggedin_user_domain() . 'profile/';
						?>
						<div class="btn-link mr-4 desktop">
							<a href="<?php echo esc_url( wp_logout_url('login') ); ?>" data-toggle="tooltip" class="header-avatar" title="Log out">
								<i class="fa fa-sign-out"></i>
							</a>
						</div>
						<a href="<?=$profile_link;?>" class="header-avatar">
							<?=$user->avatar;?>
						</a>
					<?php
					} else {
						$profile_link = bp_loggedin_user_domain() . 'profile/'; ?>
						<div class="btn-link mr-4">
							<?php
							wp_loginout(home_url( $wp->request )); ?>
						</div>
						<a class="d-md-none" href="<?php echo home_url('sign-up'); ?>">Sign Up</a>
						<?php
						echo unsealed_btn('Sign Up', home_url('sign-up'), 'small d-none d-md-flex');
					} ?>
				</div>


				<button class="navbar-toggler first-button collapsed main-mob-menu" type="button" data-toggle="collapse" data-target="#bs4navbar,#bs4navbartop" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
					<div class="menu-ham"><span></span><span></span><span></span></div>
				</button>


        <!-- End Menu -->

    </div>

	</header><!-- #masthead -->

	<?php
	if ( bp_is_groups_component() && !pmpro_hasMembershipLevel() ) { ?>
		<div id="announcement" class="content bg-black p-2 sticky-top">
			<div class="row justify-content-center align-items-center group-announcement">
				<p class="mb-0">To post a letter or write someone back join our community!</p>
				<div>
					<a class="teal teal-btn font-weight-bold ml-2" href="<?php echo pmpro_url("checkout", '?redirect_to=' . bp_get_group_permalink(), "https"); ?>">Sign up</a> or <a class="teal teal-btn font-weight-bold" href="<?php echo pmpro_url("login", '?redirect_to=' . bp_get_group_permalink(), "https"); ?>">Log in</a>
				</div>
			</div>
		</div>
	<?php
	} ?>

	<div id="content" class="site-content">
