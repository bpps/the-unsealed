<?php
/**
 * The template for displaying the footer
 */
?>

	</div><!-- #content -->

	<?php echo footer_signup(); ?>
	<?php
	$wordCount = get_field('form_validation_word_count', 'option') ?? 100;
	$wordMax = get_field('form_validation_word_max', 'option') ?? null;
	wp_localize_script( 'unsealed-scripts', 'validationWordCount', [ 'min' => $wordCount, 'max' => $wordMax ] );

	?>
	<footer id="colophon" class="site-footer bg-black">
		<div class="site-info container d-flex justify-content-between align-items-center flex-column flex-md-row">
			<div class="social-links d-flex py-3 pt-4 py-md-5">
				<a href="<?php the_field('facebook_link', 'options'); ?>" target="_blank" aria-label="facebook link">
					<?php include get_template_directory() . '/img/facebook.svg'; ?>
				</a>
				<a href="<?php the_field('twitter_link', 'options'); ?>" target="_blank" aria-label="twitter link">
					<?php include get_template_directory() . '/img/twitter.svg'; ?>
				</a>
				<a href="<?php the_field('instagram_link', 'options'); ?>" target="_blank" aria-label="instagram link">
					<?php include get_template_directory() . '/img/instagram.svg'; ?>
				</a>
			</div>
			<div class="copyright py-3 py-md-5">
				Copyright ©<?php echo date('Y'); ?> Lauren Brill Media LLC, All Rights Reserved
			</div>
		</div><!-- .site-info -->
		<?php echo email_form(); ?>
		<?php
		$footer_menu = wp_get_nav_menu_items('footer');
		if ( $footer_menu ) { ?>
			<div class="container">
				<div class="container d-flex justify-content-between align-items-center flex-column flex-md-row pb-4">
					<div class="small">
						A <a class="teal" href="https://fullmetalworkshop.com" target="_blank">Full Metal Workshop</a> Production
					</div>
					<ul id="menu-footer-menu" class="mb-0 col-auto nav-ul align-items-center small row">
						<?php
						$menu = buildTree($footer_menu);
						foreach( $menu as $item ){
							create_menu($item);
						} ?>
					</ul>
				</div>
			</div>
		<?php
		} ?>
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php

if ($GLOBALS['content_locked'] != null) {
	signup_lock_popup('overlay');
}

if ( $GLOBALS['content_locked'] == null && !$GLOBALS['blocked_inline'] && !pmpro_hasMembershipLevel() && !isset($_COOKIE['subscribe-fortune']) && !is_page('login') ) {
	echo subscribe_popup_form();
}

$show_intro_popup = !pmpro_hasMembershipLevel() && !isset($_COOKIE['non-member-video']) && !is_page('login') && !is_page('sign-up') && !isset($_SESSION['nonMemberVideo']);

if ( $show_intro_popup && $GLOBALS['content_locked'] == null && !$GLOBALS['blocked_inline'] ) {
	$_SESSION['nonMemberVideo'] = 1;
	$timeout = get_field('intro_video_timeout', 'option') ? get_field('intro_video_timeout', 'option') * 1000 : 10000;
	$popup_placeholder = get_field('popup_video_placeholder_image', 'option');
	if ( $popup_video = get_field('popup_video', 'option') ) {
		$popup_video = $popup_video['url']; ?>
		<div class="modal fade video-modal" data-timeout="<?php echo $timeout; ?>" data-autoplay="false" data-video-src="<?php echo $popup_video; ?>" data-video-type="upload" id="non-member-video-popup" tabindex="-1" role="dialog" aria-labelledby="non-member-video-label" aria-hidden="true">
			<div class="sr-only" id="non-member-video-label">Intro video popup</div>
			<div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<div type="button" class="home-play-button">
							<button class="play-button" aria-label="play-button"></button>
						</div>
						<div class="embed-responsive embed-responsive-16by9">
							<video controls class="embed-responsive-item video" poster="<?php echo $popup_placeholder['sizes']['medium-large']; ?>">
								<source type="video/mp4">
							</video>
							<iframe class="video-iframe" src="" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
	} elseif ( $popup_placeholder ) { ?>
		<div class="modal fade image-modal" data-timeout="<?php echo $timeout; ?>" id="non-member-video-popup" tabindex="-1" role="dialog" aria-labelledby="non-member-video-label" aria-hidden="true">
			<div class="sr-only" id="non-member-video-label">Promo</div>
			<div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
				<div class="modal-content">
					<?php
					$popup_link = get_field('popup_link', 'option');
					if ( $popup_link ) { ?>
						<a href="<?php echo $popup_link['url']; ?>" target="<?php echo $popup_link['target']; ?>">
					<?php
					} ?>
					<div class="modal-body">
						<div class="post-image">
							<img src="<?php echo $popup_placeholder['sizes']['medium-large']; ?>"/>
						</div>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php
					if ( $popup_link ) { ?>
						</a>
					<?php
					} ?>
				</div>
			</div>
		</div>
	<?php
	}
} ?>

<?php wp_footer(); ?>
</body>
</html>
