<?php
//-----------------------------------------------------
// Setup
//-----------------------------------------------------
if ( ! function_exists( 'unsealed_setup' ) ) :

	function unsealed_setup() {
		if ( ! isset( $content_width ) ) {
			$content_width = 730; /* pixels */
		}
		load_theme_textdomain( 'unsealed', get_template_directory() . '/languages' );

		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
		) );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'wp-block-styles' );
		add_theme_support( 'align-wide' );

		remove_image_size( 'medium_large' );
		remove_image_size( '1536x1536' );
		remove_image_size( '2048x2048' );

		add_image_size( 'small', 300, 200 );
		add_image_size( 'small-medium', 450, 300 );
		add_image_size( 'medium', 600, 400 );
		add_image_size( 'medium-large', 950, 600 );
		add_image_size( 'large', 1200, 900 );

		register_nav_menus( array(
		'extramenu' => 'Main Menu',
		) );

	}
	endif;
	add_action( 'after_setup_theme', 'unsealed_setup' );

//-----------------------------------------------------
// Scripts & Styles
//-----------------------------------------------------
if ( ! function_exists( 'unsealed_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function unsealed_scripts() {
		// Get the theme data.
		$manifest = json_decode(file_get_contents('dist/assets.json', true));
		$main = $manifest->main;
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );
		wp_enqueue_media();

		wp_enqueue_style( 'jcrop-styles', 'https://unpkg.com/jcrop/dist/jcrop.css' );

		wp_enqueue_style( 'unsealed-styles', get_template_directory_uri() . $main->css, array(), $theme_version );

		wp_enqueue_script( 'unsealed-scripts', get_template_directory_uri() . $main->js, array(), $theme_version, true );

		if ( is_page_template( 'templates/community-letters.php' ) || is_post_type_archive('community-voices') || is_singular('community-voices') ) {
			wp_enqueue_script( 'unsealed-community', get_template_directory_uri() . $manifest->community->js, array(), $theme_version, true );
		}

		if ( bp_is_current_component('contest-entries') ) {
			wp_enqueue_script( 'bp-contest-entries', get_template_directory_uri() . $manifest->contestsjs->js, array(), $theme_version, false );
		}

		if ( is_page_template( 'templates/ask-the-unsealed.php' ) || is_post_type_archive('ask-the-unsealed') || is_singular('ask-the-unsealed') ) {
			wp_enqueue_script( 'unsealed-ask-unsealed', get_template_directory_uri() . $manifest->askunsealed->js, array(), $theme_version, true );
		}

		if ( is_page_template( 'templates/letters.php' ) ) {
			wp_enqueue_script( 'unsealed-letters', get_template_directory_uri() . $manifest->letters->js, array(), $theme_version, true );
		}

		if ( is_page_template( 'templates/contests.php' ) ) {
			wp_enqueue_script( 'unsealed-contests', get_template_directory_uri() . $manifest->contests->js, array(), $theme_version, true );
		}

		if ( is_page_template( 'templates/contest-submissions.php' ) || is_post_type_archive('contest-submissions') || is_singular('contest-submissions') ) {
			wp_enqueue_script( 'unsealed-entries', get_template_directory_uri() . $manifest->entries->js, array(), $theme_version, true );
		}

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'unsealed_scripts' ).

add_action( 'wp_enqueue_scripts', 'unsealed_scripts' );


function strip_vc_tags ($content) {
	$shortcode_tags = array('VC_COLUMN_INNTER', 'VC_COLUMN');
	$values = array_values( $shortcode_tags );
	$exclude_codes  = implode( '|', $values );

	// strip all shortcodes but keep content
	// $content = preg_replace("~(?:\[/?)[^/\]]+/?\]~s", '', $content);

	// strip all shortcodes except $exclude_codes and keep all content
	$content = preg_replace( "~(?:\[/?)(?!(?:$exclude_codes))[^/\]]+/?\]~s", '', $content );
	return $content;
}

//-----------------------------------------------------
// Require
//-----------------------------------------------------
require_once get_template_directory() . '/inc/bootstrap/wp_bootstrap_pagination.php';
require_once get_template_directory() . '/inc/bootstrap/wp_bootstrap_navwalker.php';
require_once get_template_directory() . '/inc/acf_functions.php';
require_once get_template_directory() . '/inc/acf_blocks.php';
// require_once get_template_directory() . '/inc/subscription_functions.php';
require_once get_template_directory() . '/inc/unsealed_functions.php';
require_once get_template_directory() . '/inc/unsealed_taxonomies.php';
require_once get_template_directory() . '/inc/unsealed_post_types.php';
require_once get_template_directory() . '/inc/unsealed_shortcodes.php';
require_once get_template_directory() . '/inc/subscription_functions.php';
//-----------------------------------------------------
// Modules
//-----------------------------------------------------
require_once get_template_directory() . '/modules/home-hero.php';
require_once get_template_directory() . '/modules/featured-letters.php';
require_once get_template_directory() . '/modules/community.php';
require_once get_template_directory() . '/modules/spotlights.php';
require_once get_template_directory() . '/modules/featured-videos.php';
require_once get_template_directory() . '/modules/about-lauren.php';
require_once get_template_directory() . '/modules/as-seen-on.php';
require_once get_template_directory() . '/modules/footer-signup.php';
require_once get_template_directory() . '/modules/unsealed-button.php';
require_once get_template_directory() . '/modules/email-form.php';
require_once get_template_directory() . '/modules/subscribe-popup.php';
require_once get_template_directory() . '/modules/more-stories.php';
require_once get_template_directory() . '/modules/social-platform-callout.php';

function letters_rewrite_rule() {
  // add_rewrite_rule('^community-voices/(.+)?', 'index.php?pagename=community-voices', 'top');
	add_rewrite_rule('^contest-submissions/(.+)?', 'index.php?pagename=contest-submissions', 'top');
	add_rewrite_rule('^letters/(.+)?', 'index.php?pagename=letters', 'top');
	flush_rewrite_rules();
}

add_action('init', 'letters_rewrite_rule');

if ( !is_admin() ) {

	function wpb_search_filter($query) {
		if ($query->is_search) {
			$query->set('post_type', ['post', 'community-voices', 'ask-the-unsealed', 'contest-submissions']);
		}
		return $query;
	}
	add_filter('pre_get_posts','wpb_search_filter');

	function relevanssi_add_custom_fields( $fields ) {
		$fields = array();
		$fields[] = 'author_name';
		$fields[] = 'member';
		$fields = implode( ',', $fields );
		return $fields;
	}

	add_filter( 'option_relevanssi_index_fields', 'relevanssi_add_custom_fields' );

}

if ( !function_exists('log_it') ) {
	function log_it( $message ) {
	 if( WP_DEBUG === true ){
	   if( is_array( $message ) || is_object( $message ) ){
	     error_log( print_r( $message, true ) );
	   } else {
	     error_log( $message );
	   }
	 }
	}
}

function cf_search_join( $join ) {
	global $wpdb;

	if ( is_search() ) {
			$join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
			$join .=' LEFT JOIN '.$wpdb->users. ' ON '. $wpdb->postmeta . '.meta_key = "member" AND ' . $wpdb->postmeta . '.meta_value = '.$wpdb->users . '.id';
	}

	return $join;
}

add_filter('posts_join', 'cf_search_join' );

/**
* Modify the search query with posts_where
*
* http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
*/
function cf_search_where( $where ) {
	global $pagenow, $wpdb;

	if ( is_search() ) {
			$where = preg_replace(
					"/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
					"((".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1) OR (".$wpdb->users.".display_name LIKE $1)  OR (".$wpdb->users.".user_login LIKE $1)) AND ".$wpdb->posts.".post_status = 'publish' ", $where );
	}

	return $where;
}

add_filter( 'posts_where', 'cf_search_where' );

/**
* Prevent duplicates
*
* http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
*/
function cf_search_distinct( $where ) {
	global $wpdb;

	if ( is_search() ) {
			return "DISTINCT";
	}

	return $where;
}

add_filter( 'posts_distinct', 'cf_search_distinct' );
