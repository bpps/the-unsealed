<?php

function update_excerpt() {
  $args = [
    'post_type' => 'post',
    'posts_per_page' => -1
  ];

  $letters_query = new WP_Query( $args );
  if ( $letters_query->have_posts() ) :
    while ( $letters_query->have_posts() ) : $letters_query->the_post();
      if ( $excerpt = get_field('Letters Page Summary') ) {
        $the_post = array(
          'ID'           => get_the_ID(),//the ID of the Post
          'post_excerpt' => $excerpt
        );
        wp_update_post( $the_post );
        delete_field('Letters Page Summary', get_the_ID());
      }

    // if shortcode has attributes
    // if (!empty($shortcode_atts)) {
    // foreach($shortcode_atts as $att) {
    //   preg_match('/id="(\d+)"/', $matches[3][$att], $book_id);
    //
    //   // fill the id into main array
    //   $book_ids[] = $book_id[1];
    // }

    endwhile;
    wp_reset_postdata();
  endif;
}

function block_content_inline( $atts = false ) {
  ob_start();
  if ( isset($atts['position']) && $atts['position'] == 'start' ) { ?>
    <div class="inline-lock-wrapper">
      <?php 
      $title = $atts['title'] ? $atts['title'] : 'Premium Content';
      $content = $atts['content'] ? $atts['content'] : 'To see this free content login or subscribe to our newsletter by putting your email here.';
      signup_lock_popup('content', $title, $content);
  } else { ?>
    </div>
  <?php
  }
  return ob_get_clean();
}
add_shortcode( 'block-content', 'block_content_inline' );