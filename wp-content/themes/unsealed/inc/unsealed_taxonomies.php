<?php
function register_custom_taxonomies() {

	$labels = [
		'name'                       => _x( 'Topics', 'taxonomy general name', 'unsealed' ),
		'singular_name'              => _x( 'Topic', 'taxonomy singular name', 'unsealed' ),
		'search_items'               => __( 'Search Topics', 'unsealed' ),
		'popular_items'              => __( 'Popular Topics', 'unsealed' ),
		'all_items'                  => __( 'All Topics', 'unsealed' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Topic', 'unsealed' ),
		'update_item'                => __( 'Update Topic', 'unsealed' ),
		'add_new_item'               => __( 'Add New Topic', 'unsealed' ),
		'new_item_name'              => __( 'New Topic Name', 'unsealed' ),
		'separate_items_with_commas' => __( 'Separate topics with commas', 'unsealed' ),
		'add_or_remove_items'        => __( 'Add or remove topics', 'unsealed' ),
		'choose_from_most_used'      => __( 'Choose from the most used topics', 'unsealed' ),
		'not_found'                  => __( 'No topics found.', 'unsealed' ),
		'menu_name'                  => __( 'Topics', 'unsealed' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'has_archive'						=> false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'show_in_rest'					=> true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => false,
	];

	register_taxonomy( 'topic', ['ask-the-unsealed', 'community-voices', 'post', 'event'], $args );
	// register_taxonomy_for_object_type( 'member-type', ['team'] );

	$labels = [
		'name'                       => _x( 'Advisors', 'taxonomy general name', 'unsealed' ),
		'singular_name'              => _x( 'Advisor', 'taxonomy singular name', 'unsealed' ),
		'search_items'               => __( 'Search Advisors', 'unsealed' ),
		'popular_items'              => __( 'Popular Advisors', 'unsealed' ),
		'all_items'                  => __( 'All Advisors', 'unsealed' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Advisor', 'unsealed' ),
		'update_item'                => __( 'Update Advisor', 'unsealed' ),
		'add_new_item'               => __( 'Add New Advisor', 'unsealed' ),
		'new_item_name'              => __( 'New Advisor Name', 'unsealed' ),
		'separate_items_with_commas' => __( 'Separate topics with commas', 'unsealed' ),
		'add_or_remove_items'        => __( 'Add or remove topics', 'unsealed' ),
		'choose_from_most_used'      => __( 'Choose from the most used topics', 'unsealed' ),
		'not_found'                  => __( 'No topics found.', 'unsealed' ),
		'menu_name'                  => __( 'Advisors', 'unsealed' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'has_archive'						=> false,
		'show_in_rest'					=> true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => false,
	];

	register_taxonomy( 'advisor', ['ask-the-unsealed', 'post', 'event'], $args );

}

add_action('init', 'register_custom_taxonomies', 0);
