<?php
function set_custom_subscription_amount ($order) {
  $order->InitialPayment = 3;
  $order->PaymentAmount = 3;
  return $order;
  // $sub_id = $data['subscription_id'];
  // log_it($data);
  // log_it($data['post_data']['custom_membership_cost_' . $sub_id]);
  // if ( isset($data['post_data']['custom_membership_cost_' . $sub_id] ) ) {
  //   $sub_price = $data['post_data']['custom_membership_cost_' . $sub_id];
  //   $data['price'] = $sub_price;
  //   $data['initial_price'] = $sub_price;
  //   if ( $data['auto_renew'] == 1 ) {
  //     $data['recurring_price'] = $sub_price;
  //   }
  // }
  // log_it('NEW DATA');
  // log_it($data);
  // return $data;
}
// add_filter('rcp_subscription_data', 'set_custom_subscription_amount', 10);

// add_filter( 'pmpro_checkout_order', 'set_custom_subscription_amount', 10 );

function set_account_page_html ($html) {
  $html = '';
  return $html;
}
// add_filter( 'pmpro_pages_shortcode_account', 'set_account_page_html', 10 );

function unsealed_get_level_fields( $level_id ) {
	$defaults = array(
		'variable_pricing'      => 0,
		'min_price'             => '',
    'price_options'         => [],
    'selected_option_price' => ''
	);
	$settings = get_option( "unsealed_{$level_id}", $defaults );
	$settings = array_merge( $defaults, $settings );  // make sure newly added settings have defaults appended

	return $settings;
}

function unsealed_admin_level_pricing_options () {
	global $pmpro_currency_symbol;
	$level_id = intval( $_REQUEST['edit'] );
	if ( $level_id > 0 ) {
		$level_fields          = unsealed_get_level_fields( $level_id );
		$variable_pricing      = $level_fields['variable_pricing'];
		$min_price             = $level_fields['min_price'] ? $level_fields['min_price'] : 0;
    $price_options         = $level_fields['price_options'];
    $selected_option_price = $level_fields['selected_option_price'];
	} else {
		$variable_pricing       = 0;
		$min_price              = '';
    $price_options          = '';
    $selected_option_price  = '';
	} ?>
  <h3 class="topborder"><?php esc_html_e( 'Pricing Options', 'unsealed-pricing-options' ); ?></h3>
  <p><?php esc_html_e( 'Set multiple price options separated by commas ie. 5, 10, 20', 'unsealed-pricing-options' ); ?></p>

  <table>
    <tbody class="form-table">
    	<tr>
    		<th scope="row" valign="top"><label for="unsealed-pricing-options"><?php _e( 'Price Options:', 'unsealed-price-options' ); ?></label></th>
    		<td>
    			<input type="text" name="price_options" id="unsealed-pricing-options" value="<?php echo implode(',', $price_options); ?>" />
    		</td>
    	</tr>
      <tr>
    		<th scope="row" valign="top"><label for="unsealed-starting-option"><?php _e( 'Selected Option:', 'unsealed-price-options' ); ?></label></th>
    		<td>
    			<input type="text" name="selected_option_price" id="unsealed-starting-option" value="<?php echo esc_attr($selected_option_price); ?>" />
    		</td>
    	</tr>
    </tbody>
  </table>

  <h3 class="topborder"><?php esc_html_e( 'Variable Pricing', 'unsealed-variable-pricing' ); ?></h3>
  <p><?php esc_html_e( 'If variable pricing is enabled, users will be able to set their own price at checkout. That price will override any initial payment and billing amount values you set on this level. You can set the minimum amount for this subscription.', 'unsealed-variable-pricing' ); ?></p>

  <table>
    <tbody class="form-table">
    	<tr>
    		<th scope="row" valign="top"><label for="unsealed_variable_pricing"><?php _e( 'Enable:', 'unsealed-variable-pricing' ); ?></label></th>
    		<td>
    			<input type="checkbox" name="variable_pricing" id="unsealed_variable_pricing" value="1" <?php checked( $variable_pricing, '1' ); ?> /> <label for="unsealed_variable_pricing"><?php _e( 'Enable Variable Pricing', 'unsealed-variable-pricing' ); ?></label>
    		</td>
    	</tr>
    	<tr class="unsealed_setting">
    		<th scope="row" valign="top"><label for="unsealed_min_price"><?php _e( 'Min Price:', 'unsealed-variable-pricing' ); ?></label></th>
    		<td>
    			<?php echo esc_html( $pmpro_currency_symbol ); ?><input type="text" name="min_price" id="unsealed_min_price" value="<?php echo esc_attr( $min_price ); ?>" />
    		</td>
    	</tr>
    </tbody>
  </table>
<?php
}

add_action( 'pmpro_membership_level_after_other_settings', 'unsealed_admin_level_pricing_options' );

function unsealed_save_subscription_level( $level_id ) {
	$variable_pricing = intval( $_REQUEST['variable_pricing'] );
	$min_price        = preg_replace( '[^0-9\.]', '', $_REQUEST['min_price'] );
	$price_options  = explode(',', $_REQUEST['price_options']);
  $selected_option_price  = intval( $_REQUEST['selected_option_price'] );
  array_walk($price_options, 'trim_value');
	update_option(
		'unsealed_' . $level_id, array(
			'variable_pricing'       => $variable_pricing,
			'min_price'              => $min_price,
			'price_options'          => $price_options,
      'selected_option_price'  => $selected_option_price
		)
	);
}

add_action( 'pmpro_save_membership_level', 'unsealed_save_subscription_level' );

function trim_value(&$value) {
    $value = trim($value);
}

function unsealed_display_subscription_levels() {
  global $pmpro_error_fields, $pmpro_msg, $pmpro_msgt;
  $pmpro_levels = pmpro_getAllLevels(false, true);
  $selected_level_id = isset($_REQUEST['level']) ? $_REQUEST['level'] : null;
  if ( isset($_REQUEST['one-time']) ) {
    $selected_level_id = 3;
  }
  $header = get_field('header_subscription');
  $content = get_field('content_subscription');

  $should_show = true;
  if ( isset($_REQUEST['level']) || isset($_REQUEST['one-time']) ) {
    $should_show = false;
  }
  if ( count($pmpro_error_fields) > 0 ) {
    $should_show = false;
  } ?>
  <section data-step-id="1" data-step="Subscription" class="checkout-step pt-3 pb-5<?php echo $should_show ? ' visible' : ''; ?>" id="checkout-step-1">
    <?php echo unsealed_checkout_section_header($header, $content); ?>
    <div class="d-flex justify-content-center container">
      <?php
      foreach ( $pmpro_levels as $level ) {
        if ( !isset($_REQUEST['one-time']) && $level->cycle_number == 0 ) continue; ?>
        <div class="level-btn-wrapper px-3">
          <div class="level-button btn-unsealed small px-3">
            <label class="script-font" for="level-option-<?php echo $level->id; ?>">
              <?php echo $level->name; ?>
            </label>
            <input class="level-radio" type="radio" data-level-id="<?php echo $level->id; ?>" onchange="focus_level_options(this)" id="level-option-<?php echo $level->id; ?>" name="level" <?php if ( $selected_level_id == $level->id ) { ?>checked <?php } ?>value="<?php echo $level->id; ?>"/>
            <div class="btn-background-text" aria-hidden="true">
              <?php for($y = 0; $y < 3; $y++){
      					for($x = 0; $x < 10; $x++){
      						echo $level->name . '  ';
      					}
      					echo '<br />';
      				} ?>
            </div>
          </div>
        </div>
      <?php
      } ?>
    </div>
  </section>
  <script>
    function focus_level_options (level) {
      var levelId = jQuery(level).data('level-id')
      jQuery('.pricing-description').hide()
      jQuery('.pricing-description-' + levelId).show()
      jQuery('.unsealed-level').hide()
      jQuery('.unsealed-level-' + levelId).show()
      checkoutNextStep(2)
    }
  </script>
<?php
}

// add_action( 'pmpro_checkout_after_level_cost', 'unsealed_display_subscription_levels' );

function unsealed_display_level_options () {
  global $pmpro_level, $pmpro_review, $pmpro_currencies, $pmpro_currency, $pmpro_currency_symbol;
  global $skip_account_fields, $pmpro_error_fields;

  $pmpro_levels = pmpro_getAllLevels(false, true);
  $first_level_id = null;

  foreach ( $pmpro_levels as $key => $level ) {
    if ( $key == 0 ) {
      $first_level_id = $level->id;
    }
    if ( isset($_REQUEST['one-time']) && $level->cycle_number == 0 ) {
      $first_level_id = $level->id;
    }
  }
  $selected_level_id = isset($_REQUEST['level']) ? $_REQUEST['level'] : $first_level_id;
  $header = get_field('header_pricing');

  if ( isset($_REQUEST['one-time']) ) {
    $selected_level_id = 3;
  }

  $should_show = false;
  if ( isset($_REQUEST['level']) || isset($_REQUEST['one-time']) ) {
    $should_show = true;
    if ( count($pmpro_error_fields) > 0 ) {
      $should_show = false;
    }
  } ?>
  <section data-step-id="2" data-step="Pricing" class="checkout-step pt-3 pb-5<?php echo $should_show ? ' visible' : ''; ?>" id="checkout-step-2">
    <button type="button" class="px-0 d-flex align-items-center subscription-go-back btn teal" onClick="checkoutNextStep(1)"><i class="fa fa-chevron-left"></i>Levels</button>
    <?php
    foreach( $pmpro_levels as $level ) { ?>
      <div style="display: <?php echo $selected_level_id == $level->id ? 'block' : 'none'; ?>" class="pricing-description pricing-description-<?php echo $level->id; ?>">
        <?php
        $content = $pmpro_levels[$level->id]->description != '' ? $pmpro_levels[$level->id]->description : get_field('content_pricing');
        echo unsealed_checkout_section_header($header, $content); ?>
      </div>
    <?php
    }
    foreach ( $pmpro_levels as $level ) {
      $level_id = $level->id;
    	// get variable pricing info
    	$fields = unsealed_get_level_fields( $level_id );

    	// no variable pricing? just return
    	if ( empty( $fields ) || ( empty( $fields['variable_pricing'] ) && empty( $fields['price_options'] ) ) ) {
    		return;
    	}

    	// okay, now we're showing the form
      $variable_pricing      = $fields['variable_pricing'];
    	$min_price             = $fields['min_price'];
    	$price_options         = $fields['price_options'];
    	$selected_option_price = $fields['selected_option_price'];
      $custom_selected = false;

    	if ( isset( $_REQUEST['price'] ) && ( isset( $_REQUEST['level'] ) && $level_id == $_REQUEST['level'] )) {
    		$price = preg_replace( '[^0-9\.]', '', $_REQUEST['price'] );
        if ( $selected_option_price ) {
          $selected_option_price = preg_replace( '[^0-9\.]', '', $_REQUEST['price'] );
        }
    	} else {
    		$price = $selected_option_price ? preg_replace( '[^0-9\.]', '', $selected_option_price ) : '';
        if ( $selected_option_price ) {
          $selected_option_price = preg_replace( '[^0-9\.]', '', $price );
        }
    	}

    	$price_text_description = __( 'Subscription Options', 'unsealed-variable-pricing' );

    	/**
    	 * @filter pmpropvp_checkout_price_description - Filter to modify the variable price description text
    	 * @param string $price_text_description
    	 */
    	$price_text_description = apply_filters( 'pmpropvp_checkout_price_description', $price_text_description );

    	if ( empty( $pmpro_currencies[$pmpro_currency]['position'] ) || $pmpro_currencies[$pmpro_currency]['position'] == 'left' ) {
    		$price_text = sprintf(
    			__( 'Custom %s contribution:', 'unsealed-variable-pricing' ),
    			strtolower($level->name)
    		);
    	} else {
    		$price_text = __( 'Your contribution:', 'unsealed-variable-pricing' );
    	}

    	/**
    	 * @filter unsealed_checkout_price_input_label - Filter to modify the label for the Variable Price input box on the checkout page
    	 * @param string $price_text
    	 */
    	$price_text = apply_filters( 'unsealed_checkout_price_input_label', $price_text ); ?>
      <div class="unsealed-level unsealed-level-<?php echo $level_id; ?>" style="display: <?php echo $selected_level_id == $level_id ? 'block' : 'none'; ?>">
        <?php
        if ( $selected_option_price ) {
          $cycle = $level->cycle_period == 0 ? $level->name : $level->cycle_period; ?>
          <div class="d-flex w-100 justify-content-center container align-items-center">
            <div class="col-md-6 d-flex flex-column justify-content-center align-items-center">
              <p class="h1 default-level-price">$<?php echo $selected_option_price; ?></p>
              <p><?php echo $cycle; ?> contribution</p>
              <div class="custom-contribution flex-grow-0" id="custom-contribution-<?php echo $level_id; ?>">
                <div class="custom-contribution-options d-flex justify-content-center flex-wrap">
                  <?php
                  $price_option_selected = false;
                  if ( $price_options ) {
                    foreach ( $price_options as $price_option ) {
                      if ( $selected_option_price && $selected_option_price == $price_option ) {
                        $price_option_selected = true;
                      } ?>
                      <div class="position-relative">
                        <input
                          type="radio"
                          class="price-option-select"
                          data-min-price="<?php echo $min_price; ?>"
                          id="price-option-<?php echo $price_option; ?>-level-<?php echo $level_id; ?>"
                          name="level-<?php echo $level_id; ?>-price"
                          <?php if ( $selected_option_price && $selected_option_price == $price_option ) { ?>
                            checked
                          <?php } ?>
                          value="<?php echo $price_option; ?>"
                           />
                        <label for="price-option-<?php echo $price_option; ?>-level-<?php echo $level_id; ?>">
                          $<?php echo $price_option; ?>
                        </label>
                      </div>
                    <?php
                    }
                  }
                  if ( $variable_pricing ) { ?>
                    <div class="variable-option">
                      <?php
                      if ( $price_options ) { ?>
                        <label for="price-option-custom">
                        <div class="position-relative">
                          <input
                            type="radio" id="price-option-custom-level-<?php echo $level_id; ?>"
                            name="level-<?php echo $level_id; ?>-price"
                            value="0"
                            <?php if ( !$price_option_selected ) { ?>
                              checked
                            <?php } ?>/>
                        </div>
                      <?php
                      } ?>
                      <p class="text-center"><?php echo esc_html( $price_text ); ?></p>
                      <div class="mt-1 d-flex justify-content-center align-items-center">
                        <span class="mr-2">$</span>
                        <input
                          class="custom-price-input"
                          onchange="focus_level_price(this, <?php echo $min_price; ?>)"
                          onfocus="focus_custom_option(this)"
                          type="number"
                          <?php if ( $min_price ) { ?>
                            min="<?php echo $min_price; ?>"
                          <?php } ?>
                          id="price-<?php echo $level_id; ?>"
                          name="level-<?php echo $level_id; ?>-custom_price" size="10" value="<?php echo !$price_option_selected ? $selected_option_price : ''; ?>" <?php if( $pmpro_review ) { ?> readonly <?php } ?>/>
                        <?php
                        if ( $min_price ) { ?>
                          <sub class="ml-2">(<?php echo pmpro_formatPrice($min_price); ?> minimum)</sub>
                        <?php
                        } ?>
                      </div>
                      <?php
                      if ( $price_options ) { ?>
                        </label>
                      <?php
                      } ?>
                    </div>
                    <div class="pricing-alert alert py-0 px-0" style="color: red; display: none;"><sub>The minimum contribution is $<?php echo $min_price; ?></sub></div>
                  <?php
                  } ?>
                </div>
              </div>
            </div>
          </div>
        <?php
        } ?>
        <div class="d-flex row justify-content-center mt-4">
          <button type="button" class="btn submit-btn" onClick="pricing_continue(<?php echo '3, ' . $level_id . ', ' . $min_price; ?>)">
            <?php echo unsealed_btn('Continue', false, 'small'); ?>
          </button>
        </div>
        <span id="unsealed-level-<?php echo $level_id; ?>-warning" class="pmpro_message pmpro_alert" style="display:none;"><small><?php echo $price_text_description; ?></small></span></p>
      </div> <!-- end .unsealed -->
    <?php
    } ?>
    <input id="alter-price" type="hidden" class="<?php echo pmpro_get_element_class( 'input pmpro_alter_price', 'discount_code' ); ?>" value=""/>
  </section>
  <script>
    function focus_custom_option (custom_option) {
      var optionParent = custom_option.closest('.variable-option')
      var option = optionParent.querySelector('input[type="radio"]')
      option.checked = true
    }

    jQuery('.custom-contribution-options input').change(function() {
      var minPricing = jQuery(this).data('min-price')
      focus_level_price(this, minPricing)
    });

    jQuery('.level-radio').change(function() {
      jQuery('#alter-price').val('').trigger('change')
    })

    function focus_level_price (price, minPrice) {
      jQuery('.pricing-alert').hide()
      var optionParent = price.closest('.unsealed-level')
      var defaultPrice = optionParent.querySelector('.default-level-price')
      if ( price.value < minPrice ) {
        return
      }
      if ( !jQuery(price).hasClass('custom-price-input') ) {
        jQuery('.custom-price-input').val('')
      }
      jQuery('#alter-price').val(price.value).trigger('change')
      defaultPrice.innerHTML = '$' + price.value
    }
  </script>
  <?php
  do_action("pmpro_checkout_after_level_cost");
}

function unsealed_display_account_fields () {
  global $pmpro_review, $pmpro_msg, $pmpro_msgt, $skip_account_fields, $pmpro_level, $pmpro_error_fields, $tospage;
	global $discount_code, $username, $first_name, $last_name, $password, $password2, $bfirstname, $blastname, $baddress1, $baddress2, $bcity, $bstate, $bzipcode, $bcountry, $bphone, $bemail, $bconfirmemail, $CardType, $AccountNumber, $ExpirationMonth,$ExpirationYear,$pmpro_email_field_type;
  $should_show = false;
  $account_errors = ['username','password','password2', 'bemail', 'bconfirmemail'];
  foreach ( $pmpro_error_fields as $error_field ) {
    if ( in_array($error_field, $account_errors) ) {
      $should_show = true;
    }
  }
  $header = get_field('header_account');
  $content = get_field('content_account'); ?>
  <section data-step-id="3" data-step="Account Details" class="checkout-step pt-3 pb-5<?php echo $should_show ? ' visible' : ''; ?>" id="checkout-step-3">
    <button type="button" class="px-0 d-flex align-items-center subscription-go-back btn teal" onClick="checkoutNextStep(2)"><i class="fa fa-chevron-left"></i>Pricing</button>
    <?php echo unsealed_checkout_section_header($header, $content);  ?>
		<div id="pmpro_user_fields" class="<?php echo pmpro_get_element_class( 'pmpro_checkout', 'pmpro_user_fields' ); ?> d-flex justify-content-center align-itens-center flex-column">
			<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-fields' ); ?> col-md-6 offset-md-3">
        <div id="pmpro_user_fields_errors"></div>
        <?php
        if(!$skip_account_fields && !$pmpro_review) {
      			// Get discount code from URL parameter, so if the user logs in it will keep it applied.
      			$discount_code_link = !empty( $discount_code) ? '&discount_code=' . $discount_code : ''; ?>
  				<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-username', 'pmpro_checkout-field-username' ); ?>">
  					<label for="username"><?php _e('Username', 'paid-memberships-pro' );?><span id="username-approved">✓</span></label>
  					<input id="username" autocomplete="off" name="username" type="text" class="<?php echo pmpro_get_element_class( 'input', 'username' ); ?>" size="30" value="<?php echo esc_attr($username); ?>" />
            <span id="username-taken" class="error">Username is taken</span>
  				</div> <!-- end pmpro_checkout-field-username -->

  				<!-- <div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-first_name', 'pmpro_checkout-field-first_name' ); ?>">
  					<label for="first_name"><?php _e('First name', 'paid-memberships-pro' );?></label>
  					<input id="first_name" name="first_name" type="text" class="<?php echo pmpro_get_element_class( 'input', 'first_name' ); ?>" size="30" value="<?php echo esc_attr($first_name); ?>" />
  				</div> -->

  				<?php
  					do_action('pmpro_checkout_after_username');
  				?>

  				<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-password', 'pmpro_checkout-field-password' ); ?>">
  					<label for="password"><?php _e('Password', 'paid-memberships-pro' );?></label>
  					<input id="password" minlength="8" name="password" type="password" class="<?php echo pmpro_get_element_class( 'input', 'password' ); ?>" size="30" value="<?php echo esc_attr($password); ?>" />
  				</div> <!-- end pmpro_checkout-field-password -->

  				<?php
  					$pmpro_checkout_confirm_password = apply_filters("pmpro_checkout_confirm_password", true);
  					if($pmpro_checkout_confirm_password) { ?>
  						<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-password2', 'pmpro_checkout-field-password2' ); ?>">
  							<label for="password2"><?php _e('Confirm Password', 'paid-memberships-pro' );?></label>
  							<input id="password2" name="password2" type="password" class="<?php echo pmpro_get_element_class( 'input', 'password2' ); ?>" size="30" value="<?php echo esc_attr($password2); ?>" />
  						</div> <!-- end pmpro_checkout-field-password2 -->
  					<?php } else { ?>
  						<input type="hidden" name="password2_copy" value="1" />
  					<?php }
  				?>

  				<?php
  					do_action('pmpro_checkout_after_password');
  				?>

  				<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-bemail', 'pmpro_checkout-field-bemail' ); ?>">
  					<label for="bemail"><?php _e('Email Address', 'paid-memberships-pro' );?></label>
  					<input id="bemail" name="bemail" type="<?php echo ($pmpro_email_field_type ? 'email' : 'text'); ?>" class="<?php echo pmpro_get_element_class( 'input', 'bemail' ); ?>" size="30" value="<?php echo esc_attr($bemail); ?>" />
            <span id="email-taken" class="error">It looks like you're already subscribed! <a href="<?php echo home_url('login'); ?>">Login now</a></span>
  				</div> <!-- end pmpro_checkout-field-bemail -->

  				<?php
  					$pmpro_checkout_confirm_email = apply_filters("pmpro_checkout_confirm_email", true);
  					if($pmpro_checkout_confirm_email) { ?>
  						<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-bconfirmemail', 'pmpro_checkout-field-bconfirmemail' ); ?>">
  							<label for="bconfirmemail"><?php _e('Confirm Email Address', 'paid-memberships-pro' );?></label>
  							<input id="bconfirmemail" name="bconfirmemail" type="<?php echo ($pmpro_email_field_type ? 'email' : 'text'); ?>" class="<?php echo pmpro_get_element_class( 'input', 'bconfirmemail' ); ?>" size="30" value="<?php echo esc_attr($bconfirmemail); ?>" />
  						</div> <!-- end pmpro_checkout-field-bconfirmemail -->
  					<?php } else { ?>
  						<input type="hidden" name="bconfirmemail_copy" value="1" />
  					<?php }
  				?>

  				<?php
  					do_action('pmpro_checkout_after_email');
            if($tospage) { ?>
        			<div id="pmpro_tos_fields" class="<?php echo pmpro_get_element_class( 'pmpro_checkout', 'pmpro_tos_fields' ); ?>">
      					<?php
                $tos_ID = $tospage->ID;
                $tos_link = get_permalink($tos_ID);
      					if ( isset( $_REQUEST['tos'] ) ) {
      						$tos = intval( $_REQUEST['tos'] );
      					} else {
      						$tos = 0;
      					} ?>
      					<input type="checkbox" name="tos" value="1" id="tos" <?php checked( 1, $tos ); ?> />
                <span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
                <label class="<?php echo pmpro_get_element_class( 'pmpro_label-inline pmpro_clickable', 'tos' ); ?>" for="tos">By checking this box I am at 18 years old or older and I agree to the <a href="<?php echo $tos_link; ?>" target="_blank" class="teal font-weight-bold">terms and conditions</a> and
                  <a href="<?php echo get_home_url('the-unsealed-privacy-policy'); ?>" target="_blank" class="teal font-weight-bold">privacy policy</a> of The Unsealed.</label>
        			</div> <!-- end pmpro_tos_fields -->
        		<?php
        		}

            do_action("pmpro_checkout_after_tos_fields");?>

  				<div class="<?php echo pmpro_get_element_class( 'pmpro_hidden' ); ?>">
  					<label for="fullname"><?php _e('Full Name', 'paid-memberships-pro' );?></label>
  					<input id="fullname" name="fullname" type="text" class="<?php echo pmpro_get_element_class( 'input', 'fullname' ); ?>" size="30" value="" autocomplete="off"/> <strong><?php _e('LEAVE THIS BLANK', 'paid-memberships-pro' );?></strong>
  				</div> <!-- end pmpro_hidden -->

  				<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_captcha', 'pmpro_captcha' ); ?>">
  				<?php
  					global $recaptcha, $recaptcha_publickey;
  					if($recaptcha == 2 || ($recaptcha == 1 && pmpro_isLevelFree($pmpro_level))) {
  						echo pmpro_recaptcha_get_html($recaptcha_publickey, NULL, true);
  					}
  				?>
  				</div> <!-- end pmpro_captcha -->

  				<?php

          do_action('pmpro_checkout_after_pricing_fields');

  				do_action('pmpro_checkout_after_captcha');
        } else {

          do_action('pmpro_checkout_after_pricing_fields');

          if($tospage && !$pmpro_review) { ?>
            <div id="pmpro_tos_fields" class="<?php echo pmpro_get_element_class( 'pmpro_checkout', 'pmpro_tos_fields' ); ?>">
              <?php
              $tos_ID = $tospage->ID;
              $tos_link = get_permalink($tos_ID);
              if ( isset( $_REQUEST['tos'] ) ) {
                $tos = intval( $_REQUEST['tos'] );
              } else {
                $tos = 0;
              } ?>
              <input type="checkbox" name="tos" value="1" id="tos" <?php checked( 1, $tos ); ?> />
              <span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
              <label class="<?php echo pmpro_get_element_class( 'pmpro_label-inline pmpro_clickable', 'tos' ); ?>" for="tos">By checking this box I am at 18 years old or older and I agree to the <a href="<?php echo $tos_link; ?>" target="_blank" class="teal font-weight-bold">terms and conditions</a> and
                <a href="<?php echo get_home_url('the-unsealed-privacy-policy'); ?>" target="_blank" class="teal font-weight-bold">privacy policy</a> of The Unsealed.</label>
            </div> <!-- end pmpro_tos_fields -->
          <?php
          }

          do_action('pmpro_checkout_after_captcha');
        } ?>
			</div>  <!-- end pmpro_checkout-fields -->
      <div class="d-flex row justify-content-center mt-3">
        <button type="button" class="btn submit-btn" onClick="account_continue(4)">
          <?php echo unsealed_btn('Continue', false, 'small'); ?>
        </button>
      </div>
      <?php if ( $pmpro_msg && $should_show ) { ?>
        <div id="pmpro_message_bottom" class="<?php echo pmpro_get_element_class( 'pmpro_message ' . $pmpro_msgt, $pmpro_msgt ); ?>"><?php echo $pmpro_msg; ?></div>
      <?php } else { ?>
        <div id="pmpro_message_bottom" class="<?php echo pmpro_get_element_class( 'pmpro_message' ); ?>" style="display: none;"></div>
      <?php } ?>
		</div> <!-- end pmpro_user_fields -->
    <?php do_action('pmpro_checkout_after_user_fields'); ?>
  </section>
<?php
}

function unsealed_display_payment_form () {
  global $gateway, $pmpro_review, $pmpro_requirebilling, $tospage, $pmpro_msg, $pmpro_msgt, $skip_account_fields, $pmpro_paypal_token, $wpdb, $current_user, $pmpro_level, $pmpro_levels, $pmpro_show_discount_code, $pmpro_error_fields;
	global $discount_code, $username, $first_name, $last_name, $password, $password2, $bfirstname, $blastname, $baddress1, $baddress2, $bcity, $bstate, $bzipcode, $bcountry, $bphone, $bemail, $bconfirmemail, $CardType, $AccountNumber, $ExpirationMonth,$ExpirationYear;
  $header = get_field('header_payment');
  $content = get_field('content_payment');
  $should_show = false;
  $account_errors = ['username','password','password2', 'bemail', 'bconfirmemail'];
  if ( count($pmpro_error_fields) > 0 ) {
    $should_show = true;
    foreach ( $pmpro_error_fields as $error_field ) {
      if ( in_array($error_field, $account_errors) ) {
        $should_show = false;
      }
    }
  }
  if ( $pmpro_review ) {
    $should_show = true;
  } ?>
  <section data-step-id="4" data-step="Payment" class="checkout-step pt-3 pb-5<?php echo $should_show ? ' visible' : ''; ?>" id="checkout-step-4">
    <?php
    if ( !$pmpro_review ) { ?>
      <button type="button" class="px-0 d-flex align-items-center subscription-go-back btn teal" onClick="checkoutNextStep(3)"><i class="fa fa-chevron-left"></i><?php echo !$skip_account_fields && !$pmpro_review ? 'Account' : 'Pricing'; ?></button>
      <?php
      echo unsealed_checkout_section_header($header, $content);
    }
    do_action('pmpro_checkout_boxes');
    if ( pmpro_getGateway() == "paypal" && empty($pmpro_review) && true == apply_filters('pmpro_include_payment_option_for_paypal', true ) ) { ?>
			<div id="pmpro_payment_method" class="<?php echo pmpro_get_element_class( 'pmpro_checkout', 'pmpro_payment_method' ); ?>" <?php if(!$pmpro_requirebilling) { ?>style="display: none;"<?php } ?>>
				<hr />
				<h3>
					<span class="<?php echo pmpro_get_element_class( 'pmpro_checkout-h3-name' ); ?>"><?php _e('Choose your Payment Method', 'paid-memberships-pro' ); ?></span>
				</h3>
				<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-fields' ); ?>">
					<span class="<?php echo pmpro_get_element_class( 'gateway_paypal' ); ?>">
						<input type="radio" name="gateway" value="paypal" <?php if(!$gateway || $gateway == "paypal") { ?>checked="checked"<?php } ?> />
						<a href="javascript:void(0);" class="<?php echo pmpro_get_element_class( 'pmpro_radio' ); ?>"><?php _e('Check Out with a Credit Card Here', 'paid-memberships-pro' );?></a>
					</span>
					<span class="<?php echo pmpro_get_element_class( 'gateway_paypalexpress' ); ?>">
						<input type="radio" name="gateway" value="paypalexpress" <?php if($gateway == "paypalexpress") { ?>checked="checked"<?php } ?> />
						<a href="javascript:void(0);" class="<?php echo pmpro_get_element_class( 'pmpro_radio' ); ?>"><?php _e('Check Out with PayPal', 'paid-memberships-pro' );?></a>
					</span>
				</div> <!-- end pmpro_checkout-fields -->
			</div> <!-- end pmpro_payment_method -->
		<?php
    }
		$pmpro_include_billing_address_fields = apply_filters('pmpro_include_billing_address_fields', true);
		if ( $pmpro_include_billing_address_fields ) { ?>
			<div id="pmpro_billing_address_fields" class="<?php echo pmpro_get_element_class( 'pmpro_checkout', 'pmpro_billing_address_fields' ); ?>" <?php if(!$pmpro_requirebilling || apply_filters("pmpro_hide_billing_address_fields", false) ){ ?>style="display: none;"<?php } ?>>
				<hr />
				<h3>
					<span class="<?php echo pmpro_get_element_class( 'pmpro_checkout-h3-name' ); ?>"><?php _e('Billing Address', 'paid-memberships-pro' );?></span>
				</h3>
				<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-fields' ); ?>">
					<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-bfirstname', 'pmpro_checkout-field-bfirstname' ); ?>">
						<label for="bfirstname"><?php _e('First Name', 'paid-memberships-pro' );?></label>
						<input id="bfirstname" name="bfirstname" type="text" class="<?php echo pmpro_get_element_class( 'input', 'bfirstname' ); ?>" size="30" value="<?php echo esc_attr($bfirstname); ?>" />
					</div> <!-- end pmpro_checkout-field-bfirstname -->
					<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-blastname', 'pmpro_checkout-field-blastname' ); ?>">
						<label for="blastname"><?php _e('Last Name', 'paid-memberships-pro' );?></label>
						<input id="blastname" name="blastname" type="text" class="<?php echo pmpro_get_element_class( 'input', 'blastname' ); ?>" size="30" value="<?php echo esc_attr($blastname); ?>" />
					</div> <!-- end pmpro_checkout-field-blastname -->
					<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-baddress1', 'pmpro_checkout-field-baddress1' ); ?>">
						<label for="baddress1"><?php _e('Address 1', 'paid-memberships-pro' );?></label>
						<input id="baddress1" name="baddress1" type="text" class="<?php echo pmpro_get_element_class( 'input', 'baddress1' ); ?>" size="30" value="<?php echo esc_attr($baddress1); ?>" />
					</div> <!-- end pmpro_checkout-field-baddress1 -->
					<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-baddress2', 'pmpro_checkout-field-baddress2' ); ?>">
						<label for="baddress2"><?php _e('Address 2', 'paid-memberships-pro' );?></label>
						<input id="baddress2" name="baddress2" type="text" class="<?php echo pmpro_get_element_class( 'input', 'baddress2' ); ?>" size="30" value="<?php echo esc_attr($baddress2); ?>" />
					</div> <!-- end pmpro_checkout-field-baddress2 -->
					<?php
						$longform_address = apply_filters("pmpro_longform_address", true);
						if($longform_address) { ?>
							<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-bcity', 'pmpro_checkout-field-bcity' ); ?>">
								<label for="bcity"><?php _e('City', 'paid-memberships-pro' );?></label>
								<input id="bcity" name="bcity" type="text" class="<?php echo pmpro_get_element_class( 'input', 'bcity' ); ?>" size="30" value="<?php echo esc_attr($bcity); ?>" />
							</div> <!-- end pmpro_checkout-field-bcity -->
							<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-bstate', 'pmpro_checkout-field-bstate' ); ?>">
								<label for="bstate"><?php _e('State', 'paid-memberships-pro' );?></label>
								<input id="bstate" name="bstate" type="text" class="<?php echo pmpro_get_element_class( 'input', 'bstate' ); ?>" size="30" value="<?php echo esc_attr($bstate); ?>" />
							</div> <!-- end pmpro_checkout-field-bstate -->
							<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-bzipcode', 'pmpro_checkout-field-bzipcode' ); ?>">
								<label for="bzipcode"><?php _e('Postal Code', 'paid-memberships-pro' );?></label>
								<input id="bzipcode" name="bzipcode" type="text" class="<?php echo pmpro_get_element_class( 'input', 'bzipcode' ); ?>" size="30" value="<?php echo esc_attr($bzipcode); ?>" />
							</div> <!-- end pmpro_checkout-field-bzipcode -->
						<?php } else { ?>
							<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-bcity_state_zip', 'pmpro_checkout-field-bcity_state_zip' ); ?>">
								<label for="bcity_state_zip' ); ?>"><?php _e('City, State Zip', 'paid-memberships-pro' );?></label>
								<input id="bcity" name="bcity" type="text" class="<?php echo pmpro_get_element_class( 'input', 'bcity' ); ?>" size="14" value="<?php echo esc_attr($bcity); ?>" />,
								<?php
									$state_dropdowns = apply_filters("pmpro_state_dropdowns", false);
									if($state_dropdowns === true || $state_dropdowns == "names") {
										global $pmpro_states;
										?>
										<select name="bstate" class="<?php echo pmpro_get_element_class( '', 'bstate' ); ?>">
											<option value="">--</option>
											<?php
												foreach($pmpro_states as $ab => $st) { ?>
													<option value="<?php echo esc_attr($ab);?>" <?php if($ab == $bstate) { ?>selected="selected"<?php } ?>><?php echo $st;?></option>
											<?php } ?>
										</select>
									<?php } elseif($state_dropdowns == "abbreviations") {
										global $pmpro_states_abbreviations;
										?>
										<select name="bstate" class="<?php echo pmpro_get_element_class( '', 'bstate' ); ?>">
											<option value="">--</option>
											<?php
												foreach($pmpro_states_abbreviations as $ab)
												{
											?>
												<option value="<?php echo esc_attr($ab);?>" <?php if($ab == $bstate) { ?>selected="selected"<?php } ?>><?php echo $ab;?></option>
											<?php } ?>
										</select>
									<?php } else { ?>
										<input id="bstate" name="bstate" type="text" class="<?php echo pmpro_get_element_class( 'input', 'bstate' ); ?>" size="2" value="<?php echo esc_attr($bstate); ?>" />
								<?php } ?>
								<input id="bzipcode" name="bzipcode" type="text" class="<?php echo pmpro_get_element_class( 'input', 'bzipcode' ); ?>" size="5" value="<?php echo esc_attr($bzipcode); ?>" />
							</div> <!-- end pmpro_checkout-field-bcity_state_zip -->
					<?php } ?>

					<?php
						$show_country = apply_filters("pmpro_international_addresses", true);
						if($show_country) { ?>
							<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-bcountry', 'pmpro_checkout-field-bcountry' ); ?>">
								<label for="bcountry"><?php _e('Country', 'paid-memberships-pro' );?></label>
								<select name="bcountry" id="bcountry" class="<?php echo pmpro_get_element_class( '', 'bcountry' ); ?>">
								<?php
									global $pmpro_countries, $pmpro_default_country;
									if(!$bcountry) {
										$bcountry = $pmpro_default_country;
									}
									foreach($pmpro_countries as $abbr => $country) { ?>
										<option value="<?php echo $abbr?>" <?php if($abbr == $bcountry) { ?>selected="selected"<?php } ?>><?php echo $country?></option>
									<?php } ?>
								</select>
							</div> <!-- end pmpro_checkout-field-bcountry -->
						<?php } else { ?>
							<input type="hidden" name="bcountry" value="US" />
						<?php } ?>
					<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-bphone', 'pmpro_checkout-field-bphone' ); ?>">
						<label for="bphone"><?php _e('Phone', 'paid-memberships-pro' );?></label>
						<input id="bphone" name="bphone" type="text" class="<?php echo pmpro_get_element_class( 'input', 'bphone' ); ?>" size="30" value="<?php echo esc_attr(formatPhone($bphone)); ?>" />
					</div> <!-- end pmpro_checkout-field-bphone -->
					<?php

          if($skip_account_fields) {

						if($current_user->ID) {
							if(!$bemail && $current_user->user_email) {
								$bemail = $current_user->user_email;
							}
							if(!$bconfirmemail && $current_user->user_email) {
								$bconfirmemail = $current_user->user_email;
							}
						}
					?>
					<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-bemail', 'pmpro_checkout-field-bemail' ); ?>">
						<label for="bemail"><?php _e('Email Address', 'paid-memberships-pro' );?></label>
						<input id="bemail" name="bemail" type="<?php echo ($pmpro_email_field_type ? 'email' : 'text'); ?>" class="<?php echo pmpro_get_element_class( 'input', 'bemail' ); ?>" size="30" value="<?php echo esc_attr($bemail); ?>" />
					</div> <!-- end pmpro_checkout-field-bemail -->
					<?php
						$pmpro_checkout_confirm_email = apply_filters("pmpro_checkout_confirm_email", true);
						if($pmpro_checkout_confirm_email) { ?>
							<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_checkout-field-bconfirmemail', 'pmpro_checkout-field-bconfirmemail' ); ?>">
								<label for="bconfirmemail"><?php _e('Confirm Email', 'paid-memberships-pro' );?></label>
								<input id="bconfirmemail" name="bconfirmemail" type="<?php echo ($pmpro_email_field_type ? 'email' : 'text'); ?>" class="<?php echo pmpro_get_element_class( 'input', 'bconfirmemail' ); ?>" size="30" value="<?php echo esc_attr($bconfirmemail); ?>" />
							</div> <!-- end pmpro_checkout-field-bconfirmemail -->
						<?php } else { ?>
							<input type="hidden" name="bconfirmemail_copy" value="1" />
						<?php } ?>
					<?php } ?>
				</div> <!-- end pmpro_checkout-fields -->
			</div> <!--end pmpro_billing_address_fields -->
		<?php
    } ?>

		<?php
    do_action("pmpro_checkout_after_billing_fields");
		$pmpro_accepted_credit_cards = pmpro_getOption("accepted_credit_cards");
		$pmpro_accepted_credit_cards = explode(",", $pmpro_accepted_credit_cards);
		$pmpro_accepted_credit_cards_string = pmpro_implodeToEnglish($pmpro_accepted_credit_cards);

		$pmpro_include_payment_information_fields = apply_filters("pmpro_include_payment_information_fields", true);
		if ( $pmpro_include_payment_information_fields ) { ?>
			<div id="pmpro_payment_information_fields" class="<?php echo pmpro_get_element_class( 'pmpro_checkout', 'pmpro_payment_information_fields' ); ?>" <?php if(!$pmpro_requirebilling || apply_filters("pmpro_hide_payment_information_fields", false) ) { ?>style="display: none;"<?php } ?>>
				<hr />
				<h3>
					<span class="<?php echo pmpro_get_element_class( 'pmpro_checkout-h3-name' ); ?>"><?php _e('Payment Information', 'paid-memberships-pro' );?></span>
					<span class="<?php echo pmpro_get_element_class( 'pmpro_checkout-h3-msg' ); ?>"><?php printf(__('We Accept %s', 'paid-memberships-pro' ), $pmpro_accepted_credit_cards_string);?></span>
				</h3>
				<?php $sslseal = pmpro_getOption("sslseal"); ?>
				<?php if(!empty($sslseal)) { ?>
					<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-fields-display-seal' ); ?>">
				<?php } ?>
				<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-fields' ); ?>">
					<?php
						$pmpro_include_cardtype_field = apply_filters('pmpro_include_cardtype_field', false);
						if($pmpro_include_cardtype_field) { ?>
							<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_payment-card-type', 'pmpro_payment-card-type' ); ?>">
								<label for="CardType"><?php _e('Card Type', 'paid-memberships-pro' );?></label>
								<select id="CardType" name="CardType" class="<?php echo pmpro_get_element_class( '', 'CardType' ); ?>">
									<?php foreach($pmpro_accepted_credit_cards as $cc) { ?>
										<option value="<?php echo $cc; ?>" <?php if($CardType == $cc) { ?>selected="selected"<?php } ?>><?php echo $cc; ?></option>
									<?php } ?>
								</select>
							</div>
						<?php } else { ?>
							<input type="hidden" id="CardType" name="CardType" value="<?php echo esc_attr($CardType);?>" />
						<?php } ?>
					<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_payment-account-number', 'pmpro_payment-account-number' ); ?>">
						<label for="AccountNumber"><?php _e('Card Number', 'paid-memberships-pro' );?></label>
						<input id="AccountNumber" name="AccountNumber" class="<?php echo pmpro_get_element_class( 'input', 'AccountNumber' ); ?>" type="text" size="30" value="<?php echo esc_attr($AccountNumber); ?>" data-encrypted-name="number" autocomplete="off" />
					</div>
					<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_payment-expiration', 'pmpro_payment-expiration' ); ?>">
						<label for="ExpirationMonth"><?php _e('Expiration Date', 'paid-memberships-pro' );?></label>
						<select id="ExpirationMonth" name="ExpirationMonth" class="<?php echo pmpro_get_element_class( '', 'ExpirationMonth' ); ?>">
							<option value="01" <?php if($ExpirationMonth == "01") { ?>selected="selected"<?php } ?>>01</option>
							<option value="02" <?php if($ExpirationMonth == "02") { ?>selected="selected"<?php } ?>>02</option>
							<option value="03" <?php if($ExpirationMonth == "03") { ?>selected="selected"<?php } ?>>03</option>
							<option value="04" <?php if($ExpirationMonth == "04") { ?>selected="selected"<?php } ?>>04</option>
							<option value="05" <?php if($ExpirationMonth == "05") { ?>selected="selected"<?php } ?>>05</option>
							<option value="06" <?php if($ExpirationMonth == "06") { ?>selected="selected"<?php } ?>>06</option>
							<option value="07" <?php if($ExpirationMonth == "07") { ?>selected="selected"<?php } ?>>07</option>
							<option value="08" <?php if($ExpirationMonth == "08") { ?>selected="selected"<?php } ?>>08</option>
							<option value="09" <?php if($ExpirationMonth == "09") { ?>selected="selected"<?php } ?>>09</option>
							<option value="10" <?php if($ExpirationMonth == "10") { ?>selected="selected"<?php } ?>>10</option>
							<option value="11" <?php if($ExpirationMonth == "11") { ?>selected="selected"<?php } ?>>11</option>
							<option value="12" <?php if($ExpirationMonth == "12") { ?>selected="selected"<?php } ?>>12</option>
						</select>/<select id="ExpirationYear" name="ExpirationYear" class="<?php echo pmpro_get_element_class( '', 'ExpirationYear' ); ?>">
							<?php
								$num_years = apply_filters( 'pmpro_num_expiration_years', 10 );

								for($i = date_i18n("Y"); $i < intval( date_i18n("Y") ) + intval( $num_years ); $i++)
								{
							?>
								<option value="<?php echo $i?>" <?php if($ExpirationYear == $i) { ?>selected="selected"<?php } ?>><?php echo $i?></option>
							<?php
								}
							?>
						</select>
					</div>
					<?php
						$pmpro_show_cvv = apply_filters("pmpro_show_cvv", true);
						if($pmpro_show_cvv) { ?>
						<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_payment-cvv', 'pmpro_payment-cvv' ); ?>">
							<label for="CVV"><?php _e('Security Code (CVC)', 'paid-memberships-pro' );?></label>
							<input id="CVV" name="CVV" type="text" size="4" value="<?php if(!empty($_REQUEST['CVV'])) { echo esc_attr($_REQUEST['CVV']); }?>" class="<?php echo pmpro_get_element_class( 'input', 'CVV' ); ?>" />  <small>(<a href="javascript:void(0);" onclick="javascript:window.open('<?php echo pmpro_https_filter(PMPRO_URL); ?>/pages/popup-cvv.html','cvv','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=600, height=475');"><?php _e("what's this?", 'paid-memberships-pro' );?></a>)</small>
						</div>
					<?php } ?>
					<?php if($pmpro_show_discount_code) { ?>
						<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-field pmpro_payment-discount-code', 'pmpro_payment-discount-code' ); ?>">
							<label for="discount_code"><?php _e('Discount Code', 'paid-memberships-pro' );?></label>
							<input class="<?php echo pmpro_get_element_class( 'input pmpro_alter_price', 'discount_code' ); ?>" id="discount_code" name="discount_code" type="text" size="10" value="<?php echo esc_attr($discount_code); ?>" />
							<input type="button" id="discount_code_button" name="discount_code_button" value="<?php _e('Apply', 'paid-memberships-pro' );?>" />
							<p id="discount_code_message" class="<?php echo pmpro_get_element_class( 'pmpro_message', 'discount_code_message' ); ?>" style="display: none;"></p>
						</div>
					<?php } ?>
				</div> <!-- end pmpro_checkout-fields -->
				<?php if(!empty($sslseal)) { ?>
					<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout-fields-rightcol pmpro_sslseal', 'pmpro_sslseal' ); ?>"><?php echo stripslashes($sslseal); ?></div>
				</div> <!-- end pmpro_checkout-fields-display-seal -->
				<?php } ?>
			</div> <!-- end pmpro_payment_information_fields -->
		<?php
    }
    do_action('pmpro_checkout_after_payment_information_fields');


    do_action("pmpro_checkout_before_submit_button"); ?>

		<div class="<?php echo pmpro_get_element_class( 'pmpro_submit' ); ?>">
			<?php if ( $pmpro_msg && $should_show ) { ?>
				<div id="pmpro_message_bottom" class="<?php echo pmpro_get_element_class( 'pmpro_message ' . $pmpro_msgt, $pmpro_msgt ); ?>"><?php echo $pmpro_msg; ?></div>
			<?php } else { ?>
				<div id="pmpro_message_bottom" class="<?php echo pmpro_get_element_class( 'pmpro_message' ); ?>" style="display: none;"></div>
			<?php } ?>

			<?php if($pmpro_review) { ?>

				<span id="pmpro_submit_span">
					<input type="hidden" name="confirm" value="1" />
					<input type="hidden" name="token" value="<?php echo esc_attr($pmpro_paypal_token); ?>" />
					<input type="hidden" name="gateway" value="<?php echo esc_attr($gateway); ?>" />
					<input type="submit" id="pmpro_btn-submit" class="<?php echo pmpro_get_element_class( 'pmpro_btn pmpro_btn-submit-checkout', 'pmpro_btn-submit-checkout' ); ?>" value="<?php _e('Complete Payment', 'paid-memberships-pro' );?> &raquo;" />
				</span>

			<?php } else { ?>

				<?php
					$pmpro_checkout_default_submit_button = apply_filters('pmpro_checkout_default_submit_button', true);
					if($pmpro_checkout_default_submit_button)
					{
					?>
					<span id="pmpro_submit_span">
						<input type="hidden" name="submit-checkout" value="1" />
						<input type="submit"  id="pmpro_btn-submit" class="<?php echo pmpro_get_element_class(  'pmpro_btn pmpro_btn-submit-checkout', 'pmpro_btn-submit-checkout' ); ?>" value="<?php if($pmpro_requirebilling) { _e('Submit and Check Out', 'paid-memberships-pro' ); } else { _e('Submit and Confirm', 'paid-memberships-pro' );}?> &raquo;" />
					</span>
					<?php
					}
				?>

			<?php } ?>

			<span id="pmpro_processing_message" style="visibility: hidden;">
				<?php
					$processing_message = apply_filters("pmpro_processing_message", __("Processing...", 'paid-memberships-pro' ));
					echo $processing_message;
				?>
			</span>
		</div>
  </section>
<?php
}

function add_checkout_acknowledgment () {
  // Add first/last name fields to checkout

}

function unsealed_checkout_section_header ($header, $content) {
  if ( $content || $header ) {
    ob_start(); ?>
    <div class="container py-3">
      <?php
      if ( $header ) { ?>
        <h2 class="text-center"><?php echo $header; ?></h2>
      <?php
      }
      if ( $content ) { ?>
        <div class="mt-1 text-center">
          <?php echo $content; ?>
        </div>
      <?php
      } ?>
    </div>
    <?php
    return ob_get_clean();
  }
}

function unsealed_init_load_session_vars() {
	if(function_exists('pmpro_start_session')) {
		pmpro_start_session();
	}

	if ( empty( $_REQUEST['price'] ) && ! empty( $_SESSION['price'] ) ) {
		$_REQUEST['price'] = $_SESSION['price'];
	}
  if ( isset($_REQUEST['level']) ) {
    $level_id = $_REQUEST['level'];
    if ( isset($_REQUEST['level-' . $level_id . '-custom_price']) && trim($_REQUEST['level-' . $level_id . '-custom_price']) != '' ) {
      $_REQUEST['price'] = $_REQUEST['level-' . $level_id . '-custom_price'];
    } else if ( isset($_REQUEST['level-' . $level_id . '-price']) ) {
      $_REQUEST['price'] = $_REQUEST['level-' . $level_id . '-price'];
    }
  }

  // if ( ! function_exists( 'pmprorh_add_registration_field' ) ) {
	// 	return false;
	// }
	// // define the fields
	// $fields   = array();
	// $fields[] = new PMProRH_Field(
	// 	'age-verification', // input field name, used as meta key
	// 	'checkbox',          // field type
	// 	array(
	// 		'label'   => 'I am at least 16 years of age', // field label
	// 		'profile' => false,            // display on user profile
  //     'required' => true
	// 	)
	// );
	// foreach ( $fields as $field ) {
	// 	pmprorh_add_registration_field(
	// 		'after_email', // location on checkout page
	// 		$field            // PMProRH_Field object
	// 	);
	// }
}
add_action( 'init', 'unsealed_init_load_session_vars', 5 );

function unsealed_registration_checks( $continue ) {
	// only bother if we are continuing already
	if ( $continue ) {
		global $pmpro_currency_symbol, $pmpro_msg, $pmpro_msgt;

		// was a price passed in?
		if ( isset( $_REQUEST['price'] ) ) {

 			// get values
			$level_id = intval( $_REQUEST['level'] );
			$fields = unsealed_get_level_fields( $level_id );

			// Bail if the Variable Pricing is not set for this level.
			if( empty( $fields['variable_pricing'] ) && empty( $fields['price_options'] ) ){
				return $continue;
			}

			// get price
			$price = preg_replace( '[^0-9\.]', '', $_REQUEST['price'] );

			// check that the price falls between the min and max
			if ( (double) $price < (double) $fields['min_price'] ) {
				$pmpro_msg  = sprintf(
					__( 'The minimum is %1$s%2$s.', 'unsealed-variable-pricing' ),
					esc_html( $pmpro_currency_symbol ),
					esc_html( $fields['min_price'] )
				);
				$pmpro_msgt = 'pmpro_error';
				$continue   = false;
			}
			// all good!
		}
	}

	return $continue;
}

// add_filter( 'pmpro_registration_checks', 'unsealed_registration_checks' );

// save fields in session for PayPal Express/etc
function unsealed_paypalexpress_session_vars() {
  $price = '';
  $level = $_REQUEST['level'];
  if ( isset($_REQUEST['level-' . $level . '-custom_price']) && trim($_REQUEST['level-' . $level . '-custom_price']) != '' ) {
    $price = $_REQUEST['level-' . $level . '-custom_price'];
  } else if ( isset($_REQUEST['level-' . $level . '-price']) ) {
    $price = $_REQUEST['level-' . $level . '-price'];
  }
	$_SESSION['price'] = $price;
}
add_action( 'pmpro_paypalexpress_session_vars', 'unsealed_paypalexpress_session_vars', 10 );
add_action( 'pmpro_before_send_to_twocheckout', 'unsealed_paypalexpress_session_vars', 10, 2 );

function set_new_checkout_level_price( $level ) {
  if ( isset($_REQUEST['level']) ) {
    $level_id = $_REQUEST['level'];
    $pmpro_levels = pmpro_getAllLevels(false, true);
    $level = $pmpro_levels[$_REQUEST['level']];
    $price = $level->initial_payment;
    $billing_price = $level->billing_amount;
    if ( isset($_REQUEST['level-' . $level_id . '-custom_price']) && trim($_REQUEST['level-' . $level_id . '-custom_price']) != '' ) {
      $price = $_REQUEST['level-' . $level_id . '-custom_price'];
    } else if ( isset($_REQUEST['level-' . $level_id . '-price']) ) {
      $price = $_REQUEST['level-' . $level_id . '-price'];
    }
    if ( isset( $_REQUEST['price'] ) ) {
  		$price = preg_replace( '[^0-9\.\,]', '', $_REQUEST['price'] );
  	}
    // log_it($level);
    $level->initial_payment = $price;
    if ( $level->billing_amount > 0 ) {
			$level->billing_amount = $price;
		}
  }
	return $level;
}

add_filter( 'pmpro_checkout_level', 'set_new_checkout_level_price' );

function login_or_subscribe ( $redirect = false, $dark = false ) {
  $redirect = $redirect ? '?redirect_to=' . $redirect : '';
  ob_start(); ?>
  <p class="text-center mb-0">
    <a class="teal <?php if ( $dark ) { echo 'hover-white '; } ?>font-weight-bold" href="<?php echo pmpro_url("login", $redirect, "https"); ?>">
      login
    </a>
    <span>or</span>
    <a class="teal <?php if ( $dark ) { echo 'hover-white '; } ?>font-weight-bold" href="<?php echo pmpro_url("checkout", $redirect, "https"); ?>">
      subscribe
    </a>
  </p>
  <?php
  return ob_get_clean();
}

function user_membership_level_changed ($level_id, $user_id, $cancel_level) {
	$args = [
		'action' => 'activity_update',
		'secondary_item_id' => 0,
		// 'primary_id' => $group_id,
		// 'scope' => 'groups', // Shows posts from the logged in users groups
		'per_page' => false,
		'display_comments' => false,
		'meta_query' => [
			[
				'key' => $level_id == 0 ? 'view_type' : 'view_type_original',
				'value' => 'public',
				'compare' => '='
			]
		],
		'user_id' => $user_id
	];
	if ( bp_has_activities( $args ) ) :
		while ( bp_activities() ) : bp_the_activity();
			if ( $activity_id = bp_get_activity_item_id() ):
				$update_to = $level_id == 0 ? 'community' : 'public';
				bp_activity_update_meta( bp_get_activity_id(), 'view_type', $update_to );
				if ( $level_id == 0 ) {
					bp_activity_add_meta( bp_get_activity_id(), 'view_type_original', 'public' );
				} else {
					bp_activity_delete_meta( bp_get_activity_id(), 'view_type_original' );
				}
			endif;
		endwhile;
	endif;
}

add_action( 'pmpro_after_change_membership_level', 'user_membership_level_changed', 10, 3);
