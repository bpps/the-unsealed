<?php
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'capability' 	=> 'manage_options',
		'menu_title' 	=> __('Footer'),
		'menu_slug' 	=> 'footer_options',
		'page_title' 	=> __('Footer')
	));
	acf_add_options_page(array(
		'capability' 	=> 'manage_options',
		'menu_title' 	=> __('The Unsealed Settings'),
		'menu_slug' 	=> 'unsealed_settings',
		'page_title' 	=> __('Settings'),
		'position'		=> 2
	));
	acf_add_options_page(array(
		'capability' 	=> 'manage_options',
		'menu_title' 	=> __('Contest Settings'),
		'parent_slug' 	=> 'edit.php?post_type=contest',
		'page_title' 	=> __('Contest Settings'),
		'position'		=> 2
	));
	acf_add_options_page(array(
		'capability' 	=> 'manage_options',
		'menu_title' 	=> __('Annoucement'),
		'menu_slug' 	=> 'unsealed_annoucement',
		'page_title' 	=> __('Annoucement'),
		'icon_url'		=> 'dashicons-testimonial',
		'position'		=> 3
	));

	acf_add_options_sub_page(array(
    'page_title'  => __('Letter Settings'),
    'menu_title'  => __('Letter Settings'),
    'parent_slug' => 'edit.php',
  ));

	acf_add_options_sub_page(array(
    'page_title'  => __('Community Voices'),
    'menu_title'  => __('Settings'),
    'parent_slug' => 'edit.php?post_type=community-voices',
  ));

	acf_add_options_sub_page(array(
    'page_title'  => __('Conversations Video'),
    'menu_title'  => __('Conversations Video'),
    'parent_slug' => 'edit.php?post_type=event',
  ));

}

add_filter('acf/load_field/name=advice_end_date', function ($field) {
	$end_date = $date = strtotime("+14 day");
  $field['default_value'] = date('Ymd', $end_date);
  return $field;
});
