<?php
function register_custom_post_types() {

	$labels = [
    'name' => _x('Videos', 'post type general name'),
    'singular_name' => _x('Video', 'post type singular name'),
    'add_new' => _x('Add New', 'video'),
    'add_new_item' => __('Add New Video'),
    'edit_item' => __('Edit Video'),
    'new_item' => __('New Video'),
    'view_item' => __('View Video'),
    'search_items' => __('Search Videos'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
		'menu_icon' => 'dashicons-video-alt3',
    'has_archive' => true,
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes']
  ];

  register_post_type( 'video' , $args );

	$labels = [
    'name' => _x('Contests', 'post type general name'),
    'singular_name' => _x('Contest', 'post type singular name'),
    'add_new' => _x('Add New', 'contest'),
    'add_new_item' => __('Add New Contest'),
    'edit_item' => __('Edit Contest'),
    'new_item' => __('New Contest'),
    'view_item' => __('View Contest'),
    'search_items' => __('Search Contests'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
		'menu_icon' => 'dashicons-awards',
    'has_archive' => true,
    'supports' => ['title', 'thumbnail', 'excerpt', 'page-attributes']
  ];

  register_post_type( 'contest' , $args );

	$labels = [
    'name' => _x('Contest Submissions', 'post type general name'),
    'singular_name' => _x('Contest Submission', 'post type singular name'),
    'add_new' => _x('Add New', 'contest-submissions'),
    'add_new_item' => __('Add New Contest Submission'),
    'edit_item' => __('Edit Contest Submission'),
    'new_item' => __('New Contest Submission'),
    'view_item' => __('View Contest Submission'),
    'search_items' => __('Search Contest Submissions'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
		'show_in_rest' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 6,
    'has_archive' => false,
		'menu_icon' => 'dashicons-buddicons-pm',
    'supports' => ['title', 'thumbnail', 'excerpt', 'page-attributes'],
    'bp_activity' => array(
			'component_id' => 'activity',
			'action_id'    => 'new_contest-submissions',
			'contexts'     => array( 'activity', 'member' ),
			'position'     => 100,
		),
  ];

  register_post_type( 'contest-submissions' , $args );

	$labels = [
    'name' => _x('Community Voices', 'post type general name'),
    'singular_name' => _x('Community Voice', 'post type singular name'),
    'add_new' => _x('Add New', 'community-voices'),
    'add_new_item' => __('Add New Community Voice Post'),
    'edit_item' => __('Edit Community Voice Post'),
    'new_item' => __('New Community Voice Post'),
    'view_item' => __('View Community Voice Post'),
    'search_items' => __('Search Community Voice Posts'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
		'show_in_rest' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 6,
    'has_archive' => true,
		'menu_icon' => 'dashicons-buddicons-pm',
		'taxonomies' => ['topic'],
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes']
  ];

  register_post_type( 'community-voices' , $args );

	$labels = [
    'name' => _x('Ask the Unsealed', 'post type general name'),
    'singular_name' => _x('Ask the Unsealed', 'post type singular name'),
    'add_new' => _x('Add New', 'ask-the-unsealed'),
    'add_new_item' => __('Add New Ask the Unsealed Post'),
    'edit_item' => __('Edit Ask the Unsealed Post'),
    'new_item' => __('New Ask the Unsealed Post'),
    'view_item' => __('View Ask the Unsealed Post'),
    'search_items' => __('Search Ask the Unsealed Posts'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 7,
    'has_archive' => true,
		'taxonomies' => ['topic', 'advisor'],
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes']
  ];

  register_post_type( 'ask-the-unsealed' , $args );
}

add_action('init', 'register_custom_post_types');

function contest_submission_column_header ($columns) {
	$columns['contest_name'] = 'Contest';
	$columns['member'] = 'Member';
  return $columns;
}

add_filter('manage_contest-submissions_posts_columns', 'contest_submission_column_header');

function contest_submission_column_content ($column_name, $post_ID) {
	if ($column_name == 'member' ) {
    $userId = get_field('member');
    if ($userId) {
      $user = get_userdata($userId);
      echo $user->display_name;
    } else {
      echo '';
    }
  }
}

add_action('manage_contest-submissions_posts_custom_column', 'contest_submission_column_content', 10, 2);

function contest_submission_orderby_contest( $query ) {
  if( ! is_admin() || ! $query->is_main_query() ) {
    return;
  }

  if ( 'contest_name' === $query->get( 'orderby') ) {
    $query->set( 'orderby', 'meta_value' );
    $query->set( 'meta_key', 'contest' );
    $query->set( 'meta_type', 'numeric' );
  }
}

add_action( 'pre_get_posts', 'contest_submission_orderby_contest' );

function contest_submission_sortable_columns( $columns ) {
  $columns['contest_name'] = 'contest';
  return $columns;
}

add_filter( 'manage_edit-contest-submissions_sortable_columns', 'contest_submission_sortable_columns');

function contest_submission_activity_column_header ($columns) {
	$columns['contest_name'] = 'Contest';
  $columns['votes'] = 'Votes';
  return $columns;
}

add_filter('bp_activity_list_table_get_columns', 'contest_submission_activity_column_header');

function contest_submission_activity_column_content ($ret_val, $column_name, $item) {
	if ($column_name == 'contest_name') {
    if ( $contest_id = bp_activity_get_meta( $item['id'], 'contest_id' ) ) {
      $contest_title = get_the_title($contest_id);
      echo $contest_title;
    }
  } else if ($column_name == 'votes' ) {
    echo bp_activity_get_meta( $item['id'], 'favorite_count' );
  }
}

add_action('bp_activity_admin_get_custom_column', 'contest_submission_activity_column_content', 10, 3);

function wpse45436_admin_posts_filter_restrict_manage_posts(){
  $type = 'contest-submissions';
  if (isset($_GET['post_type'])) {
      $type = $_GET['post_type'];
  }

  //only add filter to post type you want

  if ('contest-submissions' == $type){
    //change this to the list of values you want to show
    //in 'label' => 'value' format
    $contests = array();
    $contests = get_posts( [ 'posts_per_page' => -1, 'post_type' => 'contest' ] );
    foreach($contests as $contest):
      $values[$contest->post_title] = $contest->ID;
    endforeach; ?>

    <select name="contest">
    <option value=""><?php _e('All Contests', 'wose45436'); ?></option>
    <?php
    $current_v = isset($_GET['contest'])? $_GET['contest']:'';
    foreach ($contests as $contest) {
      printf(
        '<option value="%s"%s>%s</option>',
        $contest->ID,
        $contest->ID == $current_v? ' selected="selected"':'',
        $contest->post_title
      );
    }
    ?>
    </select>
  <?php
  }
}

add_action( 'restrict_manage_posts', 'wpse45436_admin_posts_filter_restrict_manage_posts' );

function contest_submissions_filter_query( $query ){
  global $pagenow;
  $type = 'contest-submissions';
  if (isset($_GET['post_type'])) {
      $type = $_GET['post_type'];
  }
  if ( 
      'contest-submissions' == $type  
       && is_admin()  
       && $pagenow =='edit.php'  
       && isset($_GET['contest']) 
       && $_GET['contest'] != '' 
       && $query->is_main_query()
  ) {
    $query->query_vars['meta_key'] = 'contest';
    $query->query_vars['meta_value'] = $_GET['contest'];
    unset($query->query_vars['contest']);
    unset($query->query_vars['name']);
    unset($query->query['contest']);
    unset($query->query['name']);
  }
  return $query;
}

add_filter( 'parse_query', 'contest_submissions_filter_query' );

function community_voice_column_header ($columns) {
	$columns['author_name'] = 'Author';
  return $columns;
}

add_filter('manage_community-voices_posts_columns', 'community_voice_column_header');

function community_voice_column_content ($column_name, $post_ID) {
	if ($column_name == 'author_name') {
    echo get_field('author_name');
  }
}

add_action('manage_community-voices_posts_custom_column', 'community_voice_column_content', 10, 2);