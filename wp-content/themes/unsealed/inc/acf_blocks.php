<?php
if( function_exists('acf_register_block') ) {

	// Video Block
	acf_register_block (
		array(
			'name'						=> 'unsealed-video',
			'title'						=> __('Unsealed Video'),
			'description'			=> 'Video',
			'category'				=> 'unsealed-blocks',
			'icon'						=> 'video-alt3',
			'keywords'				=> [ 'video' ],
			'mode' 						=> 'edit',
			'render_template' => 'blocks/unsealed-video.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Header block
	acf_register_block (
		array(
			'name'						=> 'unsealed-header',
			'title'						=> __('Unsealed Header'),
			'description'			=> 'Header',
			'category'				=> 'unsealed-blocks',
			'icon'						=> 'embed-photo',
			'keywords'				=> [ 'header' ],
			'mode' 						=> 'edit',
			'render_template' => 'blocks/unsealed-header.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// BP Groups block
	acf_register_block (
		array(
			'name'						=> 'unsealed-groups',
			'title'						=> __('Unsealed Groups'),
			'description'			=> 'Display social platform groups',
			'category'				=> 'unsealed-blocks',
			'icon'						=> 'embed-photo',
			'keywords'				=> [ 'groups' ],
			'mode' 						=> 'edit',
			'render_template' => 'blocks/unsealed-groups.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// BP Members block
	acf_register_block (
		array(
			'name'						=> 'unsealed-penpals',
			'title'						=> __('Unsealed Penpals'),
			'description'			=> 'Display social platform penpals',
			'category'				=> 'unsealed-blocks',
			'icon'						=> 'embed-photo',
			'keywords'				=> [ 'members', 'penpal' ],
			'mode' 						=> 'edit',
			'render_template' => 'blocks/unsealed-penpals.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Columns block
	acf_register_block (
		array(
			'name'						=> 'unsealed-columns',
			'title'						=> __('Unsealed Columns'),
			'description'			=> 'Columns',
			'category'				=> 'unsealed-blocks',
			'icon'						=> 'columns',
			'keywords'				=> [ 'columns' ],
			'mode' 						=> 'edit',
			'render_template' => 'blocks/unsealed-columns.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Button block
	acf_register_block (
		array(
			'name'						=> 'unsealed-button',
			'title'						=> __('Unsealed Button'),
			'description'			=> 'Columns',
			'category'				=> 'unsealed-blocks',
			'icon'						=> 'button',
			'keywords'				=> [ 'button' ],
			'mode' 						=> 'edit',
			'render_template' => 'blocks/unsealed-button.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Signature Block
	acf_register_block (
		array(
			'name'						=> 'unsealed-signature',
			'title'						=> __('Unsealed Signature'),
			'description'			=> 'Signature',
			'category'				=> 'unsealed-blocks',
			'icon'						=> 'admin-customizer',
			'keywords'				=> [ 'signature' ],
			'mode' 						=> 'edit',
			'render_template' => 'blocks/unsealed-signature.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Testimonial Block
	acf_register_block (
		array(
			'name'						=> 'testimonials',
			'title'						=> __('Testimonials'),
			'description'			=> 'Testimonials',
			'category'				=> 'unsealed-blocks',
			'icon'						=> 'testimonial',
			'keywords'				=> [ 'testimonial' ],
			'mode' 						=> 'edit',
			'render_template' => 'blocks/unsealed-testimonial.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

}

function unsealed_block_category( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'unsealed-blocks',
				'title' => __( 'Unnsealed Blocks', 'unsealed-blocks' )
			)
		)
	);
}

add_filter( 'block_categories', 'unsealed_block_category', 10, 2);
