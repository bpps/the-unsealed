<?php
/**
 * The template for displaying all single posts
 */
if ( $pagename == 'membership-levels' ) {
  wp_safe_redirect( home_url('accounts/sign-up') );
}
get_header(null, ["white_nav" => true]);
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php
    while ( have_posts() ) :
    the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="container py-3 py-md-5">
          <div class="d-flex justify-content-center row">
          <?php if (!bp_current_component()): ?>
            <header class="col-sm-10 col-lg-8 mb-5 entry-header text-center">
              <?php
              if ( is_singular() ) :
                the_title( '<h1 class="entry-title">', '</h1>' );
              else :
                the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
              endif;

              if ( 'post' === get_post_type() ) :
              ?>
              <?php endif; ?>
            </header><!-- .entry-header -->
          <?php endif; ?>
          </div>
          <div class="d-flex justify-content-center row">
            <?php if (bp_is_user_profile() || bp_current_component()): ?>
              <?php if ( is_user_logged_in() ) : ?>
                <div id="top-buttons" class="d-flex align-items-center ml-auto py-2 mr-3">
                  <a href="<?php echo bp_loggedin_user_domain(); ?>">My Profile</a>
                  <a class="ml-3" href="<?php echo home_url('groups'); ?>">All Groups</a>
                  <a class="ml-3" href="<?php echo home_url('account'); ?>">Subscription Settings</a>
                  <a class="screen-reader-shortcut ml-3" href="<?php echo esc_url( wp_logout_url('login') ); ?>"><?php _e( 'Log Out' ); ?></a>
                </div>
              <?php endif; ?>
              <div class="entry-content col-sm-12 col-lg-12">
            <?php else: ?>
              <div class="entry-content col-sm-10 col-lg-8">
            <?php endif;
              the_content( sprintf(
              wp_kses(
                  /* translators: %s: Name of current post. Only visible to screen readers */
                  __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'wowgulpwpstarter' ),
                  array(
                      'span' => array(
                          'class' => array(),
                      ),
                  )
              ),
              get_the_title()
              ) );

              wp_link_pages( array(
              'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wowgulpwpstarter' ),
              'after'  => '</div>',
              ) );
              ?>
            </div><!-- .entry-content -->
          </div>
        </div>
      </article><!-- #post-<?php the_ID(); ?> -->

      <?php

      // If comments are open or we have at least one comment, load up the comment template.
      if ( comments_open() || get_comments_number() ) :
          comments_template();
      endif;

    endwhile; // End of the loop.
    ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
